unit clsEntidade;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TEntidade = class
  public
    ent_id              : Integer;
    ent_nome            : String;
    ent_datacadastro    : TDateTime;
    ent_celular         : String;
    ent_telefone        : String;
    ent_email           : String;
    ent_ativo           : Integer; //0-Sim 1-N�o
    ent_obs             : String;

    function  Recuperar(Pent_id:Integer):Boolean;
    procedure Gravar(Pent_id:Integer);
    procedure Eliminar(Pent_id:Integer);
    function  Ultima:Integer;
    function  RecuperarPorEmail(Pent_email:String):Boolean;
    function  RecuperarPorCelular(Pent_Celular:String):Boolean;
    function  RecuperarPorTelefone(Pent_Telefone:String):Boolean;
  end;

implementation

{ TEntidade }

uses DMC;

procedure TEntidade.Eliminar(Pent_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Entidade where ent_id = :K_ent_id');
  tq.ParamByName('K_ent_id').AsInteger := Pent_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TEntidade.Gravar(Pent_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_id = :k_ent_id');
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Entidade values('+
    ':k_ent_id,'+
    ':k_ent_nome,'+
    ':k_ent_datacadastro,'+
    ':k_ent_celular,'+
    ':k_ent_telefone,'+
    ':k_ent_email,'+
    ':k_ent_ativo,'+
    ':k_ent_obs)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Entidade set '+
    'ent_nome            = :k_ent_nome,'+
    'ent_datacadastro    = :k_ent_datacadastro,'+
    'ent_celular         = :k_ent_celular,'+
    'ent_telefone        = :k_ent_telefone,'+
    'ent_email           = :k_ent_email,'+
    'ent_ativo           = :k_ent_ativo,'+
    'ent_obs             = :k_ent_obs ');
    tq.SQL.Add('where ent_id = :K_ent_id ');
  end;

  tq.ParamByName('k_ent_id').AsInteger             := Pent_id;
  tq.ParamByName('k_ent_nome').AsString            := ent_nome;
  tq.ParamByName('k_ent_datacadastro').AsDateTime  := ent_datacadastro;
  tq.ParamByName('k_ent_celular').AsString         := ent_celular;
  tq.ParamByName('k_ent_telefone').AsString        := ent_telefone;
  tq.ParamByName('k_ent_email').AsString           := ent_email;
  tq.ParamByName('k_ent_ativo').AsInteger          := ent_ativo;
  tq.ParamByName('k_ent_obs').AsString             := ent_obs;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TEntidade.Recuperar(Pent_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_id = :K_ent_id');
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  ent_id              := tq.FieldByName('ent_id').AsInteger;
  ent_nome            := tq.FieldByName('ent_nome').AsString;
  ent_datacadastro    := tq.FieldByName('ent_datacadastro').AsDateTime;
  ent_celular         := tq.FieldByName('ent_celular').AsString;
  ent_telefone        := tq.FieldByName('ent_telefone').AsString;
  ent_email           := tq.FieldByName('ent_email').AsString;
  ent_ativo           := tq.FieldByName('ent_ativo').AsInteger;
  ent_obs             := tq.FieldByName('ent_obs').AsString;

  tq.Close;
  tq.Free;
end;

function TEntidade.RecuperarPorCelular(Pent_Celular: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_celular = :K_ent_celular');
  tq.ParamByName('k_ent_celular').AsString := Pent_Celular;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('ent_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TEntidade.RecuperarPorEmail(Pent_email: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_email = :K_ent_email');
  tq.ParamByName('k_ent_email').AsString := Pent_email;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('ent_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TEntidade.RecuperarPorTelefone(Pent_Telefone: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_telefone = :K_ent_telefone');
  tq.ParamByName('k_ent_Telefone').AsString := Pent_Telefone;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('ent_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TEntidade.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(ent_id) maior from Entidade');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
