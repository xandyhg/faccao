unit ConUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConUsuario = class(TF_ConPadrao)
    FDUsuario: TFDQuery;
    ProviderUsuario: TDataSetProvider;
    CdsUsuario: TClientDataSet;
    DsUsuario: TDataSource;
    FDUsuarioUSUA_ID: TIntegerField;
    FDUsuarioUSUA_NOME: TStringField;
    FDUsuarioUSUA_LOGIN: TStringField;
    FDUsuarioUSUA_DATAADMISSAO: TSQLTimeStampField;
    FDUsuarioUSUA_DATADESLIGAMENTO: TSQLTimeStampField;
    FDUsuarioUSUA_FONE: TStringField;
    FDUsuarioUSUA_ATIVO: TIntegerField;
    CdsUsuarioUSUA_ID: TIntegerField;
    CdsUsuarioUSUA_NOME: TStringField;
    CdsUsuarioUSUA_LOGIN: TStringField;
    CdsUsuarioUSUA_DATAADMISSAO: TSQLTimeStampField;
    CdsUsuarioUSUA_DATADESLIGAMENTO: TSQLTimeStampField;
    CdsUsuarioUSUA_FONE: TStringField;
    CdsUsuarioUSUA_ATIVO: TIntegerField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CdsUsuarioUSUA_ATIVOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure CdsUsuarioUSUA_DATADESLIGAMENTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure CdsUsuarioUSUA_DATAADMISSAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConUsuario            : TF_ConUsuario;
  xAux_ConUsuario_Usua_ID : Integer;

implementation

{$R *.dfm}

uses DMC, X, CadUsuario, clsUsuario, Principal;

var
  Usuario  : TUsuario;
  tq       : TFDQuery;

procedure TF_ConUsuario.CdsUsuarioUSUA_ATIVOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '0' then
    Text := 'S'
  else
    Text := 'N';
end;

procedure TF_ConUsuario.CdsUsuarioUSUA_DATAADMISSAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '30/12/1899' then
    Text := '';
end;

procedure TF_ConUsuario.CdsUsuarioUSUA_DATADESLIGAMENTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '30/12/1899' then
    Text := '';
end;

procedure TF_ConUsuario.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConUsuario.icoFecharClick(Sender: TObject);
begin
  xAux_ConUsuario_Usua_ID := CdsUsuario.FieldByName('usua_id').AsInteger;
  inherited;

end;

procedure TF_ConUsuario.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConUsuario_Usua_ID := 0;
  AbreForm(TF_CadUsuario,Self);
  icoPesquisarClick(Sender);
  if xAux_ConUsuario_Usua_ID <> 0 then
    CdsUsuario.Locate('usua_id',xAux_ConUsuario_Usua_ID,[]);
end;

procedure TF_ConUsuario.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsUsuario.Close;

  FDUsuario.Close;
  FDUsuario.Params.Clear;
  FDUsuario.SQL.Clear;
  FDUsuario.SQL.Add('select u.usua_id, u.usua_nome, u.usua_login, u.usua_dataadmissao, u.usua_datadesligamento, '+
                    'u.usua_fone, u.usua_ativo                                                                  '+
                    'from usuario u                                                                             '+
                    'order by u.usua_nome                                                                       ');
  CdsUsuario.Open;

  if not CdsUsuario.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsUsuario.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConUsuario.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConUsuario_Usua_ID := CdsUsuario.FieldByName('usua_id').AsInteger;
  AbreForm(TF_CadUsuario, Self);
  icoPesquisarClick(Sender);
  if xAux_ConUsuario_Usua_ID <> 0 then
    CdsUsuario.Locate('usua_id',xAux_ConUsuario_Usua_ID,[]);
end;

procedure TF_ConUsuario.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if Column.FieldName = 'USUA_ATIVO' then begin
    if CdsUsuario.FieldByName('USUA_ATIVO').AsInteger = 0 then begin
      JvDBGrid1.Canvas.Brush.Color := clGreen;
      JvDBGrid1.Canvas.Font.Color  := clwhite;
    end else begin
      JvDBGrid1.Canvas.Brush.Color := clRed;
      JvDBGrid1.Canvas.Font.Color  := clwhite;
    end;
  end;
  JvDBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

end.
