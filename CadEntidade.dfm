inherited F_CadEntidade: TF_CadEntidade
  Caption = ' Cadastro de Cliente'
  ClientHeight = 272
  ClientWidth = 765
  OnActivate = FormActivate
  ExplicitWidth = 781
  ExplicitHeight = 311
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 195
    Width = 765
    ExplicitTop = 427
    ExplicitWidth = 993
    inherited icoFechar: TImage
      Left = 675
      ExplicitLeft = 784
    end
    inherited icoAlterar: TImage
      Left = 495
      OnClick = icoAlterarClick
      ExplicitLeft = 604
    end
    inherited icoExcluir: TImage
      Left = 585
      OnClick = icoExcluirClick
      ExplicitLeft = 707
      ExplicitHeight = 51
    end
    inherited icoSalvar: TImage
      Left = 315
      OnClick = icoSalvarClick
      ExplicitLeft = 424
    end
    inherited icoCancelar: TImage
      Left = 405
      OnClick = icoCancelarClick
      ExplicitLeft = 514
    end
  end
  inherited Panel3: TPanel
    Width = 765
    Height = 154
    ExplicitWidth = 993
    ExplicitHeight = 386
    object Panel9: TPanel
      Left = 1
      Top = 1
      Width = 763
      Height = 152
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitTop = 42
      ExplicitWidth = 991
      ExplicitHeight = 208
      object Panel7: TPanel
        Left = 1
        Top = 1
        Width = 761
        Height = 150
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ExplicitLeft = 353
        ExplicitWidth = 637
        ExplicitHeight = 206
        object Label2: TLabel
          Left = 12
          Top = 10
          Width = 36
          Height = 17
          Alignment = taRightJustify
          Caption = 'Nome'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 296
          Top = 41
          Width = 49
          Height = 17
          Alignment = taRightJustify
          Caption = 'Telefone'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 8
          Top = 41
          Width = 40
          Height = 17
          Alignment = taRightJustify
          Caption = 'Celular'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 17
          Top = 72
          Width = 31
          Height = 17
          Alignment = taRightJustify
          Caption = 'Email'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 464
          Top = 12
          Width = 24
          Height = 17
          Alignment = taRightJustify
          Caption = 'Obs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object edEnt_nome: TEdit
          Left = 54
          Top = 7
          Width = 407
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edEnt_Telefone: TEdit
          Left = 350
          Top = 38
          Width = 111
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 15
          NumbersOnly = True
          ParentFont = False
          TabOrder = 2
          OnEnter = edEnt_TelefoneEnter
          OnExit = edEnt_TelefoneExit
        end
        object edEnt_Celular: TEdit
          Left = 54
          Top = 38
          Width = 111
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 15
          NumbersOnly = True
          ParentFont = False
          TabOrder = 1
          OnEnter = edEnt_CelularEnter
          OnExit = edEnt_CelularExit
        end
        object edEnt_email: TEdit
          Left = 54
          Top = 69
          Width = 407
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 255
          ParentFont = False
          TabOrder = 3
          OnExit = edEnt_emailExit
        end
        object edEnt_Obs: TMemo
          Left = 494
          Top = 7
          Width = 259
          Height = 134
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 4
        end
        object rgEnt_Ativo: TRadioGroup
          Left = 52
          Top = 100
          Width = 113
          Height = 41
          Caption = ' Ativo '
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ItemIndex = 0
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          TabOrder = 5
        end
      end
    end
  end
  inherited Panel4: TPanel
    Top = 240
    Width = 765
    ExplicitTop = 472
    ExplicitWidth = 993
    inherited pnCancelar: TPanel
      Left = 405
      ExplicitLeft = 633
    end
    inherited pnAlterar: TPanel
      Left = 495
      ExplicitLeft = 723
    end
    inherited pnExcluir: TPanel
      Left = 585
      ExplicitLeft = 813
    end
    inherited pnSalvar: TPanel
      Left = 315
      ExplicitLeft = 543
    end
    inherited pnFechar: TPanel
      Left = 675
      ExplicitLeft = 903
    end
  end
  inherited Panel5: TPanel
    Width = 765
    ExplicitWidth = 993
    object Label1: TLabel
      Left = 7
      Top = 12
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 304
      Top = 12
      Width = 53
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cadastro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 56
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edEnt_DataCadastro: TJvDateEdit
      Left = 363
      Top = 9
      Width = 100
      Height = 25
      ShowNullDate = False
      TabOrder = 1
    end
  end
  inherited Timer1: TTimer
    Left = 224
    Top = 0
  end
  object HTTPRIO1: THTTPRIO
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 264
  end
end
