unit ConLote;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.DBCtrls;

type
  TF_ConLote = class(TF_ConPadrao)
    FDLote: TFDQuery;
    ProviderLote: TDataSetProvider;
    CdsLote: TClientDataSet;
    DsLote: TDataSource;
    JvDBGrid2: TJvDBGrid;
    FDEntidade: TFDQuery;
    ProviderEntidade: TDataSetProvider;
    CdsEntidade: TClientDataSet;
    DsEntidade: TDataSource;
    FDEntidadeENT_ID: TIntegerField;
    FDEntidadeENT_NOME: TStringField;
    FDEntidadeENT_CELULAR: TStringField;
    FDEntidadeENT_EMAIL: TStringField;
    CdsEntidadeENT_ID: TIntegerField;
    CdsEntidadeENT_NOME: TStringField;
    CdsEntidadeENT_CELULAR: TStringField;
    CdsEntidadeENT_EMAIL: TStringField;
    FDLoteENT_ID: TIntegerField;
    FDLoteLOTE_ID: TIntegerField;
    FDLoteLOTE_DESCRICAO: TStringField;
    FDLoteLOTE_DATAFECHAMENTO: TSQLTimeStampField;
    FDLoteLOTE_VALOR: TFloatField;
    CdsLoteENT_ID: TIntegerField;
    CdsLoteLOTE_ID: TIntegerField;
    CdsLoteLOTE_DESCRICAO: TStringField;
    CdsLoteLOTE_DATAFECHAMENTO: TSQLTimeStampField;
    CdsLoteLOTE_VALOR: TFloatField;
    FDLoteLOTE_OBS: TMemoField;
    CdsLoteLOTE_OBS: TMemoField;
    DBMemo1: TDBMemo;
    FDLoteLOTE_NUMERO: TIntegerField;
    CdsLoteLOTE_NUMERO: TIntegerField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure DsEntidadeDataChange(Sender: TObject; Field: TField);
    procedure JvDBGrid2DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConLote            : TF_ConLote;
  xAux_ConLote_Ent_ID  : Integer;
  xAux_ConLote_lote_ID : Integer;

implementation

{$R *.dfm}

uses DMC, X, CadLote;

procedure TF_ConLote.DsEntidadeDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  CdsLote.Filter := 'ent_id='+IntToStr(CdsEntidade.FieldByName('ent_id').AsInteger);
  if not CdsLote.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsLote.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConLote.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConLote.icoFecharClick(Sender: TObject);
begin
  xAux_ConLote_Ent_ID  := CdsLote.FieldByName('ent_id').AsInteger;
  xAux_ConLote_lote_ID := CdsLote.FieldByName('lote_id').AsInteger;
  inherited;

end;

procedure TF_ConLote.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConLote_Ent_ID  := CdsEntidade.FieldByName('ent_id').AsInteger;
  xAux_ConLote_lote_ID := 0;
  AbreForm(TF_CadLote,Self);
  icoPesquisarClick(Sender);
  if xAux_ConLote_lote_ID <> 0 then
    CdsLote.Locate('ent_id;lote_id',VarArrayOf([xAux_ConLote_Ent_ID,xAux_ConLote_lote_ID]),[]);
end;

procedure TF_ConLote.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsEntidade.Close;

  FDEntidade.Close;
  FDEntidade.Params.Clear;
  FDEntidade.SQL.Clear;
  FDEntidade.SQL.Add('select distinct e.ent_id, e.ent_nome, e.ent_celular, e.ent_email '+
                     'from entidade e                                                  '+
                     'order by e.ent_nome                                              ');
  CdsEntidade.Open;

  CdsLote.Close;

  FDLote.Close;
  FDLote.Params.Clear;
  FDLote.SQL.Clear;
  FDLote.SQL.Add('select l.ent_id, l.lote_id, l.lote_descricao, l.lote_datafechamento, l.lote_valor, l.lote_obs, l.lote_numero '+
                 'from lote l                                                                                                  '+
                 'order by l.ent_id, l.lote_datafechamento                                                                     ');
  CdsLote.Open;

  if not CdsLote.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsLote.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConLote.JvDBGrid2DblClick(Sender: TObject);
begin
  inherited;
  if CdsLote.RecordCount > 0 then begin
    xAux_ConLote_Ent_ID  := CdsLote.FieldByName('ent_id').AsInteger;
    xAux_ConLote_lote_ID := CdsLote.FieldByName('lote_id').AsInteger;
    AbreForm(TF_CadLote, Self);
    icoPesquisarClick(Sender);
    CdsEntidade.Locate('ent_id',xAux_ConLote_Ent_ID,[]);
    CdsLote.Locate('lote_id',xAux_ConLote_lote_ID,[]);
    xAux_ConLote_Ent_ID := 0;
    xAux_ConLote_lote_ID := 0;
  end;
end;

end.
