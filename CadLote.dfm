inherited F_CadLote: TF_CadLote
  Caption = ' Cadastro de Lote'
  ClientHeight = 317
  ClientWidth = 841
  OnActivate = FormActivate
  ExplicitWidth = 857
  ExplicitHeight = 356
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 240
    Width = 841
    ExplicitTop = 240
    ExplicitWidth = 841
    inherited icoFechar: TImage
      Left = 751
      ExplicitLeft = 751
    end
    inherited icoAlterar: TImage
      Left = 571
      OnClick = icoAlterarClick
      ExplicitLeft = 571
    end
    inherited icoExcluir: TImage
      Left = 661
      OnClick = icoExcluirClick
      ExplicitLeft = 661
    end
    inherited icoSalvar: TImage
      Left = 391
      OnClick = icoSalvarClick
      ExplicitLeft = 391
    end
    inherited icoCancelar: TImage
      Left = 481
      OnClick = icoCancelarClick
      ExplicitLeft = 481
    end
  end
  inherited Panel3: TPanel
    Width = 841
    Height = 199
    ExplicitWidth = 841
    ExplicitHeight = 199
    object Label2: TLabel
      Left = 149
      Top = 13
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 508
      Top = 13
      Width = 70
      Height = 17
      Alignment = taRightJustify
      Caption = 'Fechamento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 697
      Top = 13
      Width = 28
      Height = 17
      Alignment = taRightJustify
      Caption = 'Total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 36
      Top = 13
      Width = 45
      Height = 17
      Caption = 'N'#186' Lote'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edLote_descricao: TEdit
      Left = 212
      Top = 10
      Width = 283
      Height = 25
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object edLote_datafechamento: TJvDateEdit
      Left = 584
      Top = 10
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 2
    end
    object edLote_valor: TJvCalcEdit
      Left = 731
      Top = 10
      Width = 100
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
    end
    object GroupBox1: TGroupBox
      Left = 88
      Top = 41
      Width = 743
      Height = 145
      Caption = 'Observa'#231#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      object mLote_obs: TMemo
        Left = 2
        Top = 19
        Width = 739
        Height = 124
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ExplicitLeft = 3
      end
    end
    object edLote_numero: TJvCalcEdit
      Left = 87
      Top = 10
      Width = 56
      Height = 25
      DecimalPlaces = 0
      DisplayFormat = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Panel4: TPanel
    Top = 285
    Width = 841
    ExplicitTop = 285
    ExplicitWidth = 841
    inherited pnCancelar: TPanel
      Left = 481
      ExplicitLeft = 481
    end
    inherited pnAlterar: TPanel
      Left = 571
      ExplicitLeft = 571
    end
    inherited pnExcluir: TPanel
      Left = 661
      ExplicitLeft = 661
    end
    inherited pnSalvar: TPanel
      Left = 391
      ExplicitLeft = 391
    end
    inherited pnFechar: TPanel
      Left = 751
      ExplicitLeft = 751
    end
  end
  inherited Panel5: TPanel
    Width = 841
    ExplicitWidth = 841
    object Label1: TLabel
      Left = 12
      Top = 12
      Width = 70
      Height = 17
      Caption = 'C'#243'd. Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 672
      Top = 12
      Width = 53
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cadastro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 150
      Top = 12
      Width = 56
      Height = 17
      Caption = 'C'#243'd. Lote'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 88
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edLote_datacadastro: TJvDateEdit
      Left = 731
      Top = 9
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 1
    end
    object edCodigo2: TJvCalcEdit
      Left = 212
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 288
  end
end
