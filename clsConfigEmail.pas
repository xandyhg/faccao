unit clsConfigEmail;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TConfigEmail = class
  public
    confe_id           : Integer;
    confe_nome         : String;
    confe_autenticacao : String;
    confe_porta        : Integer;
    confe_smtp         : String;
    confe_endereco     : String;

    function  Recuperar(Pconfe_ID:Integer):Boolean;
    procedure Gravar(Pconfe_id:Integer);
    procedure Eliminar(Pconfe_id:Integer);
    function  Ultima:Integer;
  end;

implementation

{ TConfigEmail }

uses DMC;

procedure TConfigEmail.Eliminar(Pconfe_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from ConfigEmail where confe_id = :K_confe_id');
  tq.ParamByName('K_confe_id').AsInteger := Pconfe_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TConfigEmail.Gravar(Pconfe_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ConfigEmail where confe_id = :k_confe_id');
  tq.ParamByName('k_confe_id').AsInteger := Pconfe_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into ConfigEmail values('+
    ':k_confe_id,'+
    ':k_confe_nome,'+
    ':k_confe_autenticacao,'+
    ':k_confe_porta,'+
    ':k_confe_smtp,'+
    ':k_confe_endereco)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update ConfigEmail set '+
    'confe_nome         = :k_confe_nome,'+
    'confe_autenticacao = :k_confe_autenticacao,'+
    'confe_porta        = :k_confe_porta,'+
    'confe_smtp         = :k_confe_smtp,'+
    'confe_endereco     = :k_confe_endereco ');
    tq.SQL.Add('where confe_id = :K_confe_id ');
  end;

  tq.ParamByName('k_confe_id').AsInteger          := Pconfe_id;
  tq.ParamByName('k_confe_nome').AsString         := confe_nome;
  tq.ParamByName('k_confe_autenticacao').AsString := confe_autenticacao;
  tq.ParamByName('k_confe_porta').AsInteger       := confe_porta;
  tq.ParamByName('k_confe_smtp').AsString         := confe_smtp;
  tq.ParamByName('k_confe_endereco').AsString     := confe_endereco;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TConfigEmail.Recuperar(Pconfe_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ConfigEmail where confe_id = :K_confe_id');
  tq.ParamByName('k_confe_id').AsInteger := Pconfe_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  confe_id           := tq.FieldByName('confe_id').AsInteger;
  confe_nome         := tq.FieldByName('confe_nome').AsString;
  confe_autenticacao := tq.FieldByName('confe_autenticacao').AsString;
  confe_porta        := tq.FieldByName('confe_porta').AsInteger;
  confe_smtp         := tq.FieldByName('confe_smtp').AsString;
  confe_endereco     := tq.FieldByName('confe_endereco').AsString;

  tq.Close;
  tq.Free;
end;

function TConfigEmail.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(confe_id) maior from ConfigEmail');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
