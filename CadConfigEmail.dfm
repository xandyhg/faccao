inherited F_CadConfigEmail: TF_CadConfigEmail
  Caption = ' Cadastro de Configura'#231#227'o de Email'
  ClientHeight = 274
  ClientWidth = 648
  OnActivate = FormActivate
  ExplicitWidth = 664
  ExplicitHeight = 313
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 197
    Width = 648
    ExplicitTop = 197
    ExplicitWidth = 648
    inherited icoFechar: TImage
      Left = 558
      ExplicitLeft = 558
    end
    inherited icoAlterar: TImage
      Left = 378
      OnClick = icoAlterarClick
      ExplicitLeft = 378
    end
    inherited icoExcluir: TImage
      Left = 468
      OnClick = icoExcluirClick
      ExplicitLeft = 468
    end
    inherited icoSalvar: TImage
      Left = 198
      OnClick = icoSalvarClick
      ExplicitLeft = 198
    end
    inherited icoCancelar: TImage
      Left = 288
      OnClick = icoCancelarClick
      ExplicitLeft = 288
    end
  end
  inherited Panel3: TPanel
    Width = 648
    Height = 156
    ExplicitWidth = 648
    ExplicitHeight = 156
    object Label2: TLabel
      Left = 30
      Top = 9
      Width = 36
      Height = 17
      Alignment = taRightJustify
      Caption = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 11
      Top = 40
      Width = 55
      Height = 17
      Alignment = taRightJustify
      Caption = 'Endere'#231'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 33
      Top = 71
      Width = 33
      Height = 17
      Alignment = taRightJustify
      Caption = 'SMTP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 35
      Top = 102
      Width = 31
      Height = 17
      Alignment = taRightJustify
      Caption = 'Porta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edConfe_nome: TEdit
      Left = 72
      Top = 6
      Width = 569
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TextHint = 'EX.: GMAIL, HOTMAIL,...'
    end
    object edConfe_endereco: TEdit
      Left = 72
      Top = 37
      Width = 569
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      TextHint = 'Ex.: gmail.com, hotmail.com,...'
    end
    object edConfe_smtp: TEdit
      Left = 72
      Top = 68
      Width = 569
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      TextHint = 'Ex.: smtp.gmail.com'
    end
    object edConfe_porta: TJvCalcEdit
      Left = 72
      Top = 99
      Width = 56
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
    end
    object rgConfe_autenticacao: TRadioGroup
      Left = 456
      Top = 93
      Width = 185
      Height = 44
      Caption = 'Autentica'#231#227'o'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Login'
        'None')
      TabOrder = 4
    end
  end
  inherited Panel4: TPanel
    Top = 242
    Width = 648
    ExplicitTop = 242
    ExplicitWidth = 648
    inherited pnCancelar: TPanel
      Left = 288
      ExplicitLeft = 288
    end
    inherited pnAlterar: TPanel
      Left = 378
      ExplicitLeft = 378
    end
    inherited pnExcluir: TPanel
      Left = 468
      ExplicitLeft = 468
    end
    inherited pnSalvar: TPanel
      Left = 198
      ExplicitLeft = 198
    end
    inherited pnFechar: TPanel
      Left = 558
      ExplicitLeft = 558
    end
  end
  inherited Panel5: TPanel
    Width = 648
    ExplicitWidth = 648
    object Label1: TLabel
      Left = 23
      Top = 10
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 72
      Top = 7
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 544
  end
end
