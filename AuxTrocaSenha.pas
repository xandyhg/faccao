unit AuxTrocaSenha;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AuxPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls;

type
  TF_AuxTrocaSenha = class(TF_AuxPadrao)
    Image3: TImage;
    edNovaSenha: TEdit;
    Image1: TImage;
    edConfirmaSenha: TEdit;
    procedure icoCancelarClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_AuxTrocaSenha              : TF_AuxTrocaSenha;
  xAux_AuxTrocaSenha_NovaSenha : String;

implementation

{$R *.dfm}

uses X, Principal;

procedure TF_AuxTrocaSenha.icoCancelarClick(Sender: TObject);
begin
  inherited;
  xAux_AuxTrocaSenha_NovaSenha := '';
  Close;
end;

procedure TF_AuxTrocaSenha.icoSalvarClick(Sender: TObject);
begin
  inherited;
  if Trim(edNovaSenha.Text) = Trim(edConfirmaSenha.Text) then begin
    xAux_AuxTrocaSenha_NovaSenha := Criptografia(edNovaSenha.Text,xAux_SenhaAdmin);;
    Close;
  end else begin
    msgPerson('Confirma��o da senha n�o confere com a nova senha !',[mbok],'Nova Senha',mtConfirmation);
    edNovaSenha.Clear;
    edConfirmaSenha.Clear;
    edNovaSenha.SetFocus;
  end;
end;

end.
