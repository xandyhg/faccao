inherited F_ConContaCorrente: TF_ConContaCorrente
  Caption = ' Consulta de Conta Corrente'
  ClientHeight = 545
  ClientWidth = 1241
  ExplicitWidth = 1257
  ExplicitHeight = 584
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 432
    Width = 1241
    ExplicitTop = 432
    ExplicitWidth = 1241
    inherited icoPesquisar: TImage
      Left = 821
      ExplicitLeft = 821
    end
    inherited icoNovo: TImage
      Left = 926
      ExplicitLeft = 926
    end
    inherited icoImprimir: TImage
      Left = 1031
      ExplicitLeft = 1031
    end
    inherited icoFechar: TImage
      Left = 1136
      ExplicitLeft = 1136
    end
    object SpeedButton1: TSpeedButton
      Left = 0
      Top = 0
      Width = 49
      Height = 45
      Hint = 'Cadastrar Cliente'
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      Glyph.Data = {
        360C0000424D360C000000000000360000002800000020000000200000000100
        180000000000000C0000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCDCCA5967BB29E7BB2
        9F7CB3A07CA7997DD1CFCEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAF7F4F1F4F0
        E7F0E9DCEDE4D3EEE3D0EFE4D0EEE4D2F0E8DAF6F1E7C8C3BBCEAB6CE8BB66E9
        BD69EDBF68D1AF6FCFCDC9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFAF9F7EEE6D9E0CFB0D7BB8BD2AE6FCFA65CD2A4
        53D3A44DD4A44BD8A74EDAAA50DCAC53DEB05AE4B765BDA06BCFAD6EEABC68EB
        BE6CEFC16BD5B273CFCECAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFAF9F7E0D0B5C9A566C1903AC08A29C48C29C7902CCB932FCE97
        33D19B38D59F3ED9A444DCA84ADEAB4DE1AE50E5B151BF9C5ACFAD6EE9BD6BEB
        C070EEC16ED4B273CAC4BBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFAFAF8D0B688BA872EBC8524BE8724C08926C48C29C7902ECA9432CE98
        36D19B3AD39E3CD7A13FDAA544DEAA4BE1AE50E5B152BE9B5ACEAB6DE8BB6AE9
        BE6EECC06DD3B171C4A973F2E3C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFEBE4D7BA8C3CBD8A32BB8526BC8624C08927C38D2CC79131CA9536CD99
        3AD09C3DD19D40B48E49AE8B4DB18E4FB49255B79659A68D60CFAB6AE6B865E7
        BB69EABD69D4B16FAF9667BFA778C4BFB6C7C3BBC6C2BACCCAC7FFFFFFFFFFFF
        FFFFFFEAE3D6BB8E3FBF8E3ABC882CBC8525BF8929C38E2FC69234CA9539CD99
        3DD09D41CC9C449C804FB48B41BB9045BF954BC69F57CEA660DDB161E2B460E5
        B763E7B964E6BA68DAB369DAB267D7AD5ED5A957D4A957AA9A7DFFFFFFFFFFFF
        FFFFFFF5F2EDC09956C0903FBE8B34BD872ABF892BC28E31C69237CA973CCD9B
        41D09F45CC9D49A1824CC08C30C69133CB973BD2A046DBAA53DEAE58E0B15CE3
        B45EE6B65FE8B85FEBBA5EEDBB5DEAB553E8B14BE7B14CB29F7BFFFFFFFFFFFF
        FFFFFFFFFFFFCFB589BE8F3EC0903DBE8B32BF8A2EC28E33C69339CA983FCD9C
        45D1A049CC9F4D9F814BBC882CC38D2FC79337CC993ED5A34BDBAA53DEAE56E1
        B159E3B35AE6B55BE8B65AEAB759E5B04EE4AE47E4AE49B19D79FFFFFFFFFFFF
        FFFFFFFFFFFFE8DECEBC9045C29445C0903CC08D34C28F35C6943CCA9842CD9D
        47D0A14CCCA0519E804BBA8527C18A2AC59031C99437CE9A3ED6A34ADBAA51DF
        AD54E1AF55E4B155E8B455E8B453E3AC47E4AB42E2AB44B09C76FFFFFFFFFFFF
        FFFFFFFFFFFFFCFBFBCDB283C09243C39545C1913CC29038C5943DC99943CC9D
        49D0A14FCDA153977C4EA47C38A98039AC853EAF8843B48D47C99943D3A044D9
        A64BDEAB4EDBAB55C9A35EC6A05AC29B52C29A4FC0984FA4957AFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0EBE2C39F60C39548C39544C2923CC5943EC89844CC9E
        4BD2A75AD5AE67C0A26DB99D6CBB9D67BA995DBA9553A28654BA8F44CE9939D1
        9C3DD7A141C49B50B4B0A9D4D3D1D3D1CFD3D1CFD3D1CFD9D9D8FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFEAE2D4C5A163C39546C49543C4933EC99C4CD7B4
        77DAC296D7C6A7D5C6ACD2C3A8D4C2A0DEC494E5C07BB8975AB68B3FCB9533CE
        9837D39C3ABF9449CFCCC7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0EAE1CEB281C29649C49441D4B071E3D3
        B8ECE8E3E8E8E8E0E0E1D7D7D8CFCFCFC9C6C1DDCCAEBEA67CB4883CC99231CB
        9534CD9633B68A3ECDC9C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9F7E3D6BFCDAD77D0AE72EDE3
        D2F5F5F6ECECEDE9E9E9E4E4E5D9DADACACACCD1C7B7B9A176AF8337C28B2AC2
        8B29C38B27B28538CCC9C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F8F7EBE3D6E8E3
        DAF5F5F5F2F2F3F1F1F1EEEEEEEAEAEADBDBDCD7D2CABDB4A3A37930B68124B8
        8426BC8728AB8136CBC8C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9FAEEEE
        EEF1F1F1EDEDEDE9E9E9E5E5E5E1E1E1DCDCDDDBDBDDD1D0CFA69B87AB9C83AB
        9D84AC9D84A89D89DBDAD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEF1F1
        F1EDEDEDE8E8E8E2E2E3DEDEDED8D8D9D3D3D3CCCCCDDDDDDDFDFDFDFCFCFDFC
        FCFDFCFCFDFCFCFDFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F0F0F1F2F2
        F2ECECECE7E7E8E2E2E3DDDDDED8D8D8D1D1D2CBCBCCCBCBCCF5F5F5FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F2F4F4F4F2F2
        F2EEEEEEE9E9EAE5E5E5E0E0E0DBDBDBD4D4D5CCCCCDC6C6C6E5E5E5FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F0F7F7F7F3F3
        F3F0F0F0EBEBEBE7E7E7E2E2E3DEDEDED9D9DACFCFD0C5C5C6D8D8D9FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF0F0F0F7F7F8F5F5
        F5F1F1F1EDEDEDE9E9E9E5E5E5E1E1E1DDDDDDD5D5D6C6C6C7CFCFD0FDFDFDFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF2F2F2F8F8F8F6F6
        F6F2F2F3EFEFEFEBEBEBE7E7E8E4E4E4E0E0E0DBDBDCC9C9CACDCDCEFCFCFCFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8D1D1D2F8F8F8F7F7
        F7F4F4F4F1F1F1EFEFEFECECECE7E7E8E3E3E4E0E0E0CCCCCDB7B8B9FAFAFAFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBA8A9ABEEEFEFF9F9
        F9F7F7F7E6E6E6D0D0D1CECFD0E0E1E1E9E9E9E4E4E5C7C7C89D9EA0FCFCFCFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA0A1A3D6D6D7F6F6
        F7A3A4A75052563C3E433C3F4455575BABACAEE9E9E9B6B7B89FA0A2FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB0B0B386878A8182
        862D30364144495154585153583E414633363B949698818285B3B5B6FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBDBDC3E41451E21
        272D303633363B32353B32353B33363B2B2E3422252A44474BDDDEDEFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFD8E8F921C1F
        24181B21181B21181B21181B21181B21181B211C1F24929396FDFDFDFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F4F59798
        9B40424723262A1C1E231B1E2324272B4244499B9C9EF5F5F5FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFE9E9E9C9CACBB9BABBBABABCCACBCCEAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 49
      Top = 0
      Width = 49
      Height = 45
      Hint = 'Cadastrar Lote'
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      Glyph.Data = {
        360C0000424D360C000000000000360000002800000020000000200000000100
        180000000000000C0000C40E0000C40E00000000000000000000FFFFFFFDFDFD
        F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8
        F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F7F7F7C4DAD1AE
        CFC3AED0C3ADCFC3C4DAD1F7F7F7F8F8F8F8F8F8FDFDFDFFFFFFFFFFFFF4F4F4
        DDE1E4D8DDE1D8DDE1D8DDE1D8DDE1D8DDE1D8DDE1D8DDE1D8DDE1D8DDE1D8DD
        E1D8DDE1D8DDE1D8DDE1D8DDE1D8DDE1D8DDE1D8DDE1D9DEE1D5DADD54A78F1F
        A27F22A3801E9F7C53A58CD5DADDD9DEE1DDE1E4F4F4F4FFFFFFFEFEFEDEE2E5
        7E9EB36E94AD6F95AE6F95AE6F95AE6F95AE6F95AE6F95AE6F95AE7095AF7095
        AF7095AF7095AF7095AF7095AF7095AF7095AF7095AF7095AF6D93AB369C8821
        B59021B38F20B18D3599856D92AA6E94AD7E9EB3DEE2E5FEFEFEFCFCFCD3D8DB
        6E95AD6590AB6590AB6591AB6691AC6692AC6792AD6792AD6793AE6893AE6894
        AF6894AF6894AF6894AF6894AF6894AF6994B06993B06993AF6691AB39A19028
        BE9B28BC9A26BB97379E8D648EA96690AC6F95AED4D8DCFCFCFCFAFAFACED3D6
        7298B06995B06A95B06A96B16B96B16B97B26B97B36C98B36C98B46C99B46D99
        B46D99B46D99B56D99B56D99B56C99B45D97A55898A25999A25797A03CAB9731
        C6A630C4A42EC3A239A89354949C55959E5A969DAEC7C2F8F8F8FAFAFACED3D7
        759BB46E9AB56E9AB56E9BB66F9BB66F9CB7709CB8709DB8709DB9709EB9719E
        B9719EB9719EBA719EBA719EBA6E9DB643A89936C1A239C3A63BC4A83CCAAD39
        CDB038CCAF36CAAC36C4A633BD9E2FB9992BB59464B9A1F3F5F4FCFCFCD3D8DC
        7A9FB9719EB9729EB9729FBA729FBB73A0BB73A1BC74A1BD74A1BD74A2BD74A2
        BE74A2BE74A2BE74A2BE74A2BF72A1BB49B2A43ACFB23DD2B640D4BA41D6BB41
        D5BB40D4B93ED2B63ACEB236CAAC31C5A62DC09F66C1A9F5F7F6FFFFFFDCE1E4
        7DA4BD74A2BD75A2BD75A2BE75A3BF76A4BF76A4C076A4C076A5C176A5C177A5
        C277A6C277A6C277A6C277A6C274A4BF4EB7AB41D6BB45D9C048DCC449DEC649
        DEC648DCC445D9C041D5BB3CD0B436CBAD32C5A66CC7B0F7F9F8FFFFFFE5EAED
        81A9C177A5C077A5C077A5C178A6C278A6C278A6C378A7C378A7C478A8C479A8
        C579A8C579A8C579A8C579A9C576A7C252B8AD4BD4BC4ED7C152DAC556E1CC54
        E5D050E3CD4CE0C94CD8C046CFB641CAAF3CC5A873CBB5F9FBFAFFFFFFECF1F4
        85ACC579A7C279A7C379A7C379A8C479A8C579A9C57AA9C67AA9C67AAAC77AAA
        C77AAAC77AABC87AABC87AABC879AAC76BAABB68ADB969ADB967ACB75FCABF61
        ECDA5BE9D655E6D157C5B765A9B367A9B46DACB4C9E4DFFFFFFFFFFFFFEEF2F6
        87AFC77BA9C47BA9C57BA9C57BA9C67BAAC67BAAC77BAAC77BABC87BABC87BAC
        C97AACC97AACCA7AACCA7AACCA7AACCA7BACCA7BABC97CABC978A9C569CAC56E
        F1E265EDDD5DEAD65EC5BC79A7C27CA9C588AEC8F0F3F7FFFFFFFFFFFFEEF3F6
        89B0C97DABC67CABC67CAAC77CABC77CABC77CABC87CABC87BACC97BACC97BAC
        CA7AACCA7AADCA7AADCB7AADCB7AADCA7AACCA7BACCA7BACC978AAC56FCECA77
        F6E96BF0E160ECDA61C8C07AA9C37DABC689B0C9EEF3F6FFFFFFFFFFFFEEF3F6
        8BB2CA7EACC77EACC87DACC87DACC87DACC87DACC97DACC97CACC97CACC97CAC
        CA7CADCA7BADCA7BADCA7BADCA7BADCA7CADCA7CACCA7CACC979ABC66CCBC674
        ECDE6CE8D863E4D262C6BE7BABC57FACC78BB2CAEEF3F6FFFFFFFFFFFFEEF3F7
        8CB3CC80AEC980AEC980AEC97FAEC97FADC97EADC97EADC97EADCA7EADCA7DAD
        CA7DADCA7DADCA7DADCA7DADCA7DADCA7DADCA7DADCA7EADCA7DADC972B1C271
        B5C171B4C170B4C072B1C17FAEC880AEC98CB3CCEEF3F7FFFFFFFFFFFFEFF3F7
        8FB5CE83B0CA82B0CA82B0CA82AFCA81AFCA81AFCB80AFCB80AFCB80AECB7FAE
        CB7FAECB7FAECB7FAECB7FAECB7FAECB7FAECB7FAECB80AECB80AFCB81AECB81
        AECB82AFCB82AFCB83AFCB82B0CA83B0CA8FB5CEEFF3F7FFFFFFFFFFFFEFF4F7
        92B8CF86B2CC85B2CC85B2CC84B2CC84B1CC84B1CC83B1CC83B1CC83B1CC82B1
        CC82B0CC82B0CC81B0CC81B0CC82B0CC82B0CC82B1CC83B1CC83B1CC83B1CC84
        B1CC84B1CC84B2CC85B2CC85B2CC86B2CC92B8CFEFF4F7FFFFFFFFFFFFEFF4F7
        94BAD189B5CF89B4CF88B4CF88B4CF87B4CE87B4CE86B3CE86B3CE86B3CE86B3
        CE85B3CE88B5CF8BB7D08BB7D088B5CF85B3CE86B3CE86B3CE86B3CE86B3CE87
        B4CE87B4CE88B4CF88B4CF89B4CF89B5CF94BAD1EFF4F7FFFFFFFFFFFFF0F4F8
        98BDD48DB9D28DB8D28CB8D28CB7D18BB7D18BB7D18BB7D18AB7D18AB6D18AB6
        D189B6D09EC3D9B3D0E1B3D0E19EC3D989B6D08AB6D18AB6D18AB7D18BB7D18B
        B7D18BB7D18CB7D18CB8D28DB8D28DB9D298BDD4F0F4F8FFFFFFFFFFFFF0F5F8
        9BBFD692BCD691BCD591BBD590BBD590BBD48FBBD48FBAD48FBAD48EBAD48EBA
        D48DB9D3A5C8DCBCD6E5BCD6E5A5C8DC8DB9D38EBAD48EBAD48FBAD48FBAD48F
        BBD490BBD490BBD591BBD591BCD592BCD69BBFD6F0F5F8FFFFFFFFFFFFF0F5F8
        9DC1D896BFD897C0D897C0D897C0D897C0D897BFD896BFD896BFD896BFD896BF
        D895BED7ABCBDFC0D9E7C0D9E7ABCBDF95BED796BFD896BFD896BFD896BFD897
        BFD897C0D897C0D897C0D897C0D896BFD89DC1D8F0F5F8FFFFFFFFFFFFF5F8FA
        9CBBD08AAFC98CB0CA8CB0CA8CB0CA8CB0CA8CB0CA8CB0CA8CB0CA8CB0CA8CB0
        CA8CB0C99EBCD2AFC8DAB1C9DB9EBCD28CB0C98CB0CA8CB0CA8CB0CA8CB0CA8C
        B0CA8CB0CA8CB0CA8CB0CA8CB0CA8AAFC99CBBD0F5F8FAFFFFFFFFFFFFFFFFFF
        BCD0DF83A9C585AAC685AAC685AAC685AAC685AAC685AAC685AAC685AAC685AA
        C684AAC693B4CDA0BDD3A3BFD493B4CD84AAC685AAC685AAC685AAC685AAC685
        AAC685AAC685AAC685AAC685AAC683A9C5BCD0DFFFFFFFFFFFFFFFFFFFFFFFFF
        E4ECF28FB2CC8AB0CB89AFCB89AFCB89AFCB89AFCB89AFCB89AFCB89AFCB89AF
        CB89AFCA94B7CF9FBDD3A1BFD594B7CF89AFCA89AFCB89AFCB89AFCB89AFCB89
        AFCB89AFCB89AFCB89AFCB8AB0CB8FB2CCE4ECF2FFFFFFFFFFFFFFFFFFFFFFFF
        FBFCFDACC7DA8CB4CE8DB4CF8DB4CF8DB4CF8DB4CF8DB4CF8DB4CF8DB4CF8DB4
        CF8CB4CE95B9D29CBED49EC0D695B9D28CB4CE8DB4CF8DB4CF8DB4CF8DB4CF8D
        B4CF8DB4CF8DB4CF8DB4CF8CB4CEACC7DAFBFCFDFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFD3E1EB90B8D291B9D391B9D391B9D391B9D391B9D391B9D391B9D391B9
        D390B9D396BCD59ABFD69CC0D796BCD590B9D391B9D391B9D391B9D391B9D391
        B9D391B9D391B9D391B9D390B8D2D3E1EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFF3F7F9A7C7DB95BDD696BED696BED696BED696BED696BED696BED696BE
        D696BDD697BFD79AC0D79BC1D897BFD796BDD696BED696BED696BED696BED696
        BED696BED696BED695BCD6A8C7DBF2F6F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFEDF3F7E0EAF2E0EAF2E0EAF1E0EAF1E0EAF1E0EAF1E0EAF1E0EA
        F1E0EAF1E0EAF1E0EAF1E0EBF2E0EAF1E0EAF1E0EAF1E0EAF1E0EAF1E0EAF1E0
        EAF1E0EAF1E0EAF2E0EAF2EEF4F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButton2Click
      ExplicitLeft = 117
      ExplicitTop = -5
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 509
    Width = 1241
    ExplicitTop = 509
    ExplicitWidth = 1241
  end
  inherited Panel3: TPanel
    Top = 89
    Width = 1241
    Height = 283
    ExplicitTop = 89
    ExplicitWidth = 1241
    ExplicitHeight = 283
    inherited JvDBGrid1: TJvDBGrid
      Width = 241
      Height = 283
      Align = alLeft
      DataSource = DsEntidade
      TitleRowHeight = 21
      Columns = <
        item
          Expanded = False
          FieldName = 'ENT_ID'
          Title.Caption = 'C'#243'd. Cliente'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME'
          Title.Caption = 'Cliente'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 129
          Visible = True
        end>
    end
    object JvDBGrid2: TJvDBGrid
      Left = 241
      Top = 0
      Width = 544
      Height = 283
      Align = alLeft
      DataSource = DsLote
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Segoe UI'
      TitleFont.Style = []
      OnDrawColumnCell = JvDBGrid2DrawColumnCell
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 21
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOTE_NUMERO'
          Title.Alignment = taCenter
          Title.Caption = 'N'#186
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 63
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOTE_DESCRICAO'
          Title.Caption = 'Descri'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOTE_VALOR'
          Title.Caption = 'Vlr. Devedor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTALRECEB'
          Title.Caption = 'Vlr. Recebido'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SALDO'
          Title.Caption = 'Saldo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HAVER'
          Title.Caption = 'Haver'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end>
    end
    object JvDBGrid3: TJvDBGrid
      Left = 785
      Top = 0
      Width = 456
      Height = 283
      Align = alClient
      DataSource = DsCC
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ParentFont = False
      PopupMenu = PopupOpcoes
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Segoe UI'
      TitleFont.Style = []
      OnDrawColumnCell = JvDBGrid3DrawColumnCell
      OnDblClick = JvDBGrid3DblClick
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 21
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CC_CONFERIDO'
          Title.Alignment = taCenter
          Title.Caption = 'Conferido'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CC_SEQ'
          Title.Caption = 'Seq.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 34
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CC_TIPO'
          Title.Alignment = taCenter
          Title.Caption = 'Tipo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 37
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CC_TIPO_TRANSACAO'
          Title.Alignment = taCenter
          Title.Caption = 'Transa'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 97
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CC_DATALANCAMENTO'
          Title.Alignment = taCenter
          Title.Caption = 'Data'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CC_DESCRICAO'
          Title.Caption = 'Descri'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 231
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CC_VALOR'
          Title.Caption = 'Valor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Top = 477
    Width = 1241
    ExplicitTop = 477
    ExplicitWidth = 1241
    inherited pnPesquisar: TPanel
      Left = 821
      ExplicitLeft = 821
      ExplicitHeight = 32
    end
    inherited pnNovo: TPanel
      Left = 926
      ExplicitLeft = 926
      ExplicitHeight = 32
    end
    inherited pnImprimir: TPanel
      Left = 1031
      Caption = 'IMPRIMIR'
      ExplicitLeft = 1031
      ExplicitHeight = 32
      object JvSpeedButton1: TJvSpeedButton
        Left = 84
        Top = 0
        Width = 21
        Height = 32
        Align = alRight
        DropDownMenu = PopupImprimir
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Segoe UI'
        HotTrackFont.Style = []
        ParentFont = False
        Transparent = True
        ExplicitTop = 5
        ExplicitHeight = 27
      end
    end
    inherited pnFechar: TPanel
      Left = 1136
      ExplicitLeft = 1136
      ExplicitHeight = 32
    end
  end
  inherited Panel1: TPanel
    Width = 1241
    Height = 89
    ExplicitWidth = 1241
    ExplicitHeight = 89
    object GroupBox3: TGroupBox
      Left = 365
      Top = 0
      Width = 180
      Height = 89
      Align = alLeft
      Caption = 'Pesquisar por'
      TabOrder = 0
      object edDescricao: TEdit
        Left = 10
        Top = 53
        Width = 160
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnKeyDown = edDescricaoKeyDown
      end
      object cbPesquisar: TComboBox
        Left = 10
        Top = 22
        Width = 160
        Height = 25
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ItemIndex = 1
        ParentFont = False
        TabOrder = 1
        Text = 'Numero do Lote'
        OnChange = cbPesquisarChange
        Items.Strings = (
          'Descri'#231#227'o do Lote'
          'Numero do Lote'
          'Valor de Fechamento')
      end
      object edValor: TJvCalcEdit
        Left = 10
        Top = 53
        Width = 83
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 2
        Visible = False
        DecimalPlacesAlwaysShown = False
        OnKeyDown = edValorKeyDown
      end
    end
    object rgFiltrar: TRadioGroup
      Left = 0
      Top = 0
      Width = 104
      Height = 89
      Align = alLeft
      Caption = ' Filtrar por '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemIndex = 1
      Items.Strings = (
        'Cadastro'
        'Lan'#231'amento'
        'Fechamento')
      ParentFont = False
      TabOrder = 1
    end
    object GroupBox4: TGroupBox
      Left = 104
      Top = 0
      Width = 116
      Height = 89
      Align = alLeft
      Caption = ' Per'#237'odo '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object Label8: TLabel
        Left = 54
        Top = 42
        Width = 7
        Height = 17
        Alignment = taRightJustify
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edDataFim: TJvDateEdit
        Left = 6
        Top = 59
        Width = 105
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 1
      end
      object edDataIni: TJvDateEdit
        Left = 6
        Top = 20
        Width = 105
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
      end
    end
    object GroupBox6: TGroupBox
      Left = 220
      Top = 0
      Width = 145
      Height = 89
      Align = alLeft
      Caption = ' Transa'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object Label1: TLabel
        Left = 128
        Top = 32
        Width = 27
        Height = 17
        Caption = 'TIPO'
        FocusControl = DBEdit4
      end
      object chkTodos5: TCheckBox
        Left = 83
        Top = 1
        Width = 55
        Height = 17
        Caption = 'Todos'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 0
      end
      object cklAluno_Atuacao: TCheckListBox
        Left = 2
        Top = 19
        Width = 141
        Height = 68
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Items.Strings = (
          'CHEQUE'
          'DEP'#211'SITO'
          'DINHEIRO'
          'DOC'
          'PIX'
          'TED')
        ParentFont = False
        TabOrder = 1
      end
      object DBEdit4: TDBEdit
        Left = 128
        Top = 72
        Width = 21
        Height = 25
        DataField = 'TIPO'
        DataSource = DsExtrato
        TabOrder = 2
      end
    end
  end
  object Panel5: TPanel [5]
    Left = 0
    Top = 372
    Width = 1241
    Height = 60
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    object gbCliente: TGroupBox
      Left = 1
      Top = 1
      Width = 174
      Height = 58
      Align = alLeft
      Caption = ' Total Devido '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object DBEdit1: TDBEdit
        Left = 12
        Top = 17
        Width = 153
        Height = 36
        DataField = 'CliTotDevido'
        DataSource = DsLote
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -20
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
    end
    object GroupBox1: TGroupBox
      Left = 175
      Top = 1
      Width = 174
      Height = 58
      Align = alLeft
      Caption = ' Total Recebido '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object DBEdit2: TDBEdit
        Left = 12
        Top = 17
        Width = 153
        Height = 36
        DataField = 'CliTotRecebido'
        DataSource = DsLote
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -20
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
    end
    object GroupBox2: TGroupBox
      Left = 349
      Top = 1
      Width = 174
      Height = 58
      Align = alLeft
      Caption = ' Saldo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      object DBEdit3: TDBEdit
        Left = 12
        Top = 17
        Width = 153
        Height = 36
        DataField = 'CliSaldo'
        DataSource = DsLote
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
    end
    object GroupBox7: TGroupBox
      Left = 1066
      Top = 1
      Width = 174
      Height = 58
      Align = alRight
      Caption = ' Total Recebido Per'#237'odo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      object edTotRecebidoPeriodo: TJvCalcEdit
        Left = 2
        Top = 19
        Width = 170
        Height = 37
        Align = alClient
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        ShowButton = False
        TabOrder = 0
        DecimalPlacesAlwaysShown = False
        ExplicitHeight = 36
      end
    end
  end
  inherited Timer1: TTimer
    Left = 775
  end
  object FDEntidade: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select distinct e.ent_id, e.ent_nome                            ' +
        '           '
      
        'from entidade e                                                 ' +
        '           '
      
        'inner join lote l on e.ent_id=l.ent_id                          ' +
        '           '
      
        'inner join contacorrente cc on l.ent_id=cc.ent_id and l.lote_id=' +
        'cc.lote_id')
    Left = 816
    Top = 8
    object FDEntidadeENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDEntidadeENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      Origin = 'ENT_NOME'
      Size = 250
    end
  end
  object ProviderEntidade: TDataSetProvider
    DataSet = FDEntidade
    Left = 848
    Top = 8
  end
  object CdsEntidade: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderEntidade'
    Left = 880
    Top = 8
    object CdsEntidadeENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Required = True
    end
    object CdsEntidadeENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      Size = 250
    end
  end
  object DsEntidade: TDataSource
    DataSet = CdsEntidade
    OnDataChange = DsEntidadeDataChange
    Left = 912
    Top = 8
  end
  object FDLote: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select ent_id, lote_id, lote_numero, lote_descricao, lote_fecham' +
        'ento, lote_valor,'
      '(totalRecebAnterior-LoteAnterior) SaldoAnterior,'
      
        'iif((totalReceb+totalRecebAnterior-LoteAnterior-lote_valor)>0,lo' +
        'te_valor,totalReceb+totalRecebAnterior-LoteAnterior) totalReceb,'
      
        'iif((lote_valor-totalReceb-totalRecebAnterior+LoteAnterior)<0,0,' +
        '(lote_valor-totalReceb-totalRecebAnterior+LoteAnterior)) Saldo,'
      
        'iif((lote_valor-totalReceb-totalRecebAnterior+LoteAnterior)<0,(t' +
        'otalReceb-lote_valor+totalRecebAnterior-LoteAnterior),0) Haver'
      'from('
      
        'select l.ent_id ent_id, l.lote_id lote_id, l.lote_numero lote_nu' +
        'mero, l.lote_descricao lote_descricao,'
      'l.lote_datafechamento lote_fechamento, l.lote_valor lote_valor,'
      '('
      'select Coalesce(sum(cc2.cc_valor),0)'
      'from contacorrente cc2'
      
        'inner join lote l2 on cc2.ent_id=l2.ent_id and cc2.lote_id=l2.lo' +
        'te_id'
      
        'inner join entidade e2 on cc2.ent_id=e2.ent_id and cc2.lote_id=l' +
        '2.lote_id'
      
        'where e2.ent_id=l.ent_id and l2.lote_datafechamento < l.lote_dat' +
        'afechamento'
      ') totalRecebAnterior,'
      '(select Coalesce(sum(l3.lote_valor),0)'
      'from lote l3'
      'inner join entidade e3 on l3.ent_id=e3.ent_id'
      
        'where e3.ent_id=l.ent_id and l3.lote_datafechamento < l.lote_dat' +
        'afechamento'
      ') LoteAnterior,'
      '('
      'select Coalesce(sum(cc.cc_valor),0)'
      'from contacorrente cc'
      
        'inner join entidade e on cc.ent_id=e.ent_id and cc.lote_id=l.lot' +
        'e_id'
      'where e.ent_id=l.ent_id'
      ') totalReceb'
      'from lote l'
      'order by l.ent_id, l.lote_datafechamento)'
      
        'group by ent_id, lote_id, lote_numero, lote_descricao, lote_fech' +
        'amento, lote_valor, totalRecebAnterior,LoteAnterior, totalReceb,' +
        ' Saldo, Haver')
    Left = 1040
    Top = 8
    object FDLoteENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDLoteLOTE_ID: TIntegerField
      FieldName = 'LOTE_ID'
      Origin = 'LOTE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDLoteLOTE_NUMERO: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'LOTE_NUMERO'
      Origin = 'LOTE_NUMERO'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDLoteLOTE_DESCRICAO: TStringField
      FieldName = 'LOTE_DESCRICAO'
      Origin = 'LOTE_DESCRICAO'
      Size = 255
    end
    object FDLoteLOTE_VALOR: TFloatField
      FieldName = 'LOTE_VALOR'
      Origin = 'LOTE_VALOR'
    end
    object FDLoteLOTE_FECHAMENTO: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'LOTE_FECHAMENTO'
      Origin = 'LOTE_FECHAMENTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDLoteTOTALRECEB: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTALRECEB'
      Origin = 'TOTALRECEB'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDLoteSALDO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'SALDO'
      Origin = 'SALDO'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDLoteHAVER: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'HAVER'
      Origin = 'HAVER'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDLoteSALDOANTERIOR: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'SALDOANTERIOR'
      Origin = 'SALDOANTERIOR'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object ProviderLote: TDataSetProvider
    DataSet = FDLote
    Left = 1072
    Top = 8
  end
  object CdsLote: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Filtered = True
    Params = <>
    ProviderName = 'ProviderLote'
    Left = 1104
    Top = 8
    object CdsLoteENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Required = True
    end
    object CdsLoteLOTE_ID: TIntegerField
      FieldName = 'LOTE_ID'
      Required = True
    end
    object CdsLoteLOTE_NUMERO: TIntegerField
      FieldName = 'LOTE_NUMERO'
      ReadOnly = True
    end
    object CdsLoteLOTE_DESCRICAO: TStringField
      FieldName = 'LOTE_DESCRICAO'
      Size = 255
    end
    object CdsLoteLOTE_VALOR: TFloatField
      DefaultExpression = '0'
      FieldName = 'LOTE_VALOR'
      DisplayFormat = '#,##0.00'
    end
    object CdsLoteLOTE_FECHAMENTO: TSQLTimeStampField
      FieldName = 'LOTE_FECHAMENTO'
      ReadOnly = True
    end
    object CdsLoteTOTALRECEB: TFloatField
      DefaultExpression = '0'
      FieldName = 'TOTALRECEB'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object CdsLoteSALDO: TFloatField
      DefaultExpression = '0'
      FieldName = 'SALDO'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object CdsLoteHAVER: TFloatField
      DefaultExpression = '0'
      FieldName = 'HAVER'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object CdsLoteSALDOANTERIOR: TFloatField
      FieldName = 'SALDOANTERIOR'
      ReadOnly = True
    end
    object CdsLoteCliTotDevido: TAggregateField
      DefaultExpression = '0'
      FieldName = 'CliTotDevido'
      Active = True
      DisplayName = ''
      DisplayFormat = '#,##0.00'
      Expression = 'Sum(Lote_Valor)'
    end
    object CdsLoteCliTotRecebido: TAggregateField
      DefaultExpression = '0'
      FieldName = 'CliTotRecebido'
      Active = True
      DisplayName = ''
      DisplayFormat = '#,##0.00'
      Expression = 'Sum(TotalReceb)'
    end
    object CdsLoteCliSaldo: TAggregateField
      FieldName = 'CliSaldo'
      Active = True
      DisplayName = ''
      DisplayFormat = '#,##0.00'
      Expression = 'Sum(Lote_Valor)-Sum(TotalReceb)'
    end
  end
  object DsLote: TDataSource
    DataSet = CdsLote
    OnDataChange = DsLoteDataChange
    Left = 1136
    Top = 8
  end
  object FDCC: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select cc.*'
      'from contacorrente cc'
      'order by cc.ent_id, cc.lote_id, cc.cc_datalancamento, cc.cc_seq')
    Left = 816
    Top = 56
    object FDCCENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDCCLOTE_ID: TIntegerField
      FieldName = 'LOTE_ID'
      Origin = 'LOTE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDCCCC_SEQ: TIntegerField
      FieldName = 'CC_SEQ'
      Origin = 'CC_SEQ'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDCCCC_TIPO: TIntegerField
      FieldName = 'CC_TIPO'
      Origin = 'CC_TIPO'
    end
    object FDCCCC_TIPO_TRANSACAO: TIntegerField
      FieldName = 'CC_TIPO_TRANSACAO'
      Origin = 'CC_TIPO_TRANSACAO'
    end
    object FDCCCC_DESCRICAO: TStringField
      FieldName = 'CC_DESCRICAO'
      Origin = 'CC_DESCRICAO'
      Size = 255
    end
    object FDCCCC_DATA: TSQLTimeStampField
      FieldName = 'CC_DATA'
      Origin = 'CC_DATA'
    end
    object FDCCCC_VALOR: TFloatField
      FieldName = 'CC_VALOR'
      Origin = 'CC_VALOR'
    end
    object FDCCCC_DATALANCAMENTO: TSQLTimeStampField
      FieldName = 'CC_DATALANCAMENTO'
      Origin = 'CC_DATALANCAMENTO'
    end
    object FDCCCC_CONFERIDO: TIntegerField
      FieldName = 'CC_CONFERIDO'
      Origin = 'CC_CONFERIDO'
      Required = True
    end
  end
  object ProviderCC: TDataSetProvider
    DataSet = FDCC
    Left = 848
    Top = 56
  end
  object CdsCC: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Filtered = True
    Params = <>
    ProviderName = 'ProviderCC'
    OnCalcFields = CdsCCCalcFields
    Left = 880
    Top = 56
    object CdsCCENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Required = True
    end
    object CdsCCLOTE_ID: TIntegerField
      FieldName = 'LOTE_ID'
      Required = True
    end
    object CdsCCCC_SEQ: TIntegerField
      FieldName = 'CC_SEQ'
      Required = True
    end
    object CdsCCCC_TIPO: TIntegerField
      FieldName = 'CC_TIPO'
      OnGetText = CdsCCCC_TIPOGetText
    end
    object CdsCCCC_TIPO_TRANSACAO: TIntegerField
      FieldName = 'CC_TIPO_TRANSACAO'
      OnGetText = CdsCCCC_TIPO_TRANSACAOGetText
    end
    object CdsCCCC_DESCRICAO: TStringField
      FieldName = 'CC_DESCRICAO'
      Size = 255
    end
    object CdsCCCC_DATA: TSQLTimeStampField
      FieldName = 'CC_DATA'
      OnGetText = CdsCCCC_DATAGetText
    end
    object CdsCCCC_VALOR: TFloatField
      FieldName = 'CC_VALOR'
      DisplayFormat = '#,##0.00'
    end
    object CdsCCCC_DATALANCAMENTO: TSQLTimeStampField
      FieldName = 'CC_DATALANCAMENTO'
    end
    object CdsCCCC_CONFERIDO: TIntegerField
      FieldName = 'CC_CONFERIDO'
      Required = True
      OnGetText = CdsCCCC_CONFERIDOGetText
    end
  end
  object DsCC: TDataSource
    DataSet = CdsCC
    Left = 912
    Top = 56
  end
  object frxImpLote: TfrxDBDataset
    UserName = 'frxImpLote'
    CloseDataSource = False
    FieldAliases.Strings = (
      'data=data'
      'saldoanterior=saldoanterior'
      'debito=debito'
      'credito=credito'
      'saldo=saldo'
      'tipo=tipo'
      'descricao=descricao'
      'lote_id=lote_id'
      'lote_numero=lote_numero'
      'lote_descricao=lote_descricao'
      'lote_fechamento=lote_fechamento'
      'lote_valor=lote_valor'
      'TotalRecebLote=TotalRecebLote'
      'SaldoLote=SaldoLote'
      'HaverLote=HaverLote')
    DataSet = CdsImpCC
    BCDToCurrency = False
    Left = 680
    Top = 16
  end
  object FDImpLote: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select l.ent_id, l.lote_id, l.lote_numero, e.ent_nome, l.lote_da' +
        'tafechamento, l.lote_valor, l.lote_descricao,'
      
        'cc.cc_seq, cc.cc_tipo_transacao, cc.cc_descricao, cc.cc_valor, c' +
        'c.cc_datalancamento'
      'from lote l'
      
        'left outer join contacorrente cc on l.ent_id=cc.ent_id and l.lot' +
        'e_id=cc.lote_id'
      'left outer join entidade e on l.ent_id=e.ent_id'
      'where l.lote_numero = :k_lote_numero'
      'order by cc.ent_id, cc.lote_id, cc.cc_datalancamento, cc.cc_seq')
    Left = 576
    Top = 16
    ParamData = <
      item
        Name = 'K_LOTE_NUMERO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object FDImpLoteENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDImpLoteLOTE_ID: TIntegerField
      FieldName = 'LOTE_ID'
      Origin = 'LOTE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDImpLoteLOTE_NUMERO: TIntegerField
      FieldName = 'LOTE_NUMERO'
      Origin = 'LOTE_NUMERO'
    end
    object FDImpLoteENT_NOME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ENT_NOME'
      Origin = 'ENT_NOME'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object FDImpLoteLOTE_DATAFECHAMENTO: TSQLTimeStampField
      FieldName = 'LOTE_DATAFECHAMENTO'
      Origin = 'LOTE_DATAFECHAMENTO'
    end
    object FDImpLoteLOTE_VALOR: TFloatField
      FieldName = 'LOTE_VALOR'
      Origin = 'LOTE_VALOR'
    end
    object FDImpLoteLOTE_DESCRICAO: TStringField
      FieldName = 'LOTE_DESCRICAO'
      Origin = 'LOTE_DESCRICAO'
      Size = 255
    end
    object FDImpLoteCC_SEQ: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'CC_SEQ'
      Origin = 'CC_SEQ'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDImpLoteCC_TIPO_TRANSACAO: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'CC_TIPO_TRANSACAO'
      Origin = 'CC_TIPO_TRANSACAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDImpLoteCC_DESCRICAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CC_DESCRICAO'
      Origin = 'CC_DESCRICAO'
      ProviderFlags = []
      ReadOnly = True
      Size = 255
    end
    object FDImpLoteCC_VALOR: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'CC_VALOR'
      Origin = 'CC_VALOR'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDImpLoteCC_DATALANCAMENTO: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'CC_DATALANCAMENTO'
      Origin = 'CC_DATALANCAMENTO'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object CdsImpCC: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 648
    Top = 16
    object CdsImpCCdata: TDateField
      FieldName = 'data'
    end
    object CdsImpCCsaldoanterior: TFloatField
      FieldName = 'saldoanterior'
    end
    object CdsImpCCdebito: TFloatField
      FieldName = 'debito'
    end
    object CdsImpCCcredito: TFloatField
      FieldName = 'credito'
    end
    object CdsImpCCsaldo: TFloatField
      FieldName = 'saldo'
    end
    object CdsImpCCtipo: TIntegerField
      FieldName = 'tipo'
    end
    object CdsImpCCdescricao: TStringField
      FieldName = 'descricao'
      Size = 255
    end
    object CdsImpCClote_id: TIntegerField
      FieldName = 'lote_id'
    end
    object CdsImpCClote_numero: TIntegerField
      FieldName = 'lote_numero'
    end
    object CdsImpCClote_descricao: TStringField
      FieldName = 'lote_descricao'
      Size = 255
    end
    object CdsImpCClote_fechamento: TDateField
      FieldName = 'lote_fechamento'
    end
    object CdsImpCClote_valor: TFloatField
      FieldName = 'lote_valor'
    end
    object CdsImpCCTotalRecebLote: TFloatField
      FieldName = 'TotalRecebLote'
    end
    object CdsImpCCSaldoLote: TFloatField
      FieldName = 'SaldoLote'
    end
    object CdsImpCCHaverLote: TFloatField
      FieldName = 'HaverLote'
    end
  end
  object PopupImprimir: TPopupMenu
    Left = 984
    Top = 8
    object Imprimirlanamentosporlote1: TMenuItem
      Caption = 'Imprimir lan'#231'amentos por lote'
      OnClick = Imprimirlanamentosporlote1Click
    end
    object ImprimirlanamentostipoCC1: TMenuItem
      Caption = 'Imprimir lan'#231'amentos tipo C/C'
      OnClick = ImprimirlanamentostipoCC1Click
    end
  end
  object frxExtrato: TfrxDBDataset
    UserName = 'frxExtrato'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIPO=TIPO'
      'DATA=DATA'
      'DESCRICAO=DESCRICAO'
      'LOTE_VALOR=LOTE_VALOR'
      'CONFERIDO=CONFERIDO'
      'ORDEM=ORDEM')
    DataSet = CdsExtrato
    BCDToCurrency = False
    Left = 712
    Top = 72
  end
  object CdsExtrato: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderExtrato'
    Left = 640
    Top = 72
    object CdsExtratoTIPO: TStringField
      FieldName = 'TIPO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object CdsExtratoDATA: TSQLTimeStampField
      FieldName = 'DATA'
    end
    object CdsExtratoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 255
    end
    object CdsExtratoLOTE_VALOR: TFloatField
      FieldName = 'LOTE_VALOR'
    end
    object CdsExtratoCONFERIDO: TIntegerField
      FieldName = 'CONFERIDO'
      Required = True
    end
    object CdsExtratoORDEM: TIntegerField
      FieldName = 'ORDEM'
    end
  end
  object FDExtrato: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select '#39'C'#39' tipo, cc.cc_data data, cc.cc_descricao descricao, cc.' +
        'cc_valor lote_valor, cc.cc_conferido conferido,'
      'cc.cc_ordem ordem'
      'from contacorrente cc'
      'inner join entidade e on cc.ent_id=e.ent_id'
      'where cc.ent_id = :k_ent_id'
      ''
      'union'
      ''
      
        'select '#39'D'#39' tipo, l.lote_datafechamento data, l.lote_descricao de' +
        'scricao, (l.lote_valor*-1) lote_valor, 0 conferido,'
      '0 ordem'
      'from lote l'
      'inner join entidade e2 on l.ent_id=e2.ent_id'
      'where l.ent_id = :k_ent_id'
      'order by 2')
    Left = 576
    Top = 72
    ParamData = <
      item
        Name = 'K_ENT_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object FDExtratoTIPO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TIPO'
      Origin = 'TIPO'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object FDExtratoDATA: TSQLTimeStampField
      FieldName = 'DATA'
      Origin = 'CC_DATA'
    end
    object FDExtratoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'CC_DESCRICAO'
      Size = 255
    end
    object FDExtratoLOTE_VALOR: TFloatField
      FieldName = 'LOTE_VALOR'
      Origin = 'CC_VALOR'
    end
    object FDExtratoCONFERIDO: TIntegerField
      FieldName = 'CONFERIDO'
      Origin = 'CC_CONFERIDO'
      Required = True
    end
    object FDExtratoORDEM: TIntegerField
      FieldName = 'ORDEM'
      Origin = 'CC_ORDEM'
    end
  end
  object ProviderExtrato: TDataSetProvider
    DataSet = FDExtrato
    Left = 608
    Top = 72
  end
  object DsExtrato: TDataSource
    DataSet = CdsExtrato
    Left = 672
    Top = 72
  end
  object PopupOpcoes: TPopupMenu
    Left = 984
    Top = 56
    object AlterarparaConferido1: TMenuItem
      Caption = 'Alterar para Conferido/N'#227'o Conferido'
      OnClick = AlterarparaConferido1Click
    end
  end
end
