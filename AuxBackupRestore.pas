unit AuxBackupRestore;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AuxPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.ComCtrls, FireDAC.Phys, FireDAC.Phys.IBBase,
  FireDAC.Phys.FB, FireDAC.Stan.Intf, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvSpin, IniFiles;

type
  TF_AuxBackupRestore = class(TF_AuxPadrao)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edBkpCaminhoCopiaBD: TEdit;
    edBkpHost: TEdit;
    edBkpDatabase: TEdit;
    edBkpBackup: TEdit;
    mBkpProgresso: TMemo;
    FDIBBackup1: TFDIBBackup;
    FDIBRestore1: TFDIBRestore;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    procedure FDIBBackup1AfterExecute(Sender: TObject);
    procedure FDIBBackup1BeforeExecute(Sender: TObject);
    procedure FDIBBackup1Error(ASender: TObject;
      const AInitiator: IFDStanObject; var AException: Exception);
    procedure FDIBBackup1Progress(ASender: TFDPhysDriverService;
      const AMessage: string);
    procedure FormActivate(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_AuxBackupRestore: TF_AuxBackupRestore;

implementation

{$R *.dfm}

uses X;

procedure TF_AuxBackupRestore.FDIBBackup1AfterExecute(Sender: TObject);
begin
  inherited;
  mBkpProgresso.Lines.Add('Backup conclu�do com sucesso!');
end;

procedure TF_AuxBackupRestore.FDIBBackup1BeforeExecute(Sender: TObject);
begin
  inherited;
  mBkpProgresso.Clear;
end;

procedure TF_AuxBackupRestore.FDIBBackup1Error(ASender: TObject;
  const AInitiator: IFDStanObject; var AException: Exception);
begin
  inherited;
  mBkpProgresso.Lines.Add('Erro');
  mBkpProgresso.Lines.Add(AException.Message);
end;

procedure TF_AuxBackupRestore.FDIBBackup1Progress(ASender: TFDPhysDriverService;
  const AMessage: string);
begin
  inherited;
  mBkpProgresso.Lines.Add(AMessage);
end;

procedure TF_AuxBackupRestore.FormActivate(Sender: TObject);
var
  vArqIni    : TIniFile;
  Aplicativo : String;
  Caminho    : String;
  ArqGdb     : String;
  Conexao    : String;
  Backup     : String;
  CaminhoBkp : String;
begin
  inherited;
  Aplicativo := 'backup';

  Caminho    := ExtractFilePath(Application.ExeName);
  try
    if not(fileexists( Caminho + Aplicativo + '.Ini' ) ) then begin
      Conexao := Caminho + ArqGdb;
      vArqIni := TIniFile.Create( Caminho + Aplicativo +  '.Ini' );
      try
        vArqIni.ReadString('backup', 'Backup', '');
      finally
        vArqIni.Free;
      end;
    end else begin
      vArqIni   := TIniFile.Create( Caminho + Aplicativo +  '.Ini' );
      try
        if vArqIni.ReadString('backup', 'Backup', '')<>'' then begin
          edBkpDatabase.Text       := vArqIni.ReadString('basedados', 'BaseDados', '');
          edBkpBackup.Text         := vArqIni.ReadString('backup', 'Backup', '');
          edBkpCaminhoCopiaBD.Text := edBkpBackup.Text+vArqIni.ReadString('bancodados', 'Dados', '');;
          edBkpHost.Text           := vArqIni.ReadString('host', 'Host', '');
        end;
      finally
        vArqIni.Free;
      end;
    end;
  except
    msgPerson('Erro ao tentar conectar com o banco de dados!',[mbok],'Aten��o',mtError);
    Application.Terminate;
  end;
end;

procedure TF_AuxBackupRestore.icoCancelarClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TF_AuxBackupRestore.icoSalvarClick(Sender: TObject);
var
  x : String;
begin
  inherited;
  if PageControl1.ActivePageIndex = 0 then begin
    //x := edBkpBackup.Text+InverterPalavras(Copy(InverterPalavras(edBkpDatabase.Text),1,Pos('\',InverterPalavras(edBkpDatabase.Text))-1));
    //CopyFile(PChar('\\'+edBkpHost.Text+edBkpDatabase.Text),PChar(x),False);
    if edBkpHost.Text = '127.0.0.1' then
      CopyFile(PChar('C:\\'+edBkpDatabase.Text),PChar(edBkpCaminhoCopiaBD.Text),False)
    else
      CopyFile(PChar('\\'+edBkpHost.Text+edBkpDatabase.Text),PChar(edBkpCaminhoCopiaBD.Text),False);


    FDIBBackup1.DriverLink := FDPhysFBDriverLink1;

    FDIBBackup1.UserName   := 'SYSDBA';
    FDIBBackup1.Password   := 'masterkey';
    FDIBBackup1.Host       := edBkpHost.Text;
    FDIBBackup1.Protocol   := ipLocal;

    FDIBBackup1.Database   := edBkpCaminhoCopiaBD.Text;
    FDIBBackup1.BackupFiles.Add(edBkpBackup.Text+FormatDateTime('ddmmyyyyhhnn', Now)+'.fbk');

    FDIBBackup1.Backup;

    Winapi.Windows.DeleteFile(PChar(edBkpCaminhoCopiaBD.Text)); //Apaga a c�pia do arquivo original, ap�s o backup
  end else if PageControl1.ActivePageIndex = 1 then begin
    //Restore
  end;
end;

end.
