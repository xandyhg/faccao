inherited F_AuxBackupRestore: TF_AuxBackupRestore
  Caption = 'Backup do Sistema'
  ClientHeight = 536
  ClientWidth = 477
  OnActivate = FormActivate
  ExplicitTop = -113
  ExplicitWidth = 493
  ExplicitHeight = 575
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    Top = 459
    Width = 477
    inherited icoFechar: TImage
      Left = 387
      Visible = False
    end
    inherited icoSalvar: TImage
      Left = 207
      OnClick = icoSalvarClick
    end
    inherited icoCancelar: TImage
      Left = 297
      OnClick = icoCancelarClick
    end
  end
  inherited Panel3: TPanel
    Width = 477
    Height = 459
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 477
      Height = 459
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Backup'
        ExplicitWidth = 691
        ExplicitHeight = 348
        object Label2: TLabel
          Left = 15
          Top = 107
          Width = 189
          Height = 17
          Caption = 'Caminho C'#243'pia Banco de Dados'
        end
        object Label3: TLabel
          Left = 15
          Top = -1
          Width = 27
          Height = 17
          Caption = 'Host'
        end
        object Label5: TLabel
          Left = 15
          Top = 53
          Width = 96
          Height = 17
          Caption = 'Banco de Dados'
        end
        object Label6: TLabel
          Left = 15
          Top = 161
          Width = 95
          Height = 17
          Caption = 'Local do Backup'
        end
        object edBkpCaminhoCopiaBD: TEdit
          Left = 15
          Top = 130
          Width = 423
          Height = 25
          ReadOnly = True
          TabOrder = 2
        end
        object edBkpHost: TEdit
          Left = 15
          Top = 22
          Width = 190
          Height = 25
          ReadOnly = True
          TabOrder = 0
          Text = '192.168.0.200'
        end
        object edBkpDatabase: TEdit
          Left = 15
          Top = 76
          Width = 423
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
        end
        object edBkpBackup: TEdit
          Left = 15
          Top = 184
          Width = 423
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
        end
        object mBkpProgresso: TMemo
          Left = 0
          Top = 215
          Width = 469
          Height = 212
          Align = alBottom
          TabOrder = 4
        end
      end
    end
  end
  inherited Panel4: TPanel
    Top = 504
    Width = 477
    inherited pnCancelar: TPanel
      Left = 297
    end
    inherited pnSalvar: TPanel
      Left = 207
    end
    inherited pnFechar: TPanel
      Left = 387
      Visible = False
    end
  end
  object FDIBBackup1: TFDIBBackup
    OnError = FDIBBackup1Error
    BeforeExecute = FDIBBackup1BeforeExecute
    AfterExecute = FDIBBackup1AfterExecute
    DriverLink = FDPhysFBDriverLink1
    OnProgress = FDIBBackup1Progress
    Verbose = True
    Left = 320
    Top = 8
  end
  object FDIBRestore1: TFDIBRestore
    Left = 360
    Top = 8
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 400
    Top = 8
  end
end
