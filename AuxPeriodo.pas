unit AuxPeriodo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AuxPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, JvToolEdit, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  JvExMask, JvBaseEdits, DateUtils;

type
  TF_AuxPeriodo = class(TF_AuxPadrao)
    Label59: TLabel;
    edEnt_id: TJvCalcEdit;
    sbEntidade: TSpeedButton;
    edEnt_Nome: TEdit;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    edDataFim: TJvDateEdit;
    edDataIni: TJvDateEdit;
    procedure edEnt_idExit(Sender: TObject);
    procedure edEnt_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbEntidadeClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_AuxPeriodo       : TF_AuxPeriodo;
  AuxPeriodo_Ent_ID  : Integer;
  AuxPeriodo_DataIni : TDateTime;
  AuxPeriodo_DataFim : TDateTime;

implementation

{$R *.dfm}

uses clsEntidade, X, ConEntidade;

var
  Entidade : TEntidade;

procedure TF_AuxPeriodo.edEnt_idExit(Sender: TObject);
begin
  inherited;
  if edEnt_id.AsInteger > 0 then begin
    Entidade := TEntidade.Create;
    if Entidade.Recuperar(edEnt_id.AsInteger) then begin
      edEnt_Nome.Text := Entidade.ent_nome;
    end else begin
      msgPerson('Cliente n�o cadastrado!',[mbok],'Entidade',mtError);
      edEnt_id.SetFocus;
      edEnt_id.SelectAll;
    end;
    FreeAndNil(Entidade);
  end else begin
    edEnt_Nome.Clear;
  end;
end;

procedure TF_AuxPeriodo.edEnt_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 113 then
    sbEntidadeClick(Sender);
end;

procedure TF_AuxPeriodo.FormActivate(Sender: TObject);
begin
  inherited;
  edDataIni.Date := StartOfTheMonth(Date);
  edDataFim.Date := EndOfTheMonth(Date);
end;

procedure TF_AuxPeriodo.icoCancelarClick(Sender: TObject);
begin
  inherited;
  AuxPeriodo_Ent_ID  := 0;
  AuxPeriodo_DataIni := 0;
  AuxPeriodo_DataFim := 0;
  Close;
end;

procedure TF_AuxPeriodo.icoSalvarClick(Sender: TObject);
begin
  inherited;
  if edEnt_id.AsInteger = 0 then begin
    msgPerson('Obrigat�rio informar um cliente!',[mbok],'Per�odo',mtConfirmation);
    Exit;
  end;

  AuxPeriodo_Ent_ID  := edEnt_id.AsInteger;
  AuxPeriodo_DataIni := edDataIni.Date;
  AuxPeriodo_DataFim := edDataFim.Date;
  Close;
end;

procedure TF_AuxPeriodo.sbEntidadeClick(Sender: TObject);
begin
  inherited;
  xAux_ConEntidade_Ent_ID := 0;
  AbreForm(TF_ConEntidade,Self);
  edEnt_id.AsInteger := xAux_ConEntidade_Ent_ID;
  edEnt_idExit(Sender);
end;

end.
