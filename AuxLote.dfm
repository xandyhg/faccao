inherited F_AuxLote: TF_AuxLote
  BorderStyle = bsNone
  Caption = ' INFORME O N'#186' DO LOTE'
  ClientHeight = 196
  ClientWidth = 306
  ExplicitWidth = 306
  ExplicitHeight = 196
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    Top = 119
    Width = 306
    ExplicitTop = 119
    ExplicitWidth = 306
    inherited icoFechar: TImage
      Left = 216
      Visible = False
      ExplicitLeft = 216
    end
    inherited icoSalvar: TImage
      Left = 36
      OnClick = icoSalvarClick
      ExplicitLeft = 36
    end
    inherited icoCancelar: TImage
      Left = 126
      OnClick = icoCancelarClick
      ExplicitLeft = 126
    end
  end
  inherited Panel3: TPanel
    Width = 306
    Height = 119
    ExplicitWidth = 306
    ExplicitHeight = 119
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 306
      Height = 41
      Align = alTop
      Caption = 'Informe o n'#186' do Lote'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object edLote_Numero: TJvCalcEdit
      Left = 106
      Top = 61
      Width = 83
      Height = 36
      DecimalPlaces = 0
      DisplayFormat = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 1
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Panel4: TPanel
    Top = 164
    Width = 306
    ExplicitTop = 164
    ExplicitWidth = 306
    inherited pnCancelar: TPanel
      Left = 126
      ExplicitLeft = 126
    end
    inherited pnSalvar: TPanel
      Left = 36
      ExplicitLeft = 36
    end
    inherited pnFechar: TPanel
      Left = 216
      Visible = False
      ExplicitLeft = 216
    end
  end
  inherited Timer1: TTimer
    Left = 155
    Top = 0
  end
end
