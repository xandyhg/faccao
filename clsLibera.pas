unit clsLibera;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TLibera = class
  public
    lib_id  : Integer;
    lib_mac : String;

    function  Recuperar(Plib_id:Integer):Boolean;
    procedure Gravar(Plib_id:Integer);
    procedure Eliminar(Plib_id:Integer);
    function  Ultima:Integer;
    function  TotalLib:Integer;
  end;

implementation

{ TLibera }

uses DMC;

procedure TLibera.Eliminar(Plib_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Libera where lib_id = :K_lib_id');
  tq.ParamByName('K_lib_id').AsInteger := Plib_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TLibera.Gravar(Plib_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Libera where lib_id = :k_lib_id');
  tq.ParamByName('k_lib_id').AsInteger := Plib_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Libera values('+
    ':k_lib_id,'+
    ':k_lib_mac)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Libera set '+
    'lib_mac = :k_lib_mac ');
    tq.SQL.Add('where lib_id = :K_lib_id ');
  end;

  tq.ParamByName('k_lib_id').AsInteger := Plib_id;
  tq.ParamByName('k_lib_mac').AsString := lib_mac;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TLibera.Recuperar(Plib_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Libera where lib_id = :K_lib_id');
  tq.ParamByName('k_lib_id').AsInteger := Plib_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  lib_id  := tq.FieldByName('lib_id').AsInteger;
  lib_mac := tq.FieldByName('lib_mac').AsString;

  tq.Close;
  tq.Free;
end;

function TLibera.TotalLib: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select count(*) totLib from Libera');
  tq.Open;

  Result := tq.FieldByName('totLib').AsInteger;

  tq.Close;
  tq.Free;
end;

function TLibera.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(lib_id) maior from Libera');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
