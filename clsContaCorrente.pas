unit clsContaCorrente;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TContaCorrente = class
  public
    ent_id            : Integer;
    lote_id           : Integer;
    cc_seq            : Integer;
    cc_tipo           : Integer; //0-D�bito 1-Cr�dito
    cc_tipo_transacao : Integer; //0-Cheque
                                 //1-Dep�sito
                                 //2-Dinheiro
                                 //3-DOC
                                 //4-PIX
                                 //5-TED
                                 //6-FECHAMENTO
    cc_descricao      : String;
    cc_data           : TDateTime;
    cc_valor          : Double;
    cc_datalancamento : TDateTime;
    cc_conferido      : Integer; //0-N�o 1-Sim
    cc_ordem          : Integer; //Ordem para aparecer no relat�rio de CC

    function  Recuperar(Pent_id,Plote_id,Pcc_seq:Integer):Boolean;
    procedure Gravar(Pent_id,Plote_id,Pcc_seq:Integer);
    procedure Eliminar(Pent_id,Plote_id,Pcc_seq:Integer);
    function  Ultima(Pent_id,Plote_id:Integer):Integer;
    function  SomaValorLoteRecebido(Pent_id,Plote_id:Integer):Double;
    procedure AlterarConferido(Pent_id,Plote_id,Pcc_seq,Pcc_conferido:Integer);
    function  UltimaOrdem(Pent_id:Integer):Integer;
  end;

implementation

{ TContaCorrente }

uses DMC;

procedure TContaCorrente.AlterarConferido(Pent_id, Plote_id, Pcc_seq,
  Pcc_conferido: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('update ContaCorrente set cc_conferido = :k_cc_conferido                 '+
             'where ent_id = :K_ent_id and lote_id = :k_lote_id and cc_seq = :k_cc_seq');
  tq.ParamByName('K_ent_id').AsInteger       := Pent_id;
  tq.ParamByName('K_lote_id').AsInteger      := Plote_id;
  tq.ParamByName('K_cc_seq').AsInteger       := Pcc_seq;
  tq.ParamByName('K_cc_conferido').AsInteger := Pcc_conferido;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TContaCorrente.Eliminar(Pent_id,Plote_id,Pcc_seq:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from ContaCorrente where ent_id = :K_ent_id and lote_id = :k_lote_id and cc_seq = :k_cc_seq');
  tq.ParamByName('K_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('K_lote_id').AsInteger := Plote_id;
  tq.ParamByName('K_cc_seq').AsInteger  := Pcc_seq;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TContaCorrente.Gravar(Pent_id,Plote_id,Pcc_seq: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContaCorrente where ent_id = :k_ent_id and lote_id = :k_lote_id and cc_seq = :k_cc_seq');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('k_lote_id').AsInteger := Plote_id;
  tq.ParamByName('k_cc_seq').AsInteger  := Pcc_seq;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into ContaCorrente values('+
    ':k_ent_id,'+
    ':k_lote_id,'+
    ':k_cc_seq,'+
    ':k_cc_tipo,'+
    ':k_cc_tipo_transacao,'+
    ':k_cc_descricao,'+
    ':k_cc_data,'+
    ':k_cc_valor,'+
    ':k_cc_datalancamento,'+
    ':k_cc_conferido,'+
    ':k_cc_ordem)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update ContaCorrente set '+
    'cc_tipo           = :k_cc_tipo,'+
    'cc_tipo_transacao = :k_cc_tipo_transacao,'+
    'cc_descricao      = :k_cc_descricao,'+
    'cc_data           = :k_cc_data,'+
    'cc_valor          = :k_cc_valor,'+
    'cc_datalancamento = :k_cc_datalancamento,'+
    'cc_conferido      = :k_cc_conferido,'+
    'cc_ordem          = :k_cc_ordem ');
    tq.SQL.Add('where ent_id = :K_ent_id and lote_id = :k_lote_id and cc_seq = :k_cc_seq');
  end;

  tq.ParamByName('k_ent_id').AsInteger             := Pent_id;
  tq.ParamByName('k_lote_id').AsInteger            := Plote_id;
  tq.ParamByName('k_cc_seq').AsInteger             := Pcc_seq;
  tq.ParamByName('k_cc_tipo').AsInteger            := cc_tipo;
  tq.ParamByName('k_cc_tipo_transacao').AsInteger  := cc_tipo_transacao;
  tq.ParamByName('k_cc_descricao').AsString        := cc_descricao;
  tq.ParamByName('k_cc_data').AsDateTime           := cc_data;
  tq.ParamByName('k_cc_valor').AsFloat             := cc_valor;
  tq.ParamByName('k_cc_datalancamento').AsDateTime := cc_datalancamento;
  tq.ParamByName('k_cc_conferido').AsInteger       := cc_conferido;
  tq.ParamByName('k_cc_ordem').AsInteger           := cc_ordem;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TContaCorrente.Recuperar(Pent_id,Plote_id,Pcc_seq:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContaCorrente where ent_id = :K_ent_id and lote_id = :k_lote_id and cc_seq = :k_cc_seq');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('k_lote_id').AsInteger := Plote_id;
  tq.ParamByName('k_cc_seq').AsInteger  := Pcc_seq;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  ent_id            := tq.FieldByName('ent_id').AsInteger;
  lote_id           := tq.FieldByName('lote_id').AsInteger;
  cc_seq            := tq.FieldByName('cc_seq').AsInteger;
  cc_tipo           := tq.FieldByName('cc_tipo').AsInteger;
  cc_tipo_transacao := tq.FieldByName('cc_tipo_transacao').AsInteger;
  cc_descricao      := tq.FieldByName('cc_descricao').AsString;
  cc_data           := tq.FieldByName('cc_data').AsDateTime;
  cc_valor          := tq.FieldByName('cc_valor').AsFloat;
  cc_datalancamento := tq.FieldByName('cc_datalancamento').AsDateTime;
  cc_conferido      := tq.FieldByName('cc_conferido').AsInteger;
  cc_ordem          := tq.FieldByName('cc_ordem').AsInteger;

  tq.Close;
  tq.Free;
end;

function TContaCorrente.SomaValorLoteRecebido(Pent_id,
  Plote_id: Integer): Double;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('select sum(cc.cc_valor) Total                       '+
             'from contacorrente cc                               '+
             'where cc.ent_id=:k_ent_id and cc.lote_id=:k_lote_id ');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('k_lote_id').AsInteger := Plote_id;
  tq.Open;

  if tq.Eof then
    Result := 0
  else
    Result := tq.FieldByName('Total').AsFloat;

  tq.Close;
  tq.Free;
end;

function TContaCorrente.Ultima(Pent_id,Plote_id:Integer): Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(cc_seq) maior from ContaCorrente where ent_id = :k_ent_id and lote_id = :k_lote_id');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('k_lote_id').AsInteger := Plote_id;
  tq.Open;

  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

function TContaCorrente.UltimaOrdem(Pent_id: Integer): Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(cc_ordem) maior from ContaCorrente where ent_id = :k_ent_id');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.Open;

  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
