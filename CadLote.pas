unit CadLote;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, JvToolEdit, Vcl.Mask, JvExMask,
  JvBaseEdits, FireDAC.Comp.Client;

type
  TF_CadLote = class(TF_CadPadrao)
    edCodigo: TJvCalcEdit;
    Label1: TLabel;
    edLote_datacadastro: TJvDateEdit;
    Label6: TLabel;
    edLote_descricao: TEdit;
    Label2: TLabel;
    edCodigo2: TJvCalcEdit;
    Label3: TLabel;
    edLote_datafechamento: TJvDateEdit;
    Label4: TLabel;
    edLote_valor: TJvCalcEdit;
    Label5: TLabel;
    GroupBox1: TGroupBox;
    mLote_obs: TMemo;
    edLote_numero: TJvCalcEdit;
    Label7: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadLote: TF_CadLote;

implementation

{$R *.dfm}

uses DMC, ConLote, Principal, X, clsLote;

var
  Lote : TLote;

procedure TF_CadLote.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConLote_lote_ID > 0 then begin
    Lote := TLote.Create;
    if Lote.Recuperar(xAux_ConLote_Ent_ID,xAux_ConLote_lote_ID) then begin
      edCodigo.AsInteger         := xAux_ConLote_Ent_ID;
      edCodigo2.AsInteger        := xAux_ConLote_lote_ID;
      edLote_datacadastro.Date   := Lote.Lote_datacadastro;
      edLote_descricao.Text      := Lote.Lote_descricao;
      edLote_datafechamento.Date := Lote.Lote_datafechamento;
      edLote_valor.Value         := Lote.Lote_valor;
      mLote_obs.Text             := Lote.Lote_obs;
      edLote_numero.AsInteger    := Lote.lote_numero;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(Lote);
  end else begin
    edCodigo.AsInteger         := xAux_ConLote_Ent_ID;
    edLote_datacadastro.Date   := Date;
    edLote_descricao.Clear;
    edLote_datafechamento.Date := 0;
    edLote_valor.Value         := 0;
    mLote_obs.Clear;
    edLote_numero.Clear;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadLote.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadLote.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConLote_lote_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadLote.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Lote',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Lote := TLote.Create;

    Lote.Eliminar(edCodigo.AsInteger,edCodigo2.AsInteger);

    msgPerson('Lote eliminado com sucesso!',[mbok],'Lote',mtConfirmation);

    FreeAndNil(Lote);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadLote.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Lote := TLote.Create;

    Lote.Lote_descricao      := edLote_descricao.Text;
    Lote.Lote_datacadastro   := edLote_datacadastro.Date;
    Lote.Lote_datafechamento := edLote_datafechamento.Date;
    Lote.Lote_valor          := edLote_valor.Value;
    Lote.Lote_obs            := mLote_obs.Text;
    Lote.lote_numero         := edLote_numero.AsInteger;

    if edCodigo2.AsInteger = 0 then begin
      xAux_ConLote_lote_ID     := Lote.Ultima(xAux_ConLote_Ent_ID);
      Lote.Gravar(xAux_ConLote_Ent_ID,xAux_ConLote_lote_ID);
    end else begin
      Lote.Gravar(xAux_ConLote_Ent_ID,xAux_ConLote_lote_ID);
    end;

    FreeAndNil(Lote);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo2.AsInteger = 0 then
      msgPerson('Lote N� '+IntToStr(edLote_numero.AsInteger)+' cadastrado com sucesso!',[mbOK], 'Lote', mtConfirmation)
    else
      msgPerson('Lote N� '+IntToStr(edLote_numero.AsInteger)+' alterado com sucesso!',[mbOK], 'Lote', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

end.
