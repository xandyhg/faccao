unit Login;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.jpeg, Vcl.ExtCtrls, FireDAC.Comp.Client,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.Imaging.pngimage;

type
  TF_Login = class(TForm)
    Image1: TImage;
    SpeedButton2: TSpeedButton;
    edLogin: TEdit;
    edSenha: TEdit;
    Button1: TButton;
    Image3: TImage;
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Login: TF_Login;

implementation

{$R *.dfm}

uses X, DMC, Principal, clsUsuario, AuxTrocaSenha, AuxBackupRestore;

var
  Usuario       : TUsuario;
  numTentativa  : Integer;

procedure TF_Login.Button1Click(Sender: TObject);
var
  aux_senha : String;
begin
  xAux_AcessoPermitido := False;

  Usuario := TUsuario.Create;

  xAux_SenhaAdmin := 'kyu250985';

  //Acesso do admin
  if (edLogin.Text='ADMIN') and (edSenha.Text=xAux_SenhaAdmin) then begin
    xAux_AcessoPermitido := True;
    xAux_NomeUsuario     := edLogin.Text;
    xAux_IDUsuario       := 0;
    Close;
    Exit;
  end;

  aux_senha := Criptografia(edSenha.Text,'kyu250985');
  numTentativa := numTentativa + 1;
  if Usuario.RecuperarPorLogin(edLogin.Text)=False then begin
    msgPerson('Nome de usu�rio n�o cadastrado!',[mbok],'Aten��o',mtError);
    edLogin.SetFocus;
    if numTentativa=3 then
      Application.Terminate;
    Exit;
  end else begin
    if (edLogin.Text='ADMIN') then begin
      Usuario.usua_senha := xAux_SenhaAdmin;
      aux_senha          := edSenha.Text;
    end;
    if (Usuario.usua_senha=aux_senha) then begin
      if Usuario.usua_ativo=1 then begin
        msgPerson('Usu�rio est� inativo!',[mbok],'Aten��o',mtError);
        Application.Terminate;
        Exit;
      end;
      if Usuario.usua_alterarSenha = 1 then begin
        AbreForm(TF_AuxTrocaSenha,Self);
        if xAux_AuxTrocaSenha_NovaSenha <> '' then begin
          Usuario.AlteraSenha(Usuario.usua_id,0,xAux_AuxTrocaSenha_NovaSenha);
        end else begin
          Application.Terminate;
          Exit;
        end;
      end;
      xAux_AcessoPermitido := True;
      xAux_NomeUsuario     := Usuario.usua_login;
      xAux_IDUsuario       := Usuario.usua_id;
      PostMessage(Self.Handle, WM_CLOSE, 0, 0);
    end else begin
      msgPerson('Usu�rio/Senha inv�lida!',[mbok],'Aten��o',mtError);
      edLogin.SetFocus;
      if numTentativa=3 then
        Application.Terminate;
      Exit;
    end;
  end;

  FreeAndNil(Usuario);

  try
    DM.FDCon.StartTransaction;
    //Ir� gravar o log de acesso
//    Log                 := TLog.create;
//    log.log_tipo        := 4;
//    log.log_tabela      := 'ACESSO';
//    log.log_data        := Date;
//    log.log_hora        := TimeToStr(Time);
//    log.log_idUsuario   := xAux_IDUsuario;
//    log.log_nomeUsuario := xAux_NomeUsuario;
//    log.log_valor       := 0;
//    log.log_obs         := 'LOGIN: '+IntToStr(xAux_IDUsuario)+'-'+xAux_NomeUsuario+'   HD: '+Retorna_Serial_HD+'  COMP.: '+Retorna_Nome_Computador+'   IP: '+Retorna_IP_Computador+'  Mac: '+Retorna_MacAddress;
//    log.log_motivo      := '';
//    log.Gravar(log.Ultima);
//    FreeAndNil(Log);
//    //final do log

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_Login.edSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=13 then
    Button1Click(Sender);
end;

procedure TF_Login.FormCreate(Sender: TObject);
begin
  if IsDebuggerPresent then begin
    edLogin.Text := 'ADMIN';
    edSenha.Text := 'kyu250985';
  end;
  numTentativa := 0;
end;

procedure TF_Login.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    Perform(Wm_NextDlgCtl,0,0);
end;

procedure TF_Login.Image3Click(Sender: TObject);
begin
  AbreForm(TF_AuxBackupRestore,Self);
end;

procedure TF_Login.SpeedButton2Click(Sender: TObject);
begin
  Application.Terminate;
end;

end.
