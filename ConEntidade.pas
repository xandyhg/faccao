unit ConEntidade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Mask, JvExMask, JvToolEdit, JvBaseEdits;

type
  TF_ConEntidade = class(TF_ConPadrao)
    FDEntidade: TFDQuery;
    ProviderEntidade: TDataSetProvider;
    CdsEntidade: TClientDataSet;
    DsEntidade: TDataSource;
    FDEntidadeENT_ID: TIntegerField;
    FDEntidadeENT_NOME: TStringField;
    FDEntidadeENT_DATACADASTRO: TSQLTimeStampField;
    FDEntidadeENT_CELULAR: TStringField;
    FDEntidadeENT_TELEFONE: TStringField;
    FDEntidadeENT_EMAIL: TStringField;
    FDEntidadeENT_ATIVO: TIntegerField;
    CdsEntidadeENT_ID: TIntegerField;
    CdsEntidadeENT_NOME: TStringField;
    CdsEntidadeENT_DATACADASTRO: TSQLTimeStampField;
    CdsEntidadeENT_CELULAR: TStringField;
    CdsEntidadeENT_TELEFONE: TStringField;
    CdsEntidadeENT_EMAIL: TStringField;
    CdsEntidadeENT_ATIVO: TIntegerField;
    rgEnt_Ativo: TRadioGroup;
    GroupBox1: TGroupBox;
    edDescricao: TEdit;
    cbPesquisar: TComboBox;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure CdsEntidadeENT_ATIVOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConEntidade             : TF_ConEntidade;
  xAux_ConEntidade_Ent_ID   : Integer;
  xAux_ConEntidade_Tela     : String;

implementation

{$R *.dfm}

uses DMC, CadEntidade, X;

procedure TF_ConEntidade.CdsEntidadeENT_ATIVOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '0' then
    Text := 'S'
  else
    Text := 'N';
end;

procedure TF_ConEntidade.edDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key=13 then
    icoPesquisarClick(Sender);
end;

procedure TF_ConEntidade.FormActivate(Sender: TObject);
begin
  inherited;
  rgEnt_Ativo.ItemIndex := 2;
  icoPesquisarClick(Sender);
  edDescricao.SetFocus;
end;

procedure TF_ConEntidade.icoFecharClick(Sender: TObject);
begin
  xAux_ConEntidade_Ent_ID := CdsEntidade.FieldByName('ent_id').AsInteger;
  inherited;

end;

procedure TF_ConEntidade.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConEntidade_Ent_ID := 0;
  AbreForm(TF_CadEntidade,Self);
  icoPesquisarClick(Sender);
  if xAux_ConEntidade_Ent_ID <> 0 then
    CdsEntidade.Locate('ent_id',xAux_ConEntidade_Ent_ID,[]);
end;

procedure TF_ConEntidade.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsEntidade.Close;

  FDEntidade.Close;
  FDEntidade.Params.Clear;
  FDEntidade.SQL.Clear;
  FDEntidade.SQL.Add('select e.ent_id, e.ent_nome, e.ent_datacadastro, e.ent_celular, e.ent_telefone, e.ent_email, e.ent_ativo '+
                     'from entidade e                                                                                          ');
  if cbPesquisar.Text = 'Celular' then
    FDEntidade.SQL.Add('where (select Retorno from proc_digitos(e.ent_celular)) like ' + QuotedStr('%'+Trim(SomenteNumero(edDescricao.Text))+'%') + ' ')
  else if cbPesquisar.Text = 'C�digo' then
    FDEntidade.SQL.Add('where e.ent_id like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Nome' then
    FDEntidade.SQL.Add('where e.ent_nome like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Telefone' then
    FDEntidade.SQL.Add('where (select Retorno from proc_digitos(e.ent_telefone)) like ' + QuotedStr('%'+Trim(SomenteNumero(edDescricao.Text))+'%') + ' ')
  else if cbPesquisar.Text = 'E-mail' then
    FDEntidade.SQL.Add('where e.ent_email like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else;
  if rgEnt_Ativo.ItemIndex <> 2 then begin
    if rgEnt_Ativo.ItemIndex = 0 then
      FDEntidade.SQL.Add('and e.ent_ativo = 0 ')
    else
      FDEntidade.SQL.Add('and e.ent_ativo = 1 ');
  end;
  FDEntidade.SQL.Add('order by e.ent_nome                                                                                      ');
  CdsEntidade.Open;

  if not CdsEntidade.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsEntidade.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConEntidade.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConEntidade_Ent_ID := CdsEntidade.FieldByName('ent_id').AsInteger;
  AbreForm(TF_CadEntidade, Self);
  icoPesquisarClick(Sender);
  if xAux_ConEntidade_Ent_ID <> 0 then
    CdsEntidade.Locate('ent_id',xAux_ConEntidade_Ent_ID,[]);
end;

procedure TF_ConEntidade.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if Column.FieldName = 'ENT_ATIVO' then begin
    if CdsEntidade.FieldByName('ENT_ATIVO').AsInteger = 0 then begin
      JvDBGrid1.Canvas.Brush.Color := clGreen;
      JvDBGrid1.Canvas.Font.Color  := clwhite;
    end else begin
      JvDBGrid1.Canvas.Brush.Color := clRed;
      JvDBGrid1.Canvas.Font.Color  := clwhite;
    end;
  end;
  JvDBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

end.
