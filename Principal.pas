unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ImgList, Vcl.ComCtrls, Vcl.ToolWin;

type
  TF_Principal = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ToolBar1: TToolBar;
    ToolButton2: TToolButton;
    TabSheet2: TTabSheet;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    ToolButton4: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    ImageList1: TImageList;
    ToolButton3: TToolButton;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Principal          : TF_Principal;
  xAux_NomeUsuario     : String;
  xAux_IDUsuario       : Integer;
  xAux_AcessoPermitido : Boolean;
  xAux_TelaCheia       : Boolean;
  xAux_SenhaAdmin      : String;

implementation

{$R *.dfm}

uses X, DMC, Login, ConUsuario, ConConfigEmail, ConEntidade, ConLote,
  ConContaCorrente, clsLibera, ConCaixa;

var
  Libera : TLibera;

procedure TF_Principal.FormActivate(Sender: TObject);
begin
  if xAux_AcessoPermitido = False then
    Application.Terminate;

  if xAux_NomeUsuario = 'ADMIN' then begin
    ToolButton1.Show;
    ToolButton4.Show;
  end;

//  SelectFirst;
end;

procedure TF_Principal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TF_Principal.FormCreate(Sender: TObject);
begin
  if IsDebuggerPresent then begin
    Caption := 'BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE';
  end;
  DM.ConectarBanco('Banco'); //Conecta no banco  //Adicionei essa linha para abrir Principal primeiro

  AbreForm(TF_Login,Self);  //Adicionei essa linha para abrir Principal primeiro
end;

procedure TF_Principal.ToolButton11Click(Sender: TObject);
begin
  AbreForm(TF_ConLote,Self);
end;

procedure TF_Principal.ToolButton1Click(Sender: TObject);
begin
  AbreForm(TF_ConUsuario,Self);
end;

procedure TF_Principal.ToolButton2Click(Sender: TObject);
begin
  AbreForm(TF_ConContaCorrente,Self);
end;

procedure TF_Principal.ToolButton3Click(Sender: TObject);
begin
  AbreForm(TF_ConCaixa,Self);
end;

procedure TF_Principal.ToolButton4Click(Sender: TObject);
begin
  AbreForm(TF_ConConfigEmail,Self);
end;

procedure TF_Principal.ToolButton8Click(Sender: TObject);
begin
  AbreForm(TF_ConEntidade,Self);
end;

end.
