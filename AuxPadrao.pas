unit AuxPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, JvRichEdit;

type
  TF_AuxPadrao = class(TForm)
    Panel2: TPanel;
    icoFechar: TImage;
    icoSalvar: TImage;
    icoCancelar: TImage;
    Panel3: TPanel;
    Panel4: TPanel;
    pnCancelar: TPanel;
    pnSalvar: TPanel;
    pnFechar: TPanel;
    Timer1: TTimer;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_AuxPadrao: TF_AuxPadrao;

implementation

{$R *.dfm}

uses  X;

procedure TF_AuxPadrao.FormCreate(Sender: TObject);
begin
  if IsDebuggerPresent then begin
    Caption := Caption + ' - BASE TESTE';
  end;
end;

procedure TF_AuxPadrao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F3) and (icoSalvar.Visible = True) then
    icoSalvar.OnClick(Self)
  else if (Key = VK_F5) and (icoCancelar.Visible = True) then
    icoCancelar.OnClick(Self)
  else if (Key = VK_ESCAPE) and (icoFechar.Visible = True) then
    icoFechar.OnClick(Self);
end;

procedure TF_AuxPadrao.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    if (not (ActiveControl is TMemo)) and (not (ActiveControl is TJvRichEdit)) then
      Perform(Wm_NextDlgCtl,0,0);
end;

end.
