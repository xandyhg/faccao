program faccao;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {F_Principal},
  DMC in 'DMC.pas' {DM: TDataModule},
  X in 'X.pas',
  Login in 'Login.pas' {F_Login},
  AuxPadrao in 'AuxPadrao.pas' {F_AuxPadrao},
  AuxTrocaSenha in 'AuxTrocaSenha.pas' {F_AuxTrocaSenha},
  CadPadrao in 'CadPadrao.pas' {F_CadPadrao},
  CadUsuario in 'CadUsuario.pas' {F_CadUsuario},
  clsUsuario in 'clsUsuario.pas',
  ConPadrao in 'ConPadrao.pas' {F_ConPadrao},
  ConUsuario in 'ConUsuario.pas' {F_ConUsuario},
  CadConfigEmail in 'CadConfigEmail.pas' {F_CadConfigEmail},
  CadEntidade in 'CadEntidade.pas' {F_CadEntidade},
  clsConfigEmail in 'clsConfigEmail.pas',
  clsEntidade in 'clsEntidade.pas',
  ConConfigEmail in 'ConConfigEmail.pas' {F_ConConfigEmail},
  ConEntidade in 'ConEntidade.pas' {F_ConEntidade},
  clsLote in 'clsLote.pas',
  ConLote in 'ConLote.pas' {F_ConLote},
  CadLote in 'CadLote.pas' {F_CadLote},
  clsContaCorrente in 'clsContaCorrente.pas',
  ConContaCorrente in 'ConContaCorrente.pas' {F_ConContaCorrente},
  CadContaCorrente in 'CadContaCorrente.pas' {F_CadContaCorrente},
  clsLibera in 'clsLibera.pas',
  AuxLote in 'AuxLote.pas' {F_AuxLote},
  AuxPeriodo in 'AuxPeriodo.pas' {F_AuxPeriodo},
  clsCaixa in 'clsCaixa.pas',
  ConCaixa in 'ConCaixa.pas' {F_ConCaixa},
  CadCaixa in 'CadCaixa.pas' {F_CadCaixa},
  AuxBackupRestore in 'AuxBackupRestore.pas' {F_AuxBackupRestore},
  AuxOrdenacao in 'AuxOrdenacao.pas' {F_AuxOrdenacao};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TF_Principal, F_Principal);
  Application.CreateForm(TF_AuxOrdenacao, F_AuxOrdenacao);
  Application.Run;
end.
