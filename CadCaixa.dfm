inherited F_CadCaixa: TF_CadCaixa
  Caption = 'F_CadCaixa'
  ClientHeight = 228
  ClientWidth = 675
  OnActivate = FormActivate
  ExplicitWidth = 691
  ExplicitHeight = 267
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 151
    Width = 675
    ExplicitTop = 151
    ExplicitWidth = 675
    inherited icoFechar: TImage
      Left = 585
      ExplicitLeft = 585
    end
    inherited icoAlterar: TImage
      Left = 405
      OnClick = icoAlterarClick
      ExplicitLeft = 399
      ExplicitTop = -5
      ExplicitHeight = 45
    end
    inherited icoExcluir: TImage
      Left = 495
      OnClick = icoExcluirClick
      ExplicitLeft = 495
    end
    inherited icoSalvar: TImage
      Left = 225
      OnClick = icoSalvarClick
      ExplicitLeft = 225
    end
    inherited icoCancelar: TImage
      Left = 315
      OnClick = icoCancelarClick
      ExplicitLeft = 315
    end
  end
  inherited Panel3: TPanel
    Width = 675
    Height = 110
    ExplicitWidth = 675
    ExplicitHeight = 110
    object Label3: TLabel
      Left = 46
      Top = 16
      Width = 26
      Height = 16
      Caption = 'Data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 530
      Top = 16
      Width = 30
      Height = 16
      Caption = 'Valor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object sb_clifor: TSpeedButton
      Left = 158
      Top = 44
      Width = 24
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000585751747069
        E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
        F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
        8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
        FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
        FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
        FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
        B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
        A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
        3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
        CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
        74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
        F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
        B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
        C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
        999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
      ParentFont = False
      OnClick = sb_cliforClick
    end
    object Label7: TLabel
      Left = 10
      Top = 47
      Width = 62
      Height = 16
      Alignment = taRightJustify
      Caption = 'Favorecido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 46
      Top = 78
      Width = 26
      Height = 16
      Caption = 'Obs.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 344
      Top = 16
      Width = 26
      Height = 17
      Caption = 'Tipo'
    end
    object dtpData: TJvDateEdit
      Left = 78
      Top = 13
      Width = 111
      Height = 25
      ShowNullDate = False
      TabOrder = 0
    end
    object rg_debcred: TRadioGroup
      Left = 195
      Top = 6
      Width = 136
      Height = 32
      Caption = ' Tipo '
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 1
      Items.Strings = (
        'Cr'#233'dito'
        'D'#233'bito')
      ParentFont = False
      TabOrder = 1
      TabStop = True
      OnClick = rg_debcredClick
    end
    object ed_cx_valor: TJvCalcEdit
      Left = 566
      Top = 13
      Width = 99
      Height = 25
      DisplayFormat = '#,##0.00'
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
    end
    object ed_clifor_nome: TEdit
      Left = 191
      Top = 44
      Width = 474
      Height = 24
      TabStop = False
      CharCase = ecUpperCase
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 40
      ParentFont = False
      TabOrder = 4
    end
    object ed_clifor_id: TJvCalcEdit
      Left = 78
      Top = 44
      Width = 75
      Height = 25
      ShowButton = False
      TabOrder = 5
      DecimalPlacesAlwaysShown = False
      OnExit = ed_clifor_idExit
      OnKeyDown = ed_clifor_idKeyDown
    end
    object ed_cx_obs: TEdit
      Left = 78
      Top = 75
      Width = 587
      Height = 24
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 200
      ParentFont = False
      TabOrder = 6
    end
    object cbFormaPgto: TComboBox
      Left = 376
      Top = 13
      Width = 145
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemIndex = 4
      ParentFont = False
      TabOrder = 2
      Text = 'PIX'
      Items.Strings = (
        'CHEQUE'
        'DEP'#211'SITO'
        'DINHEIRO'
        'DOC'
        'PIX'
        'TED'
        'FECHAMENTO')
    end
  end
  inherited Panel4: TPanel
    Top = 196
    Width = 675
    ExplicitTop = 196
    ExplicitWidth = 675
    inherited pnCancelar: TPanel
      Left = 315
      ExplicitLeft = 315
    end
    inherited pnAlterar: TPanel
      Left = 405
      ExplicitLeft = 405
    end
    inherited pnExcluir: TPanel
      Left = 495
      ExplicitLeft = 495
    end
    inherited pnSalvar: TPanel
      Left = 225
      ExplicitLeft = 225
    end
    inherited pnFechar: TPanel
      Left = 585
      ExplicitLeft = 585
    end
  end
  inherited Panel5: TPanel
    Width = 675
    ExplicitWidth = 675
    object Label1: TLabel
      Left = 33
      Top = 10
      Width = 39
      Height = 16
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object ed_cx_id: TJvCalcEdit
      Left = 78
      Top = 6
      Width = 48
      Height = 25
      Enabled = False
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 560
    Top = 0
  end
end
