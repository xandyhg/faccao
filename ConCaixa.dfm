inherited F_ConCaixa: TF_ConCaixa
  Caption = 'F_ConCaixa'
  ClientHeight = 522
  ClientWidth = 1267
  ExplicitWidth = 1283
  ExplicitHeight = 561
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 409
    Width = 1267
    ExplicitTop = 409
    ExplicitWidth = 1267
    inherited icoPesquisar: TImage
      Left = 847
      ExplicitLeft = 847
    end
    inherited icoNovo: TImage
      Left = 952
      ExplicitLeft = 952
    end
    inherited icoImprimir: TImage
      Left = 1057
      ExplicitLeft = 1057
    end
    inherited icoFechar: TImage
      Left = 1162
      ExplicitLeft = 1162
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 472
      Height = 45
      Align = alLeft
      Caption = 'Valores Totais'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitTop = -4
      object lblCredito: TLabel
        Left = 59
        Top = 18
        Width = 103
        Height = 17
        Caption = 'Cr'#233'dito:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblDebito: TLabel
        Left = 220
        Top = 18
        Width = 92
        Height = 17
        Caption = 'D'#233'bito:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblSaldo: TLabel
        Left = 362
        Top = 18
        Width = 103
        Height = 17
        Caption = 'Saldo:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label4: TLabel
        Left = 6
        Top = 18
        Width = 49
        Height = 17
        Caption = 'Cr'#233'dito:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label5: TLabel
        Left = 168
        Top = 18
        Width = 46
        Height = 17
        Caption = 'D'#233'bito:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label6: TLabel
        Left = 318
        Top = 18
        Width = 38
        Height = 17
        Caption = 'Saldo:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 486
    Width = 1267
    ExplicitTop = 486
    ExplicitWidth = 1267
  end
  inherited Panel3: TPanel
    Top = 97
    Width = 1267
    Height = 267
    ExplicitTop = 97
    ExplicitWidth = 1267
    ExplicitHeight = 267
    inherited JvDBGrid1: TJvDBGrid
      Width = 1267
      Height = 267
      DataSource = DsCaixa
      OnDrawColumnCell = JvDBGrid1DrawColumnCell
      OnDblClick = JvDBGrid1DblClick
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CX_DEBCRED'
          Title.Alignment = taCenter
          Title.Caption = 'D/C'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CX_FORMA_PGTO'
          Title.Alignment = taCenter
          Title.Caption = 'Forma Pgto.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 105
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CX_ID'
          Title.Caption = 'ID Cx.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CX_DATAENTRADA'
          Title.Alignment = taCenter
          Title.Caption = 'Data'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 114
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CX_VALOR'
          Title.Caption = 'Valor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_ID'
          Title.Caption = 'ID Cli./Forn.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME'
          Title.Caption = 'Cliente / Fornecedor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 316
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CX_OBS'
          Title.Caption = 'Observa'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 438
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Top = 454
    Width = 1267
    ExplicitTop = 454
    ExplicitWidth = 1267
    inherited pnPesquisar: TPanel
      Left = 847
      ExplicitLeft = 847
    end
    inherited pnNovo: TPanel
      Left = 952
      ExplicitLeft = 952
    end
    inherited pnImprimir: TPanel
      Left = 1057
      Caption = 'IMPRIMIR'
      PopupMenu = PopupMenu1
      ExplicitLeft = 1057
      object JvSpeedButton1: TJvSpeedButton
        Left = 84
        Top = 0
        Width = 21
        Height = 32
        Align = alRight
        DropDownMenu = PopupMenu1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Segoe UI'
        HotTrackFont.Style = []
        ParentFont = False
        Transparent = True
        ExplicitTop = 5
        ExplicitHeight = 27
      end
    end
    inherited pnFechar: TPanel
      Left = 1162
      ExplicitLeft = 1162
    end
  end
  inherited Panel1: TPanel
    Width = 1267
    Height = 97
    ExplicitWidth = 1267
    ExplicitHeight = 97
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 116
      Height = 97
      Align = alLeft
      Caption = ' Per'#237'odo '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label8: TLabel
        Left = 54
        Top = 45
        Width = 7
        Height = 17
        Alignment = taRightJustify
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edDataFim: TJvDateEdit
        Left = 6
        Top = 65
        Width = 105
        Height = 25
        ShowNullDate = False
        TabOrder = 1
      end
      object edDataIni: TJvDateEdit
        Left = 6
        Top = 20
        Width = 105
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
      end
    end
  end
  object Panel5: TPanel [5]
    Left = 0
    Top = 364
    Width = 1267
    Height = 45
    Align = alBottom
    BevelOuter = bvNone
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 5
    object GroupBox4: TGroupBox
      Left = 0
      Top = 0
      Width = 472
      Height = 45
      Align = alLeft
      Caption = 'Valores do Per'#237'odo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object lblCreditoPeriodo: TLabel
        Left = 59
        Top = 18
        Width = 103
        Height = 17
        Caption = 'Cr'#233'dito:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblDebitoPeriodo: TLabel
        Left = 220
        Top = 18
        Width = 92
        Height = 17
        Caption = 'D'#233'bito:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblSaldoPeriodo: TLabel
        Left = 362
        Top = 18
        Width = 103
        Height = 17
        Caption = 'Saldo:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label1: TLabel
        Left = 6
        Top = 18
        Width = 49
        Height = 17
        Caption = 'Cr'#233'dito:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label2: TLabel
        Left = 168
        Top = 18
        Width = 46
        Height = 17
        Caption = 'D'#233'bito:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label3: TLabel
        Left = 318
        Top = 18
        Width = 38
        Height = 17
        Caption = 'Saldo:'
        Color = 3302400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object FDCaixa: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select c.*, e.ent_nome                          '
      
        'from caixa c                                                    ' +
        '                              '
      'left outer join entidade e on c.ent_id = e.ent_id'
      'order by cx_dataentrada, cx_id ')
    Left = 464
    Top = 16
    object FDCaixaCX_ID: TIntegerField
      FieldName = 'CX_ID'
      Origin = 'CX_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDCaixaENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
    end
    object FDCaixaCX_DATAENTRADA: TSQLTimeStampField
      FieldName = 'CX_DATAENTRADA'
      Origin = 'CX_DATAENTRADA'
    end
    object FDCaixaCX_VALOR: TFloatField
      FieldName = 'CX_VALOR'
      Origin = 'CX_VALOR'
    end
    object FDCaixaCX_OBS: TStringField
      FieldName = 'CX_OBS'
      Origin = 'CX_OBS'
      Size = 200
    end
    object FDCaixaCX_DEBCRED: TStringField
      FieldName = 'CX_DEBCRED'
      Origin = 'CX_DEBCRED'
      FixedChar = True
      Size = 1
    end
    object FDCaixaCX_FORMA_PGTO: TIntegerField
      FieldName = 'CX_FORMA_PGTO'
      Origin = 'CX_FORMA_PGTO'
    end
    object FDCaixaENT_NOME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ENT_NOME'
      Origin = 'ENT_NOME'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
  end
  object ProviderCaixa: TDataSetProvider
    DataSet = FDCaixa
    Left = 496
    Top = 16
  end
  object CdsCaixa: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderCaixa'
    Left = 528
    Top = 16
    object CdsCaixaCX_ID: TIntegerField
      FieldName = 'CX_ID'
      Required = True
    end
    object CdsCaixaENT_ID: TIntegerField
      FieldName = 'ENT_ID'
    end
    object CdsCaixaCX_DATAENTRADA: TSQLTimeStampField
      FieldName = 'CX_DATAENTRADA'
    end
    object CdsCaixaCX_VALOR: TFloatField
      FieldName = 'CX_VALOR'
      DisplayFormat = '#,##0.00'
    end
    object CdsCaixaCX_OBS: TStringField
      FieldName = 'CX_OBS'
      Size = 200
    end
    object CdsCaixaCX_DEBCRED: TStringField
      FieldName = 'CX_DEBCRED'
      FixedChar = True
      Size = 1
    end
    object CdsCaixaCX_FORMA_PGTO: TIntegerField
      FieldName = 'CX_FORMA_PGTO'
      OnGetText = CdsCaixaCX_FORMA_PGTOGetText
    end
    object CdsCaixaENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      ReadOnly = True
      Size = 250
    end
  end
  object DsCaixa: TDataSource
    DataSet = CdsCaixa
    Left = 560
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 728
    Top = 16
    object RelatriodeCaixa1: TMenuItem
      Caption = 'Relat'#243'rio de Caixa'
      OnClick = RelatriodeCaixa1Click
    end
  end
  object frxCaixa: TfrxDBDataset
    UserName = 'frxCaixa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CX_ID=CX_ID'
      'ENT_ID=ENT_ID'
      'CX_DATAENTRADA=CX_DATAENTRADA'
      'CX_VALOR=CX_VALOR'
      'CX_OBS=CX_OBS'
      'CX_DEBCRED=CX_DEBCRED'
      'CX_FORMA_PGTO=CX_FORMA_PGTO'
      'ENT_NOME=ENT_NOME')
    DataSet = CdsCaixa
    BCDToCurrency = False
    Left = 768
    Top = 16
  end
end
