unit AuxOrdenacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AuxPadrao, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage;

type
  TF_AuxOrdenacao = class(TF_AuxPadrao)
    RadioGroup1: TRadioGroup;
    procedure icoSalvarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_AuxOrdenacao          : TF_AuxOrdenacao;
  xAux_AuxOrdenacao_Ordem : Integer;

implementation

{$R *.dfm}

procedure TF_AuxOrdenacao.icoCancelarClick(Sender: TObject);
begin
  inherited;
  xAux_AuxOrdenacao_Ordem := -1;
  Close;
end;

procedure TF_AuxOrdenacao.icoSalvarClick(Sender: TObject);
begin
  inherited;
  xAux_AuxOrdenacao_Ordem := RadioGroup1.ItemIndex;
  Close;
end;

end.
