unit CadUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.Buttons, Vcl.StdCtrls, Vcl.Mask, JvToolEdit,
  JvExMask, JvBaseEdits, Vcl.ComCtrls, FireDAC.Comp.Client;

type
  TF_CadUsuario = class(TF_CadPadrao)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    edCodigo: TJvCalcEdit;
    Label2: TLabel;
    edUsua_nome: TEdit;
    Label3: TLabel;
    edUsua_login: TEdit;
    Label4: TLabel;
    edUsua_senha: TEdit;
    Label5: TLabel;
    edUsua_Admissao: TJvDateEdit;
    Label6: TLabel;
    edUsua_Desligamento: TJvDateEdit;
    rgUsua_Ativo: TRadioGroup;
    ckUsua_alterarSenha: TCheckBox;
    Label7: TLabel;
    edUsua_Fone: TMaskEdit;
    Label8: TLabel;
    edUsua_Obs: TMemo;
    Label10: TLabel;
    edUsua_email_nome: TEdit;
    Label11: TLabel;
    edUsua_email: TEdit;
    Label12: TLabel;
    edUsua_email_usuario: TEdit;
    Label13: TLabel;
    edUsua_email_senha: TEdit;
    Label14: TLabel;
    edConfe_id: TJvCalcEdit;
    sbConfigEmail: TSpeedButton;
    edConfe_nome: TEdit;
    TabSheet3: TTabSheet;
    Label16: TLabel;
    edusua_caminho_imp_pdf: TEdit;
    Label17: TLabel;
    edusua_caminho_imp_padrao: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadUsuario: TF_CadUsuario;

implementation

{$R *.dfm}

uses DMC, Principal, clsUsuario, ConUsuario, X;

var
  Usuario       : TUsuario;
  tq            : TFDQuery;

procedure TF_CadUsuario.FormActivate(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 0;

  if xAux_ConUsuario_Usua_ID > 0 then begin
    Usuario := TUsuario.Create;
    if Usuario.Recuperar(xAux_ConUsuario_Usua_ID) then begin
      edCodigo.AsInteger                  := xAux_ConUsuario_Usua_ID;
      edUsua_nome.Text                    := Usuario.usua_nome;
      edUsua_login.Text                   := Usuario.usua_login;
      edUsua_senha.Text                   := Criptografia(Usuario.usua_senha,xAux_SenhaAdmin);
      edUsua_Admissao.Date                := Usuario.usua_dataadmissao;
      edUsua_Desligamento.Date            := Usuario.usua_datadesligamento;
      edUsua_Fone.Text                    := Usuario.usua_fone;
      edUsua_Obs.Text                     := Usuario.usua_obs;
      rgUsua_Ativo.ItemIndex              := Usuario.usua_ativo;
      edUsua_email_nome.Text              := Usuario.usua_email_nome;
      edUsua_email.Text                   := Usuario.usua_email;
      edUsua_email_usuario.Text           := Usuario.usua_email_usuario;
      edUsua_email_senha.Text             := Criptografia(Usuario.usua_email_senha,xAux_SenhaAdmin);
//      edConfe_id.AsInteger                := Usuario.confe_id;
      edusua_caminho_imp_pdf.Text         := Usuario.usua_caminho_imp_pdf;
      edusua_caminho_imp_padrao.Text      := Usuario.usua_caminho_imp_padrao;
      if Usuario.usua_alterarSenha = 1 then
        ckUsua_alterarSenha.Checked       := True
      else
        ckUsua_alterarSenha.Checked       := False;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(Usuario);
  end else begin
    edUsua_nome.Clear;
    edUsua_login.Clear;
    edUsua_senha.Clear;
    edUsua_Admissao.Date := Date;
    edUsua_Desligamento.Date := 0;
    edUsua_Fone.Clear;
    edUsua_Obs.Lines.Clear;
    edUsua_email_nome.Clear;
    edUsua_email.Clear;
    edUsua_email_usuario.Clear;
    edUsua_email_senha.Clear;
    edConfe_id.Clear;
    edConfe_nome.Clear;
    edusua_caminho_imp_pdf.Clear;
    edusua_caminho_imp_padrao.Clear;
    ckUsua_alterarSenha.Checked := False;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadUsuario.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadUsuario.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConUsuario_Usua_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadUsuario.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Usu�rio',mtWarning) = IDNO then
    Exit;

  Try
    DM.FDCon.StartTransaction;
    Usuario := TUsuario.Create;

    Usuario.Eliminar(edCodigo.AsInteger);

//    xAux_AuxJustificativa_Codigo   := edCodigo.AsInteger;
//    xAux_AuxJustificativa_NomeForm := UpperCase(Copy(Name,6,Length(Name)));
//    xAux_AuxJustificativa_Descricao:= edUsua_nome.Text;
//    AbreForm(TF_AuxJustificativa,Self);
//    xAux_AuxJustificativa_Codigo   := 0;
//    xAux_AuxJustificativa_NomeForm := '';
//    xAux_AuxJustificativa_Descricao:= '';

    msgPerson('Usu�rio eliminado com sucesso!',[mbok],'Usu�rio',mtConfirmation);

    FreeAndNil(Usuario);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadUsuario.icoSalvarClick(Sender: TObject);
var
  i                  : Integer;
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Usuario := TUsuario.Create;

    Usuario.usua_nome                := edUsua_nome.Text;
    Usuario.usua_login               := edUsua_login.Text;
    Usuario.usua_senha               := Criptografia(edUsua_senha.Text,xAux_SenhaAdmin);
    Usuario.usua_dataadmissao        := edUsua_Admissao.Date;
    Usuario.usua_datadesligamento    := edUsua_Desligamento.Date;
    Usuario.usua_fone                := edUsua_Fone.Text;
    Usuario.usua_obs                 := edUsua_Obs.Text;
    Usuario.usua_ativo               := rgUsua_Ativo.ItemIndex;
    Usuario.usua_email_nome          := edUsua_email_nome.Text;
    Usuario.usua_email               := edUsua_email.Text;
    Usuario.usua_email_usuario       := edUsua_email_usuario.Text;
    Usuario.usua_email_senha         := Criptografia(edUsua_email_senha.Text,xAux_SenhaAdmin);
//    Usuario.confe_id                 := edConfe_id.AsInteger;
    Usuario.usua_caminho_imp_pdf     := edusua_caminho_imp_pdf.Text;
    Usuario.usua_caminho_imp_padrao  := edusua_caminho_imp_padrao.Text;
    if ckUsua_alterarSenha.Checked = True then
      Usuario.usua_alterarSenha      := 1
    else
      Usuario.usua_alterarSenha      := 0;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConUsuario_Usua_ID     := Usuario.Ultima;
      Usuario.Gravar(xAux_ConUsuario_Usua_ID);
    end else begin
      Usuario.Gravar(xAux_ConUsuario_Usua_ID);
    end;

    FreeAndNil(Usuario);

//    if edCodigo.AsInteger = 0 then begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 1;
//      log.log_tabela      := 'USUARIO';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'GRAVADO C�DIGO: '+IntToStr(xAux_ConUsuario_Usua_ID)+' - '+edUsua_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end else begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 2;
//      log.log_tabela      := 'USUARIO';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'ALTERADO C�DIGO: '+IntToStr(xAux_ConUsuario_Usua_ID)+' - '+edUsua_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end;

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Usu�rio '+IntToStr(xAux_ConUsuario_Usua_ID)+' cadastrado com sucesso!',[mbOK], 'Usu�rio', mtConfirmation)
    else
      msgPerson('Usu�rio '+IntToStr(xAux_ConUsuario_Usua_ID)+' alterado com sucesso!',[mbOK], 'Usu�rio', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

end.
