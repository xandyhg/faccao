unit ConCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Mask, JvExMask, JvToolEdit, DateUtils, Vcl.Menus,
  frxClass, frxDBSet, JvExControls, JvSpeedButton;

type
  TF_ConCaixa = class(TF_ConPadrao)
    GroupBox1: TGroupBox;
    Label8: TLabel;
    edDataFim: TJvDateEdit;
    edDataIni: TJvDateEdit;
    FDCaixa: TFDQuery;
    ProviderCaixa: TDataSetProvider;
    CdsCaixa: TClientDataSet;
    DsCaixa: TDataSource;
    FDCaixaCX_ID: TIntegerField;
    FDCaixaENT_ID: TIntegerField;
    FDCaixaCX_DATAENTRADA: TSQLTimeStampField;
    FDCaixaCX_VALOR: TFloatField;
    FDCaixaCX_OBS: TStringField;
    FDCaixaCX_DEBCRED: TStringField;
    FDCaixaCX_FORMA_PGTO: TIntegerField;
    FDCaixaENT_NOME: TStringField;
    CdsCaixaCX_ID: TIntegerField;
    CdsCaixaENT_ID: TIntegerField;
    CdsCaixaCX_DATAENTRADA: TSQLTimeStampField;
    CdsCaixaCX_VALOR: TFloatField;
    CdsCaixaCX_OBS: TStringField;
    CdsCaixaCX_DEBCRED: TStringField;
    CdsCaixaCX_FORMA_PGTO: TIntegerField;
    CdsCaixaENT_NOME: TStringField;
    GroupBox2: TGroupBox;
    lblCredito: TLabel;
    lblDebito: TLabel;
    lblSaldo: TLabel;
    Panel5: TPanel;
    GroupBox4: TGroupBox;
    lblCreditoPeriodo: TLabel;
    lblDebitoPeriodo: TLabel;
    lblSaldoPeriodo: TLabel;
    PopupMenu1: TPopupMenu;
    frxCaixa: TfrxDBDataset;
    RelatriodeCaixa1: TMenuItem;
    JvSpeedButton1: TJvSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RelatriodeCaixa1Click(Sender: TObject);
    procedure CdsCaixaCX_FORMA_PGTOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConCaixa          : TF_ConCaixa;
  xAux_ConCaixa_cx_id : Integer;

implementation

{$R *.dfm}

uses DMC, CadCaixa, clsCaixa, X, Principal;

var
  tq        : TFDQuery;
  Caixa     : TCaixa;

procedure TF_ConCaixa.CdsCaixaCX_FORMA_PGTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '0' then
    Text := 'CHEQUE'
  else if Sender.AsString = '1' then
    Text := 'DEP�SITO'
  else if Sender.AsString = '2' then
    Text := 'DINHEIRO'
  else if Sender.AsString = '3' then
    Text := 'DOC'
  else if Sender.AsString = '4' then
    Text := 'PIX'
  else if Sender.AsString = '5' then
    Text := 'TED'
  else if Sender.AsString = '6' then
    Text := 'FECHAMENTO'
  else
    Text := '';
end;

procedure TF_ConCaixa.FormActivate(Sender: TObject);
begin
  inherited;
  edDataIni.Date := StartOfTheMonth(now);
  edDataFim.Date := EndOfTheMonth(now);
  icoPesquisarClick(Sender);
end;

procedure TF_ConCaixa.icoFecharClick(Sender: TObject);
begin
  xAux_ConCaixa_cx_id := CdsCaixa.FieldByName('cx_id').AsInteger;
  inherited;

end;

procedure TF_ConCaixa.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConCaixa_cx_id := 0;
  AbreForm(TF_CadCaixa, Self);
  icoPesquisarClick(Sender);
end;

procedure TF_ConCaixa.icoPesquisarClick(Sender: TObject);
var
  xTotCredito  : Double;
  xTotaDebito  : Double;
  xTotal       : Double;
  auxUsuaCad   : String;
  auxHistorico : String;
begin
  inherited;
  xTotCredito := 0;
  xTotaDebito := 0;

  CdsCaixa.Close;
  FDCaixa.Close;
  FDCaixa.Params.Clear;
  FDCaixa.SQL.Clear;
  FDCaixa.SQL.Add('select c.*, e.ent_nome                                                  '+
                  'from caixa c                                                            '+
                  'left outer join entidade e on c.ent_id = e.ent_id                       '+
                  'where c.cx_dataEntrada >= :k_dataIni and c.cx_dataEntrada <= :k_dataFim ');
  FDCaixa.SQL.Add('order by cx_dataentrada, cx_id                                          ');
  FDCaixa.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
  FDCaixa.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  CdsCaixa.Open;

  if not CdsCaixa.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsCaixa.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';

  while not CdsCaixa.Eof do begin
    if CdsCaixa.FieldByName('cx_debcred').AsString = 'C' then
      xTotCredito := xTotCredito + CdsCaixa.FieldByName('cx_valor').AsFloat
    else
      xTotaDebito := xTotaDebito + CdsCaixa.FieldByName('cx_valor').AsFloat;
    CdsCaixa.Next;
  end;

  xTotal := xTotCredito - xTotaDebito;

  lblCreditoPeriodo.Caption := 'R$ ' + Trim(Formata_Valor_decimal(xTotCredito,2));
  lblDebitoPeriodo.Caption  := 'R$ ' + Trim(Formata_Valor_decimal(xTotaDebito,2));
  lblSaldoPeriodo.Caption   := 'R$ ' + Trim(Formata_Valor_decimal(xTotCredito-xTotaDebito,2));

  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select                                                                          '+
             'Coalesce((sum(case when c.cx_debcred=''C'' then c.cx_valor end)),0) as Credito, '+
             'Coalesce((sum(case when c.cx_debcred=''D'' then c.cx_valor end)),0) as Debito,  '+
             'Coalesce((sum(case when c.cx_debcred=''C'' then c.cx_valor end)),0) -           '+
             'Coalesce((sum(case when c.cx_debcred=''D'' then c.cx_valor end)),0) as Saldo    '+
             'from caixa c                                                                    '+
             'left outer join entidade e on c.ent_id = e.ent_id                               ');
  tq.Open;

  lblCredito.Caption := 'R$ ' + Trim(Formata_Valor_decimal(tq.FieldByName('credito').AsFloat,2));
  lblDebito.Caption  := 'R$ ' + Trim(Formata_Valor_decimal(tq.FieldByName('debito').AsFloat,2));
  lblSaldo.Caption   := 'R$ ' + Trim(Formata_Valor_decimal(tq.FieldByName('saldo').AsFloat,2));

  tq.Close;
  tq.Free;
end;

procedure TF_ConCaixa.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConCaixa_cx_id  := CdsCaixa.FieldByName('cx_id').AsInteger;
  AbreForm(TF_CadCaixa, Self);
  icoPesquisarClick(Sender);
end;

procedure TF_ConCaixa.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if Column.FieldName = 'CX_DEBCRED' then begin
    if CdsCaixa.FieldByName('cx_debcred').AsString = 'D' then begin
      JVDBGrid1.Canvas.Brush.Color  := clRed;
      JvDBGrid1.Canvas.Font.Color   := clWhite;
    end else begin
      if CdsCaixa.FieldByName('cx_debcred').AsString = 'C' then begin
        JVDBGrid1.Canvas.Brush.Color  := clGreen;
        JVDBGrid1.Canvas.Font.Color   := clWhite;
      end;
    end;
  end;

  JvDBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TF_ConCaixa.RelatriodeCaixa1Click(Sender: TObject);
var
  caminho : String;
  arquivo : String;
begin
  inherited;
  caminho     := ExtractFilePath(Application.ExeName);
  arquivo     := 'logo.jpg';

  DM.frxGenerico.Clear;
  DM.frxGenerico.LoadFromFile(ExtractFilePath(Application.ExeName)+'Fast\ImpCaixa.fr3');
  DM.frxGenerico.Variables['usuario']        := QuotedStr(xAux_NomeUsuario);
  DM.frxGenerico.Variables['DataIni']        := QuotedStr(DateToStr(edDataIni.Date));
  DM.frxGenerico.Variables['DataFim']        := QuotedStr(DateToStr(edDataFim.Date));
  DM.frxGenerico.Variables['CreditoPeriodo'] := QuotedStr(lblCreditoPeriodo.Caption);
  DM.frxGenerico.Variables['DebitoPeriodo']  := QuotedStr(lblDebitoPeriodo.Caption);
  DM.frxGenerico.Variables['SaldoPeriodo']   := QuotedStr(lblSaldoPeriodo.Caption);
  DM.frxGenerico.Variables['Saldo']          := QuotedStr(lblSaldo.Caption);

  if ( FileExists(caminho+arquivo)) then begin
    if (DM.frxGenerico.FindObject('Picture1')<> nil )  then
      TfrxPictureView(DM.frxGenerico.FindObject('Picture1')).Picture.LoadFromFile(caminho+arquivo);
  end;

  DM.frxGenerico.ShowReport;
end;

end.
