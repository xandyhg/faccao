unit clsLote;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TLote = class
  public
    ent_id              : Integer;
    lote_id             : Integer;
    Lote_descricao      : String;
    Lote_datacadastro   : TDateTime;
    Lote_datafechamento : TDateTime;
    Lote_valor          : Double;
    Lote_obs            : String;
    lote_numero         : Integer;

    function  Recuperar(Pent_id,Plote_id:Integer):Boolean;
    procedure Gravar(Pent_id,Plote_id:Integer);
    procedure Eliminar(Pent_id,Plote_id:Integer);
    function  Ultima(Pent_id:Integer):Integer;
    function  RecuperaPorNumeroLote(PEnt_id,Plote_numero:Integer):Integer;
  end;

implementation

{ TLote }

uses DMC;

procedure TLote.Eliminar(Pent_id,Plote_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Lote where ent_id = :K_ent_id and lote_id = :k_lote_id');
  tq.ParamByName('K_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('K_lote_id').AsInteger := Plote_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TLote.Gravar(Pent_id,Plote_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Lote where ent_id = :k_ent_id and lote_id = :k_lote_id');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('k_lote_id').AsInteger := Plote_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Lote values('+
    ':k_ent_id,'+
    ':k_lote_id,'+
    ':k_Lote_descricao,'+
    ':k_Lote_datacadastro,'+
    ':k_Lote_datafechamento,'+
    ':k_Lote_valor,'+
    ':k_Lote_obs,'+
    ':k_lote_numero)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Lote set '+
    'Lote_descricao      = :k_Lote_descricao,'+
    'Lote_datacadastro   = :k_Lote_datacadastro,'+
    'Lote_datafechamento = :k_Lote_datafechamento,'+
    'Lote_valor          = :k_Lote_valor,'+
    'Lote_obs            = :k_Lote_obs,'+
    'lote_numero         = :k_lote_numero ');
    tq.SQL.Add('where ent_id = :K_ent_id and lote_id = :k_lote_id');
  end;

  tq.ParamByName('k_ent_id').AsInteger               := Pent_id;
  tq.ParamByName('k_lote_id').AsInteger              := Plote_id;
  tq.ParamByName('k_Lote_descricao').AsString        := Lote_descricao;
  tq.ParamByName('k_Lote_datacadastro').AsDateTime   := Lote_datacadastro;
  tq.ParamByName('k_Lote_datafechamento').AsDateTime := Lote_datafechamento;
  tq.ParamByName('k_Lote_valor').AsFloat             := Lote_valor;
  tq.ParamByName('k_Lote_obs').AsString              := Lote_obs;
  tq.ParamByName('k_lote_numero').AsInteger          := lote_numero;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function TLote.RecuperaPorNumeroLote(PEnt_id,Plote_numero: Integer): Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select lote_id from Lote                              '+
             'where lote_numero=:k_lote_numero and ent_id=:k_ent_id ');
  tq.ParamByName('K_ent_id').AsInteger      := PEnt_id;
  tq.ParamByName('k_lote_numero').AsInteger := Plote_numero;
  tq.Open;

  if tq.Eof then
    Result := 0
  else
    Result := tq.FieldByName('lote_id').AsInteger;

  tq.Close;
  tq.Free;
end;

function  TLote.Recuperar(Pent_id,Plote_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Lote where ent_id = :K_ent_id and lote_id = :k_lote_id');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('k_lote_id').AsInteger := Plote_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  ent_id              := tq.FieldByName('ent_id').AsInteger;
  lote_id             := tq.FieldByName('lote_id').AsInteger;
  Lote_descricao      := tq.FieldByName('Lote_descricao').AsString;
  Lote_datacadastro   := tq.FieldByName('Lote_datacadastro').AsDateTime;
  Lote_datafechamento := tq.FieldByName('Lote_datafechamento').AsDateTime;
  Lote_valor          := tq.FieldByName('Lote_valor').AsFloat;
  Lote_obs            := tq.FieldByName('Lote_obs').AsString;
  lote_numero         := tq.FieldByName('lote_numero').AsInteger;

  tq.Close;
  tq.Free;
end;

function TLote.Ultima(Pent_id:Integer): Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(lote_id) maior from Lote where ent_id = :k_ent_id');
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;

  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
