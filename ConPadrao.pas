unit ConPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids,
  JvDBGrid, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client, JvRichEdit;

type
  TF_ConPadrao = class(TForm)
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    Panel3: TPanel;
    Panel4: TPanel;
    pnPesquisar: TPanel;
    pnNovo: TPanel;
    pnImprimir: TPanel;
    pnFechar: TPanel;
    icoPesquisar: TImage;
    icoNovo: TImage;
    icoImprimir: TImage;
    icoFechar: TImage;
    lblRegTotal: TLabel;
    JvDBGrid1: TJvDBGrid;
    Panel1: TPanel;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure icoFecharClick(Sender: TObject);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure icoPesquisarClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoImprimirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConPadrao: TF_ConPadrao;

implementation

{$R *.dfm}

uses Principal, X;

procedure TF_ConPadrao.FormActivate(Sender: TObject);
begin
//
end;

procedure TF_ConPadrao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  JvDBGrid1.Columns.SaveToFile(ExtractFilePath(Application.ExeName)+xAux_NomeUsuario+Copy(Name,3,Length(Name))+'.cfg');
end;

procedure TF_ConPadrao.FormCreate(Sender: TObject);
begin
  if IsDebuggerPresent then begin
    Caption := Caption + ' - BASE TESTE';
  end;
  Timer1Timer(Self);
  if FileExists(ExtractFilePath(Application.ExeName)+xAux_NomeUsuario+Copy(Name,3,Length(Name))+'.cfg') = True then
    JvDBGrid1.Columns.LoadFromFile(ExtractFilePath(Application.ExeName)+xAux_NomeUsuario+Copy(Name,3,Length(Name))+'.cfg');
end;

procedure TF_ConPadrao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F1) and (icoNovo.Visible = True) then
    icoNovo.OnClick(Self)
  else if (Key = VK_F5) and (icoImprimir.Visible = True) then
    icoImprimir.OnClick(Self)
  else if (Key = VK_F6) and (icoPesquisar.Visible = True) then
    icoPesquisar.OnClick(Self)
  else if (Key = VK_ESCAPE) and (icoFechar.Visible = True) then
    icoFechar.OnClick(Self);
end;

procedure TF_ConPadrao.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    if (not (ActiveControl is TMemo)) and (not (ActiveControl is TJvRichEdit)) then
      Perform(Wm_NextDlgCtl,0,0);
end;

procedure TF_ConPadrao.icoFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TF_ConPadrao.icoImprimirClick(Sender: TObject);
begin
  //
end;

procedure TF_ConPadrao.icoNovoClick(Sender: TObject);
begin
  //
end;

procedure TF_ConPadrao.icoPesquisarClick(Sender: TObject);
begin
  //
end;

procedure TF_ConPadrao.JvDBGrid1TitleClick(Column: TColumn);
var
  indice: string;
  existe: boolean;
  clientdataset_idx: tclientdataset;
begin
  clientdataset_idx := TClientDataset(Column.Grid.DataSource.DataSet);

  if clientdataset_idx.IndexFieldNames = Column.FieldName then begin
    indice := AnsiUpperCase(Column.FieldName+'_INV');

    try
      clientdataset_idx.IndexDefs.Find(indice);
      existe := true;
    except
      existe := false;
    end;

    if not existe then
      with clientdataset_idx.IndexDefs.AddIndexDef do begin
        Name := indice;
        Fields := Column.FieldName;
        Options := [ixDescending];
      end;

    clientdataset_idx.IndexName := indice;
  end else
    clientdataset_idx.IndexFieldNames := Column.FieldName;
end;

procedure TF_ConPadrao.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
var
  lCanvas : TCanvas;
begin
  //Verifica o �ndice do painel pois o desenho pode ser diferente p/ cada um
  if (Panel.Index = 0) or (Panel.Index = 1) or (Panel.Index = 2) or (Panel.Index = 3) then begin
    lCanvas := StatusBar1.Canvas;
    //Configura o fonte para exibir a mensagem
    StatusBar1.Font.Size := 18;
    StatusBar1.Font.Name := 'Segoe UI';
    StatusBar1.Font.Style := [fsBold];

    //Posiciona o texto no painel
    lCanvas.TextOut(Rect.Left + 5, Rect.Top -2, Panel.Text);
  end;
end;

procedure TF_ConPadrao.Timer1Timer(Sender: TObject);
begin
  //Informa Data Atual com dia da semana
  StatusBar1.Panels [0].Text := ' ' + formatdatetime ('dddd", "dd" de "mmmm" de "yyyy', now);
  //Informa Hora Atual
  StatusBar1.Panels[1].Text := TimeToStr(Now);
  //Informa o usu�rio logado no sistema
  StatusBar1.Panels[2].Text := xAux_NomeUsuario;
end;

end.
