unit AuxLote;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AuxPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits;

type
  TF_AuxLote = class(TF_AuxPadrao)
    Panel1: TPanel;
    edLote_Numero: TJvCalcEdit;
    procedure icoCancelarClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_AuxLote                : TF_AuxLote;
  xAux_AuxLote_Lote_Numero : Integer;

implementation

{$R *.dfm}

procedure TF_AuxLote.icoCancelarClick(Sender: TObject);
begin
  inherited;
  xAux_AuxLote_Lote_Numero := 0;
  Close;
end;

procedure TF_AuxLote.icoSalvarClick(Sender: TObject);
begin
  inherited;
  xAux_AuxLote_Lote_Numero := edLote_Numero.AsInteger;
  Close;
end;

end.
