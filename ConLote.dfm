inherited F_ConLote: TF_ConLote
  Caption = ' Consuta de Lote'
  ClientHeight = 519
  ClientWidth = 1263
  ExplicitWidth = 1279
  ExplicitHeight = 558
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 406
    Width = 1263
    ExplicitTop = 406
    ExplicitWidth = 1263
    inherited icoPesquisar: TImage
      Left = 843
      ExplicitLeft = 843
    end
    inherited icoNovo: TImage
      Left = 948
      ExplicitLeft = 948
    end
    inherited icoImprimir: TImage
      Left = 1053
      ExplicitLeft = 1053
    end
    inherited icoFechar: TImage
      Left = 1158
      ExplicitLeft = 1158
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 483
    Width = 1263
    ExplicitTop = 483
    ExplicitWidth = 1263
  end
  inherited Panel3: TPanel
    Width = 1263
    Height = 365
    ExplicitWidth = 1263
    ExplicitHeight = 365
    inherited JvDBGrid1: TJvDBGrid
      Width = 609
      Height = 365
      Align = alLeft
      DataSource = DsEntidade
      TitleRowHeight = 21
      Columns = <
        item
          Expanded = False
          FieldName = 'ENT_ID'
          Title.Caption = 'C'#243'digo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME'
          Title.Caption = 'Cliente'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_CELULAR'
          Title.Caption = 'Celular'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_EMAIL'
          Title.Caption = 'Email'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 166
          Visible = True
        end>
    end
    object JvDBGrid2: TJvDBGrid
      Left = 609
      Top = 0
      Width = 501
      Height = 365
      Align = alLeft
      DataSource = DsLote
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Segoe UI'
      TitleFont.Style = []
      OnDblClick = JvDBGrid2DblClick
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 21
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOTE_NUMERO'
          Title.Alignment = taCenter
          Title.Caption = 'N'#186
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOTE_DESCRICAO'
          Title.Caption = 'Descri'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 209
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOTE_DATAFECHAMENTO'
          Title.Alignment = taCenter
          Title.Caption = 'Fechamento'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOTE_VALOR'
          Title.Caption = 'Valor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 105
          Visible = True
        end>
    end
    object DBMemo1: TDBMemo
      Left = 1110
      Top = 0
      Width = 153
      Height = 365
      Align = alClient
      DataField = 'LOTE_OBS'
      DataSource = DsLote
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 2
      ExplicitLeft = 1158
      ExplicitWidth = 105
    end
  end
  inherited Panel4: TPanel
    Top = 451
    Width = 1263
    ExplicitTop = 451
    ExplicitWidth = 1263
    inherited pnPesquisar: TPanel
      Left = 843
      ExplicitLeft = 843
    end
    inherited pnNovo: TPanel
      Left = 948
      ExplicitLeft = 948
    end
    inherited pnImprimir: TPanel
      Left = 1053
      ExplicitLeft = 1053
    end
    inherited pnFechar: TPanel
      Left = 1158
      ExplicitLeft = 1158
    end
  end
  inherited Panel1: TPanel
    Width = 1263
    ExplicitWidth = 1263
  end
  object FDLote: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select l.ent_id, l.lote_id, l.lote_descricao, l.lote_datafechame' +
        'nto, l.lote_valor, l.lote_obs, l.lote_numero'
      'from lote l'
      'order by l.ent_id, l.lote_datafechamento')
    Left = 520
    Top = 8
    object FDLoteENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDLoteLOTE_ID: TIntegerField
      FieldName = 'LOTE_ID'
      Origin = 'LOTE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDLoteLOTE_DESCRICAO: TStringField
      FieldName = 'LOTE_DESCRICAO'
      Origin = 'LOTE_DESCRICAO'
      Size = 255
    end
    object FDLoteLOTE_DATAFECHAMENTO: TSQLTimeStampField
      FieldName = 'LOTE_DATAFECHAMENTO'
      Origin = 'LOTE_DATAFECHAMENTO'
    end
    object FDLoteLOTE_VALOR: TFloatField
      FieldName = 'LOTE_VALOR'
      Origin = 'LOTE_VALOR'
    end
    object FDLoteLOTE_OBS: TMemoField
      FieldName = 'LOTE_OBS'
      Origin = 'LOTE_OBS'
      BlobType = ftMemo
    end
    object FDLoteLOTE_NUMERO: TIntegerField
      FieldName = 'LOTE_NUMERO'
      Origin = 'LOTE_NUMERO'
    end
  end
  object ProviderLote: TDataSetProvider
    DataSet = FDLote
    Left = 552
    Top = 8
  end
  object CdsLote: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'ProviderLote'
    Left = 584
    Top = 8
    object CdsLoteENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Required = True
    end
    object CdsLoteLOTE_ID: TIntegerField
      FieldName = 'LOTE_ID'
      Required = True
    end
    object CdsLoteLOTE_DESCRICAO: TStringField
      FieldName = 'LOTE_DESCRICAO'
      Size = 255
    end
    object CdsLoteLOTE_DATAFECHAMENTO: TSQLTimeStampField
      FieldName = 'LOTE_DATAFECHAMENTO'
    end
    object CdsLoteLOTE_VALOR: TFloatField
      FieldName = 'LOTE_VALOR'
      DisplayFormat = '#,##0.00'
    end
    object CdsLoteLOTE_OBS: TMemoField
      FieldName = 'LOTE_OBS'
      BlobType = ftMemo
    end
    object CdsLoteLOTE_NUMERO: TIntegerField
      FieldName = 'LOTE_NUMERO'
    end
  end
  object DsLote: TDataSource
    DataSet = CdsLote
    Left = 616
    Top = 8
  end
  object FDEntidade: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select distinct e.ent_id, e.ent_nome, e.ent_celular, e.ent_email'
      'from entidade e'
      'order by e.ent_nome')
    Left = 72
    Top = 8
    object FDEntidadeENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDEntidadeENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      Origin = 'ENT_NOME'
      Size = 250
    end
    object FDEntidadeENT_CELULAR: TStringField
      FieldName = 'ENT_CELULAR'
      Origin = 'ENT_CELULAR'
    end
    object FDEntidadeENT_EMAIL: TStringField
      FieldName = 'ENT_EMAIL'
      Origin = 'ENT_EMAIL'
      Size = 255
    end
  end
  object ProviderEntidade: TDataSetProvider
    DataSet = FDEntidade
    Left = 104
    Top = 8
  end
  object CdsEntidade: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderEntidade'
    Left = 136
    Top = 8
    object CdsEntidadeENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Required = True
    end
    object CdsEntidadeENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      Size = 250
    end
    object CdsEntidadeENT_CELULAR: TStringField
      FieldName = 'ENT_CELULAR'
    end
    object CdsEntidadeENT_EMAIL: TStringField
      FieldName = 'ENT_EMAIL'
      Size = 255
    end
  end
  object DsEntidade: TDataSource
    DataSet = CdsEntidade
    OnDataChange = DsEntidadeDataChange
    Left = 168
    Top = 8
  end
end
