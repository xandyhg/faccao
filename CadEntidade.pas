unit CadEntidade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, Vcl.ComCtrls, FireDAC.Comp.Client, Vcl.Buttons,
  Soap.InvokeRegistry, Soap.Rio, Soap.SOAPHTTPClient, WinInet;

type
  TF_CadEntidade = class(TF_CadPadrao)
    edCodigo: TJvCalcEdit;
    Label1: TLabel;
    edEnt_DataCadastro: TJvDateEdit;
    Label3: TLabel;
    HTTPRIO1: THTTPRIO;
    Panel9: TPanel;
    Panel7: TPanel;
    Label2: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label17: TLabel;
    edEnt_nome: TEdit;
    edEnt_Telefone: TEdit;
    edEnt_Celular: TEdit;
    edEnt_email: TEdit;
    edEnt_Obs: TMemo;
    rgEnt_Ativo: TRadioGroup;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure edEnt_CelularEnter(Sender: TObject);
    procedure edEnt_CelularExit(Sender: TObject);
    procedure edEnt_TelefoneEnter(Sender: TObject);
    procedure edEnt_TelefoneExit(Sender: TObject);
    procedure edEnt_emailExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadEntidade         : TF_CadEntidade;

implementation

{$R *.dfm}

uses DMC, clsEntidade, ConEntidade, X, Principal;

var
  Entidade : TEntidade;

procedure TF_CadEntidade.edEnt_emailExit(Sender: TObject);
begin
  inherited;
  if Trim(edEnt_email.Text) <> '' then begin
    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorEmail(edEnt_email.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo email!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;


procedure TF_CadEntidade.edEnt_CelularEnter(Sender: TObject);
begin
  inherited;
  edEnt_Celular.Text := SomenteNumero(edEnt_Celular.Text);
  edEnt_Celular.SelectAll;
end;

procedure TF_CadEntidade.edEnt_CelularExit(Sender: TObject);
var
  xFone : String;
begin
  inherited;
  xFone := Trim(TiraZeroEsquerda(SomenteNumero(edEnt_Celular.Text))); //Celular do Cliente
  if Length(xFone) = 8 then
    edEnt_Celular.Text := Copy(xFone,1,4)+'-'+Copy(xFone,5,4) //Telefone normal ou celular sem o d�gito 9, sem o DDD
  else if Length(xFone) = 9 then
    edEnt_Celular.Text := Copy(xFone,1,5)+'-'+Copy(xFone,6,4) //Telefone Celular, com o d�gito 9 a mais, sem DDD
  else if Length(xFone) = 10 then
    edEnt_Celular.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,4)+'-'+Copy(xFone,7,4) //Telefone normal ou celular sem o d�gito 9, com o DDD
  else if Length(xFone) = 11 then
    edEnt_Celular.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,5)+'-'+Copy(xFone,8,4) //Telefone Celular, com o d�gito 9 a mais, com DDD
  else
    edEnt_Celular.Text := Copy(StringOfChar(' ',15),1,15);
  if Trim(edEnt_Celular.Text) <> '' then begin
    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorCelular(edEnt_Celular.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo celular!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadEntidade.edEnt_TelefoneEnter(Sender: TObject);
begin
  inherited;
  edEnt_Telefone.Text := SomenteNumero(edEnt_Telefone.Text);
  edEnt_Telefone.SelectAll;
end;

procedure TF_CadEntidade.edEnt_TelefoneExit(Sender: TObject);
var
  xFone : String;
begin
  inherited;
  xFone := Trim(TiraZeroEsquerda(SomenteNumero(edEnt_Telefone.Text))); //Fixo do Cliente
  if Length(xFone) = 8 then
    edEnt_Telefone.Text := Copy(xFone,1,4)+'-'+Copy(xFone,5,4) //Telefone normal ou celular sem o d�gito 9, sem o DDD
  else if Length(xFone) = 9 then
    edEnt_Telefone.Text := Copy(xFone,1,5)+'-'+Copy(xFone,6,4) //Telefone Celular, com o d�gito 9 a mais, sem DDD
  else if Length(xFone) = 10 then
    edEnt_Telefone.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,4)+'-'+Copy(xFone,7,4) //Telefone normal ou celular sem o d�gito 9, com o DDD
  else if Length(xFone) = 11 then
    edEnt_Telefone.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,5)+'-'+Copy(xFone,8,4) //Telefone Celular, com o d�gito 9 a mais, com DDD
  else
    edEnt_Telefone.Text := Copy(StringOfChar(' ',15),1,15);
  if Trim(edEnt_Telefone.Text) <> '' then begin
    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorTelefone(edEnt_Telefone.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo telefone!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadEntidade.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConEntidade_Ent_ID > 0 then begin
    Entidade := TEntidade.Create;
    if Entidade.Recuperar(xAux_ConEntidade_Ent_ID) then begin
      edCodigo.AsInteger           := xAux_ConEntidade_Ent_ID;
      edEnt_nome.Text              := Entidade.ent_nome;
      edEnt_DataCadastro.Date      := Entidade.ent_datacadastro;
      edEnt_Celular.Text           := Entidade.ent_celular;
      edEnt_Telefone.Text          := Entidade.ent_telefone;
      edEnt_email.Text             := Entidade.ent_email;
      rgEnt_Ativo.ItemIndex        := Entidade.ent_ativo;
      edEnt_Obs.Text               := Entidade.ent_obs;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(Entidade);
  end else begin
    edEnt_nome.Clear;
    edEnt_DataCadastro.Date      := Date;
    edEnt_Celular.Clear;
    edEnt_Telefone.Clear;
    edEnt_email.Clear;
    rgEnt_Ativo.ItemIndex        := 0;
    edEnt_Obs.Clear;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadEntidade.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadEntidade.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConEntidade_Ent_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadEntidade.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Entidade',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Entidade := TEntidade.Create;

    Entidade.Eliminar(edCodigo.AsInteger);

    msgPerson('Registro eliminado com sucesso!',[mbok],'Entidade',mtConfirmation);

    FreeAndNil(Entidade);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadEntidade.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Entidade := TEntidade.Create;

    Entidade.ent_nome            := edEnt_nome.Text;
    Entidade.ent_datacadastro    := edEnt_DataCadastro.Date;
    Entidade.ent_celular         := edEnt_Celular.Text;
    Entidade.ent_telefone        := edEnt_Telefone.Text;
    Entidade.ent_email           := edEnt_email.Text;
    Entidade.ent_ativo           := rgEnt_Ativo.ItemIndex;
    Entidade.ent_obs             := edEnt_Obs.Text;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConEntidade_Ent_ID := Entidade.Ultima;
      Entidade.Gravar(xAux_ConEntidade_Ent_ID);
    end else begin
      Entidade.Gravar(xAux_ConEntidade_Ent_ID);
    end;

    FreeAndNil(Entidade);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Entidade '+IntToStr(xAux_ConEntidade_Ent_ID)+' cadastrada com sucesso!',[mbOK], 'Entidade', mtConfirmation)
    else
      msgPerson('Entidade '+IntToStr(xAux_ConEntidade_Ent_ID)+' alterada com sucesso!',[mbOK], 'Entidade', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

end.
