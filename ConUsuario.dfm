inherited F_ConUsuario: TF_ConUsuario
  Caption = 'Consulta de Usu'#225'rios'
  ClientHeight = 528
  ClientWidth = 946
  ExplicitWidth = 962
  ExplicitHeight = 567
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 415
    Width = 946
    ExplicitTop = 404
    ExplicitWidth = 946
    inherited icoPesquisar: TImage
      Left = 526
      ExplicitLeft = 526
    end
    inherited icoNovo: TImage
      Left = 631
      ExplicitLeft = 631
    end
    inherited icoImprimir: TImage
      Left = 736
      ExplicitLeft = 736
    end
    inherited icoFechar: TImage
      Left = 841
      ExplicitLeft = 841
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 492
    Width = 946
    ExplicitTop = 492
    ExplicitWidth = 946
  end
  inherited Panel3: TPanel
    Width = 946
    Height = 374
    ExplicitWidth = 946
    ExplicitHeight = 363
    inherited JvDBGrid1: TJvDBGrid
      Width = 946
      Height = 363
      DataSource = DsUsuario
      OnDrawColumnCell = JvDBGrid1DrawColumnCell
      OnDblClick = JvDBGrid1DblClick
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'USUA_ATIVO'
          Title.Alignment = taCenter
          Title.Caption = 'Ativo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 57
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'USUA_ID'
          Title.Caption = 'C'#243'digo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'USUA_NOME'
          Title.Caption = 'Nome'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 271
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'USUA_LOGIN'
          Title.Alignment = taCenter
          Title.Caption = 'Login'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 126
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'USUA_DATAADMISSAO'
          Title.Alignment = taCenter
          Title.Caption = 'Admiss'#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 92
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'USUA_DATADESLIGAMENTO'
          Title.Alignment = taCenter
          Title.Caption = 'Desligamento'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'USUA_FONE'
          Title.Caption = 'Telefone'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Top = 460
    Width = 946
    ExplicitTop = 455
    ExplicitWidth = 946
    inherited pnPesquisar: TPanel
      Left = 526
      ExplicitLeft = 526
    end
    inherited pnNovo: TPanel
      Left = 631
      ExplicitLeft = 631
    end
    inherited pnImprimir: TPanel
      Left = 736
      ExplicitLeft = 736
    end
    inherited pnFechar: TPanel
      Left = 841
      ExplicitLeft = 841
    end
  end
  inherited Panel1: TPanel
    Width = 946
    ExplicitWidth = 946
  end
  object FDUsuario: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select u.usua_id, u.usua_nome, u.usua_login, u.usua_dataadmissao' +
        ', u.usua_datadesligamento,'
      'u.usua_fone, u.usua_ativo'
      'from usuario u'
      'order by u.usua_nome')
    Left = 720
    Top = 8
    object FDUsuarioUSUA_ID: TIntegerField
      FieldName = 'USUA_ID'
      Origin = 'USUA_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDUsuarioUSUA_NOME: TStringField
      FieldName = 'USUA_NOME'
      Origin = 'USUA_NOME'
      Required = True
      Size = 250
    end
    object FDUsuarioUSUA_LOGIN: TStringField
      FieldName = 'USUA_LOGIN'
      Origin = 'USUA_LOGIN'
      Required = True
      Size = 50
    end
    object FDUsuarioUSUA_DATAADMISSAO: TSQLTimeStampField
      FieldName = 'USUA_DATAADMISSAO'
      Origin = 'USUA_DATAADMISSAO'
    end
    object FDUsuarioUSUA_DATADESLIGAMENTO: TSQLTimeStampField
      FieldName = 'USUA_DATADESLIGAMENTO'
      Origin = 'USUA_DATADESLIGAMENTO'
    end
    object FDUsuarioUSUA_FONE: TStringField
      FieldName = 'USUA_FONE'
      Origin = 'USUA_FONE'
      Size = 15
    end
    object FDUsuarioUSUA_ATIVO: TIntegerField
      FieldName = 'USUA_ATIVO'
      Origin = 'USUA_ATIVO'
    end
  end
  object ProviderUsuario: TDataSetProvider
    DataSet = FDUsuario
    Left = 752
    Top = 8
  end
  object CdsUsuario: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderUsuario'
    Left = 784
    Top = 8
    object CdsUsuarioUSUA_ID: TIntegerField
      FieldName = 'USUA_ID'
      Required = True
    end
    object CdsUsuarioUSUA_NOME: TStringField
      FieldName = 'USUA_NOME'
      Required = True
      Size = 250
    end
    object CdsUsuarioUSUA_LOGIN: TStringField
      FieldName = 'USUA_LOGIN'
      Required = True
      Size = 50
    end
    object CdsUsuarioUSUA_DATAADMISSAO: TSQLTimeStampField
      FieldName = 'USUA_DATAADMISSAO'
      OnGetText = CdsUsuarioUSUA_DATAADMISSAOGetText
    end
    object CdsUsuarioUSUA_DATADESLIGAMENTO: TSQLTimeStampField
      FieldName = 'USUA_DATADESLIGAMENTO'
      OnGetText = CdsUsuarioUSUA_DATADESLIGAMENTOGetText
    end
    object CdsUsuarioUSUA_FONE: TStringField
      FieldName = 'USUA_FONE'
      Size = 15
    end
    object CdsUsuarioUSUA_ATIVO: TIntegerField
      FieldName = 'USUA_ATIVO'
      OnGetText = CdsUsuarioUSUA_ATIVOGetText
    end
  end
  object DsUsuario: TDataSource
    DataSet = CdsUsuario
    Left = 816
    Top = 8
  end
end
