inherited F_AuxPeriodo: TF_AuxPeriodo
  Caption = 'Informe o cliente e o per'#237'odo para imprimir o extrato'
  ClientHeight = 184
  ClientWidth = 426
  OnActivate = FormActivate
  ExplicitWidth = 442
  ExplicitHeight = 223
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    Top = 107
    Width = 426
    ExplicitTop = 107
    ExplicitWidth = 426
    inherited icoFechar: TImage
      Left = 336
      Visible = False
      ExplicitLeft = 336
    end
    inherited icoSalvar: TImage
      Left = 156
      OnClick = icoSalvarClick
      ExplicitLeft = 156
    end
    inherited icoCancelar: TImage
      Left = 246
      OnClick = icoCancelarClick
      ExplicitLeft = 246
    end
  end
  inherited Panel3: TPanel
    Width = 426
    Height = 107
    ExplicitWidth = 426
    ExplicitHeight = 107
    object Label59: TLabel
      Left = 7
      Top = 16
      Width = 39
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object sbEntidade: TSpeedButton
      Left = 117
      Top = 13
      Width = 25
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000585751747069
        E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
        F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
        8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
        FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
        FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
        FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
        B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
        A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
        3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
        CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
        74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
        F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
        B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
        C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
        999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
      ParentFont = False
      OnClick = sbEntidadeClick
    end
    object edEnt_id: TJvCalcEdit
      Left = 52
      Top = 13
      Width = 59
      Height = 25
      DecimalPlaces = 0
      DisplayFormat = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
      OnExit = edEnt_idExit
      OnKeyDown = edEnt_idKeyDown
    end
    object edEnt_Nome: TEdit
      Left = 148
      Top = 13
      Width = 269
      Height = 25
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object GroupBox4: TGroupBox
      Left = 52
      Top = 44
      Width = 241
      Height = 53
      Caption = ' Per'#237'odo '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object Label8: TLabel
        Left = 117
        Top = 23
        Width = 7
        Height = 17
        Alignment = taRightJustify
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edDataFim: TJvDateEdit
        Left = 130
        Top = 20
        Width = 105
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 1
      end
      object edDataIni: TJvDateEdit
        Left = 6
        Top = 20
        Width = 105
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
      end
    end
  end
  inherited Panel4: TPanel
    Top = 152
    Width = 426
    ExplicitTop = 152
    ExplicitWidth = 426
    inherited pnCancelar: TPanel
      Left = 246
      ExplicitLeft = 246
    end
    inherited pnSalvar: TPanel
      Left = 156
      ExplicitLeft = 156
    end
    inherited pnFechar: TPanel
      Left = 336
      Visible = False
      ExplicitLeft = 336
    end
  end
  inherited Timer1: TTimer
    Left = 323
    Top = 0
  end
end
