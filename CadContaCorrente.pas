unit CadContaCorrente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, JvToolEdit, Vcl.StdCtrls, Vcl.Mask, JvExMask,
  JvBaseEdits, FireDAC.Comp.Client;

type
  TF_CadContaCorrente = class(TF_CadPadrao)
    edCodigo: TJvCalcEdit;
    Label1: TLabel;
    Label3: TLabel;
    edCodigo2: TJvCalcEdit;
    edCC_data: TJvDateEdit;
    Label6: TLabel;
    edCodigo3: TJvCalcEdit;
    Label2: TLabel;
    edCC_datalancamento: TJvDateEdit;
    Label4: TLabel;
    rgCC_tipo: TRadioGroup;
    cbCC_tipo_transacao: TComboBox;
    Label5: TLabel;
    edCC_valor: TJvCalcEdit;
    Label7: TLabel;
    edCC_descricao: TEdit;
    Label8: TLabel;
    rgCC_Conferido: TRadioGroup;
    edcc_ordem: TJvCalcEdit;
    Label9: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadContaCorrente: TF_CadContaCorrente;

implementation

{$R *.dfm}

uses DMC, X, Principal, clsContaCorrente, ConContaCorrente, clsLote, AuxLote,
  clsCaixa;

var
  CC    : TContaCorrente;
  Lote  : TLote;
  Caixa : TCaixa;

procedure TF_CadContaCorrente.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConCC_cc_seq > 0 then begin
    CC := TContaCorrente.Create;
    if CC.Recuperar(xAux_ConCC_ent_ID,xAux_ConCC_lote_ID,xAux_ConCC_cc_seq) then begin
      edCodigo.AsInteger            := xAux_ConCC_ent_ID;
      edCodigo2.AsInteger           := xAux_ConCC_lote_ID;
      edCodigo3.AsInteger           := xAux_ConCC_cc_seq;
      rgCC_tipo.ItemIndex           := CC.cc_tipo;
      cbCC_tipo_transacao.ItemIndex := CC.cc_tipo_transacao;
      edCC_descricao.Text           := CC.cc_descricao;
      edCC_data.Date                := CC.cc_data;
      edCC_valor.Value              := CC.cc_valor;
      edCC_datalancamento.Date      := CC.cc_datalancamento;
      rgCC_Conferido.ItemIndex      := CC.cc_conferido;
      edcc_ordem.AsInteger          := CC.cc_ordem;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(CC);
  end else begin
    edCodigo.AsInteger            := xAux_ConCC_ent_ID;
    edCodigo2.AsInteger           := xAux_ConCC_lote_ID;
    rgCC_tipo.ItemIndex           := 1;
    cbCC_tipo_transacao.ItemIndex := 4;
    edCC_descricao.Clear;
    edCC_data.Date                := Date;
    edCC_valor.Clear;
    edCC_datalancamento.Date      := Date;
    rgCC_Conferido.ItemIndex      := 0;
    CC := TContaCorrente.Create;
    edcc_ordem.AsInteger          := CC.UltimaOrdem(xAux_ConCC_ent_ID);
    FreeAndNil(CC);

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadContaCorrente.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadContaCorrente.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConCC_cc_seq > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadContaCorrente.icoExcluirClick(Sender: TObject);
var
  xAuxCaixaID : Integer;
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Conta Corrente',mtWarning) = IDNO then
    Exit;

  Caixa := TCaixa.Create;
  if Caixa.RecuperarLancCC(edCodigo.AsInteger,edCodigo2.AsInteger,edCodigo3.AsInteger) then begin
    xAuxCaixaID := Caixa.cx_id;
    Caixa.Eliminar(xAuxCaixaID);
    msgPerson('Registro do Caixa eliminado com sucesso!',[mbok],'Conta Corrente',mtConfirmation);
    FreeAndNil(Caixa);
  end;
  FreeAndNil(Caixa);

  try
    DM.FDCon.StartTransaction;
    CC := TContaCorrente.Create;

    CC.Eliminar(edCodigo.AsInteger,edCodigo2.AsInteger,edCodigo3.AsInteger);

    msgPerson('Lan�amento eliminado com sucesso!',[mbok],'Conta Corrente',mtConfirmation);

    FreeAndNil(CC);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadContaCorrente.icoSalvarClick(Sender: TObject);
var
  xRestante    : Double;
  xAux_CaixaID : Integer;
begin
  inherited;
//  CC        := TContaCorrente.Create;
//  Lote      := TLote.Create;
//  xRestante := 0;
//  if Lote.Recuperar(edCodigo.AsInteger,edCodigo2.AsInteger) then begin
//    xRestante := Lote.Lote_valor - CC.SomaValorLoteRecebido(edCodigo.AsInteger,edCodigo2.AsInteger);
//    if edCC_valor.Value > xRestante then begin
//      if msgPerson('O valor ultrapassa o valor do lote! Deseja lan�ar a diferen�a em outro lote?',[mbYes,mbNo],'Conta Corrente',mtWarning) = IDNO then begin
//        FreeAndNil(CC);
//        FreeAndNil(Lote);
//        Exit;
//      end;
//      xAux_AuxLote_Lote_Numero := 0;
//      AbreForm(TF_AuxLote,Self);
//
//      if (xAux_AuxLote_Lote_Numero > 0)  then begin
//        if Lote.Recuperar(edCodigo.AsInteger,edCodigo2.AsInteger) then begin
//          CC.cc_tipo           := rgCC_tipo.ItemIndex;
//          CC.cc_tipo_transacao := cbCC_tipo_transacao.ItemIndex;
//          CC.cc_descricao      := 'RESTANTE DO LOTE '+IntToStr(Lote.lote_numero)+' ('+edCC_descricao.Text+')';
//          CC.cc_data           := Date;
//          CC.cc_valor          := xRestante*(-1);
////          CC.cc_valor          := edCC_valor.Value+xRestante;
////          edCC_valor.Value     := xRestante;
//          CC.cc_datalancamento := edCC_datalancamento.Date;
//          CC.Gravar(edCodigo.AsInteger,
//                    Lote.RecuperaPorNumeroLote(edCodigo.AsInteger,xAux_AuxLote_Lote_Numero),
//                    CC.Ultima(edCodigo.AsInteger,
//                              Lote.RecuperaPorNumeroLote(edCodigo.AsInteger,xAux_AuxLote_Lote_Numero)));
//        end else begin
//          msgPerson('Lote '+IntToStr(xAux_AuxLote_Lote_Numero)+' n�o est� cadastrado!'+#13+'Fa�a o cadastro para lan�ar a diferen�a!',[mbOK], 'Conta Corrente', mtConfirmation);
//          FreeAndNil(CC);
//          FreeAndNil(Lote);
//          Exit;
//        end;
//      end else begin
//        FreeAndNil(CC);
//        FreeAndNil(Lote);
//        Exit;
//      end;
//    end;
//  end;
//  FreeAndNil(CC);
//  FreeAndNil(Lote);

  Try
    DM.FDCon.StartTransaction;
    CC := TContaCorrente.Create;

    CC.cc_tipo           := rgCC_tipo.ItemIndex;
    CC.cc_tipo_transacao := cbCC_tipo_transacao.ItemIndex;
    CC.cc_descricao      := edCC_descricao.Text;
    CC.cc_data           := edCC_data.Date;
    CC.cc_valor          := edCC_valor.Value;
    CC.cc_datalancamento := edCC_datalancamento.Date;
    CC.cc_conferido      := rgCC_Conferido.ItemIndex;
    CC.cc_ordem          := edcc_ordem.AsInteger;

    if edCodigo3.AsInteger = 0 then begin
      xAux_ConCC_cc_seq     := CC.Ultima(xAux_ConCC_ent_ID,xAux_ConCC_lote_ID);
      CC.Gravar(xAux_ConCC_ent_ID,xAux_ConCC_lote_ID,xAux_ConCC_cc_seq);
    end else begin
      CC.Gravar(xAux_ConCC_ent_ID,xAux_ConCC_lote_ID,xAux_ConCC_cc_seq);
    end;

    FreeAndNil(CC);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo3.AsInteger = 0 then
      msgPerson('Lan�amento '+IntToStr(xAux_ConCC_cc_seq)+' cadastrado com sucesso!',[mbOK], 'Conta Corrente', mtConfirmation)
    else
      msgPerson('Lan�amento '+IntToStr(xAux_ConCC_cc_seq)+' alterado com sucesso!',[mbOK], 'Conta Corrente', mtConfirmation);
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;

  Caixa := TCaixa.Create;
  if (rgCC_Conferido.ItemIndex = 1) and (not Caixa.RecuperarLancCC(xAux_ConCC_ent_ID,xAux_ConCC_lote_ID,xAux_ConCC_cc_seq)) then begin
    Try
      DM.FDCon.StartTransaction;
      Caixa.cx_dataentrada := edCC_datalancamento.Date;
      if rgCC_tipo.ItemIndex = 1 then
        Caixa.cx_debcred   := 'C'
      else
        Caixa.cx_debcred   := 'D';
      Caixa.cx_valor       := edCC_valor.Value;
      Caixa.ent_id         := edCodigo.AsInteger;
      Caixa.cx_obs         := edCC_descricao.Text;
      Caixa.cx_forma_pgto  := cbCC_tipo_transacao.ItemIndex;
      Caixa.ent_id_cc      := xAux_ConCC_ent_ID;
      Caixa.lote_id        := xAux_ConCC_lote_ID;
      Caixa.cc_seq         := xAux_ConCC_cc_seq;

      xAux_CaixaID := Caixa.Ultima;
      Caixa.Gravar(xAux_CaixaID);

      if rgCC_tipo.ItemIndex = 1 then
        msgPerson('Cr�dito lan�ado no caixa com sucesso!',[mbok],'Lan�amento de Caixa',mtConfirmation)
      else
        msgPerson('D�bito lan�ado no caixa com sucesso!',[mbok],'Lan�amento de Caixa',mtConfirmation);

      DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;
    Except
      on e:Exception do begin
        EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
        DM.FDCon.Rollback;
        ShowMessage('Erro na grava��o! ('+e.message+' )');
      end;
    End;
  end;
  FreeAndNil(Caixa);

  Close;
end;

end.
