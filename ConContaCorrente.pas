unit ConContaCorrente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Buttons, JvExMask, JvToolEdit, JvBaseEdits, Vcl.Mask,
  Vcl.DBCtrls, DateUtils, Vcl.CheckLst, frxClass, frxDBSet, Vcl.Menus,
  JvExControls, JvSpeedButton;

type
  TF_ConContaCorrente = class(TF_ConPadrao)
    JvDBGrid2: TJvDBGrid;
    JvDBGrid3: TJvDBGrid;
    FDEntidade: TFDQuery;
    ProviderEntidade: TDataSetProvider;
    CdsEntidade: TClientDataSet;
    DsEntidade: TDataSource;
    FDLote: TFDQuery;
    ProviderLote: TDataSetProvider;
    CdsLote: TClientDataSet;
    DsLote: TDataSource;
    FDCC: TFDQuery;
    ProviderCC: TDataSetProvider;
    CdsCC: TClientDataSet;
    DsCC: TDataSource;
    FDEntidadeENT_ID: TIntegerField;
    FDEntidadeENT_NOME: TStringField;
    CdsEntidadeENT_ID: TIntegerField;
    CdsEntidadeENT_NOME: TStringField;
    FDLoteENT_ID: TIntegerField;
    FDLoteLOTE_ID: TIntegerField;
    FDLoteLOTE_DESCRICAO: TStringField;
    FDLoteLOTE_VALOR: TFloatField;
    CdsLoteENT_ID: TIntegerField;
    CdsLoteLOTE_ID: TIntegerField;
    CdsLoteLOTE_DESCRICAO: TStringField;
    CdsLoteLOTE_VALOR: TFloatField;
    CdsCCENT_ID: TIntegerField;
    CdsCCLOTE_ID: TIntegerField;
    CdsCCCC_SEQ: TIntegerField;
    CdsCCCC_TIPO: TIntegerField;
    CdsCCCC_TIPO_TRANSACAO: TIntegerField;
    CdsCCCC_DESCRICAO: TStringField;
    CdsCCCC_DATA: TSQLTimeStampField;
    CdsCCCC_VALOR: TFloatField;
    FDCCENT_ID: TIntegerField;
    FDCCLOTE_ID: TIntegerField;
    FDCCCC_SEQ: TIntegerField;
    FDCCCC_TIPO: TIntegerField;
    FDCCCC_TIPO_TRANSACAO: TIntegerField;
    FDCCCC_DESCRICAO: TStringField;
    FDCCCC_DATA: TSQLTimeStampField;
    FDCCCC_VALOR: TFloatField;
    FDCCCC_DATALANCAMENTO: TSQLTimeStampField;
    CdsCCCC_DATALANCAMENTO: TSQLTimeStampField;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    FDLoteLOTE_FECHAMENTO: TSQLTimeStampField;
    FDLoteTOTALRECEB: TFloatField;
    FDLoteSALDO: TFloatField;
    CdsLoteLOTE_FECHAMENTO: TSQLTimeStampField;
    CdsLoteTOTALRECEB: TFloatField;
    CdsLoteSALDO: TFloatField;
    CdsLoteCliTotDevido: TAggregateField;
    CdsLoteCliTotRecebido: TAggregateField;
    Panel5: TPanel;
    gbCliente: TGroupBox;
    DBEdit1: TDBEdit;
    GroupBox1: TGroupBox;
    DBEdit2: TDBEdit;
    CdsLoteCliSaldo: TAggregateField;
    GroupBox2: TGroupBox;
    DBEdit3: TDBEdit;
    FDLoteLOTE_NUMERO: TIntegerField;
    CdsLoteLOTE_NUMERO: TIntegerField;
    GroupBox3: TGroupBox;
    edDescricao: TEdit;
    cbPesquisar: TComboBox;
    edValor: TJvCalcEdit;
    rgFiltrar: TRadioGroup;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    edDataFim: TJvDateEdit;
    edDataIni: TJvDateEdit;
    GroupBox6: TGroupBox;
    chkTodos5: TCheckBox;
    cklAluno_Atuacao: TCheckListBox;
    frxImpLote: TfrxDBDataset;
    FDImpLote: TFDQuery;
    FDImpLoteENT_ID: TIntegerField;
    FDImpLoteLOTE_ID: TIntegerField;
    FDImpLoteLOTE_NUMERO: TIntegerField;
    FDImpLoteENT_NOME: TStringField;
    FDImpLoteLOTE_DATAFECHAMENTO: TSQLTimeStampField;
    FDImpLoteLOTE_VALOR: TFloatField;
    FDImpLoteLOTE_DESCRICAO: TStringField;
    FDImpLoteCC_SEQ: TIntegerField;
    FDImpLoteCC_TIPO_TRANSACAO: TIntegerField;
    FDImpLoteCC_DESCRICAO: TStringField;
    FDImpLoteCC_VALOR: TFloatField;
    FDImpLoteCC_DATALANCAMENTO: TSQLTimeStampField;
    CdsImpCC: TClientDataSet;
    CdsImpCCdata: TDateField;
    CdsImpCCsaldoanterior: TFloatField;
    CdsImpCCdebito: TFloatField;
    CdsImpCCcredito: TFloatField;
    CdsImpCCsaldo: TFloatField;
    CdsImpCCtipo: TIntegerField;
    CdsImpCCdescricao: TStringField;
    CdsImpCClote_id: TIntegerField;
    CdsImpCClote_numero: TIntegerField;
    CdsImpCClote_descricao: TStringField;
    CdsImpCClote_valor: TFloatField;
    CdsImpCClote_fechamento: TDateField;
    CdsImpCCTotalRecebLote: TFloatField;
    FDLoteHAVER: TFloatField;
    CdsLoteHAVER: TFloatField;
    CdsImpCCSaldoLote: TFloatField;
    CdsImpCCHaverLote: TFloatField;
    FDLoteSALDOANTERIOR: TFloatField;
    CdsLoteSALDOANTERIOR: TFloatField;
    JvSpeedButton1: TJvSpeedButton;
    PopupImprimir: TPopupMenu;
    Imprimirlanamentosporlote1: TMenuItem;
    ImprimirlanamentostipoCC1: TMenuItem;
    frxExtrato: TfrxDBDataset;
    CdsExtrato: TClientDataSet;
    FDExtrato: TFDQuery;
    ProviderExtrato: TDataSetProvider;
    DsExtrato: TDataSource;
    FDExtratoTIPO: TStringField;
    FDExtratoDATA: TSQLTimeStampField;
    FDExtratoDESCRICAO: TStringField;
    CdsExtratoTIPO: TStringField;
    CdsExtratoDATA: TSQLTimeStampField;
    CdsExtratoDESCRICAO: TStringField;
    FDExtratoLOTE_VALOR: TFloatField;
    CdsExtratoLOTE_VALOR: TFloatField;
    Label1: TLabel;
    DBEdit4: TDBEdit;
    FDCCCC_CONFERIDO: TIntegerField;
    CdsCCCC_CONFERIDO: TIntegerField;
    FDExtratoCONFERIDO: TIntegerField;
    CdsExtratoCONFERIDO: TIntegerField;
    PopupOpcoes: TPopupMenu;
    AlterarparaConferido1: TMenuItem;
    GroupBox7: TGroupBox;
    edTotRecebidoPeriodo: TJvCalcEdit;
    FDExtratoORDEM: TIntegerField;
    CdsExtratoORDEM: TIntegerField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid3DblClick(Sender: TObject);
    procedure CdsCCCC_DATAGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure CdsCCCC_TIPOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure CdsCCCC_TIPO_TRANSACAOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure JvDBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DsEntidadeDataChange(Sender: TObject; Field: TField);
    procedure DsLoteDataChange(Sender: TObject; Field: TField);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure JvDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbPesquisarChange(Sender: TObject);
    procedure edValorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure icoImprimirClick(Sender: TObject);
    procedure Imprimirlanamentosporlote1Click(Sender: TObject);
    procedure ImprimirlanamentostipoCC1Click(Sender: TObject);
    procedure CdsCCCC_CONFERIDOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure AlterarparaConferido1Click(Sender: TObject);
    procedure CdsCCCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConContaCorrente: TF_ConContaCorrente;
  xAux_ConCC_ent_ID  : Integer;
  xAux_ConCC_lote_ID : Integer;
  xAux_ConCC_cc_seq  : Integer;

implementation

{$R *.dfm}

uses DMC, X, CadContaCorrente, CadEntidade, ConEntidade, ConLote, CadLote,
  AuxLote, AuxPeriodo, clsContaCorrente, AuxOrdenacao;

var
  tq : TFDQuery;
  CC : TContaCorrente;

procedure TF_ConContaCorrente.AlterarparaConferido1Click(Sender: TObject);
begin
  inherited;
  CC := TContaCorrente.Create;
  if CdsCC.FieldByName('cc_conferido').AsInteger = 1 then begin
    CC.AlterarConferido(CdsCC.FieldByName('ent_id').AsInteger,
                        CdsCC.FieldByName('lote_id').AsInteger,
                        CdsCC.FieldByName('cc_seq').AsInteger,
                        0);
    msgPerson('Alterado lan�amento para N�O conferido!',[mbok],'Conta Corrente',mtConfirmation);
  end else begin
    CC.AlterarConferido(CdsCC.FieldByName('ent_id').AsInteger,
                        CdsCC.FieldByName('lote_id').AsInteger,
                        CdsCC.FieldByName('cc_seq').AsInteger,
                        1);
    msgPerson('Lan�amento conferido com sucesso!',[mbok],'Conta Corrente',mtConfirmation);
  end;
  FreeAndNil(CC);
  CdsCC.Refresh;
end;

procedure TF_ConContaCorrente.cbPesquisarChange(Sender: TObject);
begin
  inherited;
  if (cbPesquisar.Text = 'Valor de Fechamento') or (cbPesquisar.Text = 'Valor de Lan�amento') then begin
    edDescricao.Clear;
    edValor.Show;
    edDescricao.Hide;
  end else begin
    edValor.Clear;
    edValor.Hide;
    edDescricao.Show;
  end;
end;

procedure TF_ConContaCorrente.CdsCCCalcFields(DataSet: TDataSet);
begin
  inherited;
  edTotRecebidoPeriodo.Value := edTotRecebidoPeriodo.Value+CdsCCCC_VALOR.Value;
end;

procedure TF_ConContaCorrente.CdsCCCC_CONFERIDOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '1' then
    Text := 'SIM'
  else
    Text := 'N�O';
end;

procedure TF_ConContaCorrente.CdsCCCC_DATAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '30/12/1899' then
    Text := '';
end;

procedure TF_ConContaCorrente.CdsCCCC_TIPOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '0' then
    Text := 'D'
  else if Sender.AsString = '1' then
    Text := 'C'
  else
    Text := '';
end;

procedure TF_ConContaCorrente.CdsCCCC_TIPO_TRANSACAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '0' then
    Text := 'CHEQUE'
  else if Sender.AsString = '1' then
    Text := 'DEP�SITO'
  else if Sender.AsString = '2' then
    Text := 'DINHEIRO'
  else if Sender.AsString = '3' then
    Text := 'DOC'
  else if Sender.AsString = '4' then
    Text := 'PIX'
  else if Sender.AsString = '5' then
    Text := 'TED'
  else if Sender.AsString = '6' then
    Text := 'FECHAMENTO'
  else
    Text := '';
end;

procedure TF_ConContaCorrente.DsEntidadeDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  CdsLote.Filter := 'ent_id='+IntToStr(CdsEntidade.FieldByName('ent_id').AsInteger);
end;

procedure TF_ConContaCorrente.DsLoteDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  CdsCC.Filter := 'ent_id='+IntToStr(CdsEntidade.FieldByName('ent_id').AsInteger)+' and lote_id='+IntToStr(CdsLote.FieldByName('lote_id').AsInteger);

  if not CdsCC.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsCC.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';

  if DBEdit1.Text > DBEdit2.Text then begin
    DBEdit3.Color     := clRed;
    GroupBox2.Caption := 'Saldo Devedor';    
  end else if DBEdit1.Text < DBEdit2.Text then begin
    DBEdit3.Color     := clBlue;
    GroupBox2.Caption := 'Saldo em Haver';
  end else begin
    DBEdit3.Color     := clBlack;
    GroupBox2.Caption := 'LOTE QUITADO';
  end;
end;

procedure TF_ConContaCorrente.edDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key=13 then
    icoPesquisarClick(Sender);
end;

procedure TF_ConContaCorrente.edValorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key=13 then
    icoPesquisarClick(Sender);
end;

procedure TF_ConContaCorrente.FormActivate(Sender: TObject);
begin
  inherited;
  edDataIni.Date      := 0; //StartOfTheMonth(now);
  edDataFim.Date      := Date; //EndOfTheMonth(now);
  rgFiltrar.ItemIndex := 1;
  icoPesquisarClick(Sender);
end;

procedure TF_ConContaCorrente.icoFecharClick(Sender: TObject);
begin
  xAux_ConCC_ent_ID  := CdsCC.FieldByName('ent_id').AsInteger;
  xAux_ConCC_lote_ID := CdsCC.FieldByName('lote_id').AsInteger;
  xAux_ConCC_cc_seq  := CdsCC.FieldByName('cc_seq').AsInteger;
  inherited;

end;

procedure TF_ConContaCorrente.icoImprimirClick(Sender: TObject);
begin
  inherited;
//  caminho     := ExtractFilePath(Application.ExeName);
//  arquivo     := 'logo.png';
//
//  xAux_AuxLote_Lote_Numero := 0;
//  AbreForm(TF_AuxLote,Self);
//
//  if (xAux_AuxLote_Lote_Numero > 0)  then begin
//    DM.frxGenerico.Clear;
//    DM.frxGenerico.LoadFromFile(ExtractFilePath(Application.ExeName)+'Fast\ImpLote.fr3');
//
//    FDImpLote.Close;
//    FDImpLote.ParamByName('k_lote_numero').AsInteger := xAux_AuxLote_Lote_Numero;
//    FDImpLote.Open;
//
//    if ( FileExists(caminho+arquivo)) then begin
//      if (DM.frxGenerico.FindObject('Picture1')<> nil )  then
//        TfrxPictureView(DM.frxGenerico.FindObject('Picture1')).Picture.LoadFromFile(caminho+arquivo);
//    end;
//
//    DM.frxGenerico.PrepareReport;
//    DM.frxGenerico.ShowReport;
//  end;


end;

procedure TF_ConContaCorrente.icoNovoClick(Sender: TObject);
begin
  inherited;
  if CdsEntidade.RecordCount = 0 then begin
    msgPerson('N�o h� nenhum cliente cadastrado para efetuar um la�amento!',[mbok],'Conta Corrente',mtError);
    Exit;
  end else if CdsLote.RecordCount = 0 then begin
    msgPerson('N�o h� nenhum lote cadastrado para efetuar um la�amento!',[mbok],'Conta Corrente',mtError);
    Exit;
  end;

  xAux_ConCC_ent_ID  := CdsEntidade.FieldByName('ent_id').AsInteger;
  xAux_ConCC_lote_ID := CdsLote.FieldByName('lote_id').AsInteger;
  xAux_ConCC_cc_seq  := 0;
  AbreForm(TF_CadContaCorrente,Self);
  icoPesquisarClick(Sender);
  if xAux_ConCC_cc_seq <> 0 then begin
    CdsEntidade.Locate('ent_id',xAux_ConCC_ent_ID,[]);
    CdsLote.Locate('ent_id;lote_id',VarArrayOf([xAux_ConCC_ent_ID,xAux_ConCC_lote_ID]),[]);
    CdsCC.Locate('ent_id;lote_id;cc_seq',VarArrayOf([xAux_ConCC_ent_ID,xAux_ConCC_lote_ID,xAux_ConCC_cc_seq]),[]);
  end;
  xAux_ConCC_ent_ID  := 0;
  xAux_ConCC_lote_ID := 0;
  xAux_ConCC_cc_seq  := 0;
end;

procedure TF_ConContaCorrente.icoPesquisarClick(Sender: TObject);
var
  xTransacao : String;
begin
  inherited;
  xTransacao := LerItemIndex(cklAluno_Atuacao);

  CdsEntidade.Close;

  FDEntidade.Close;
  FDEntidade.Params.Clear;
  FDEntidade.SQL.Clear;
  FDEntidade.SQL.Add('select distinct e.ent_id, e.ent_nome                                            '+
                     'from entidade e                                                                 '+
                     'left outer join lote l on e.ent_id=l.ent_id                                     '+
                     'left outer join contacorrente cc on l.ent_id=cc.ent_id and l.lote_id=cc.lote_id ');
  if cbPesquisar.Text = 'Descri��o do Lote' then
    FDEntidade.SQL.Add('where lote_descricao like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Numero do Lote' then
    FDEntidade.SQL.Add('where lote_numero like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Valor de Fechamento' then begin
    FDEntidade.SQL.Add('where lote_valor = :k_lote_valor ');
    FDEntidade.ParamByName('k_lote_valor').AsFloat := edValor.Value;
  end;
  if rgFiltrar.ItemIndex = 0 then begin
    FDEntidade.SQL.Add('and cc.cc_data >= :k_dataIni and cc.cc_data <= :k_dataFim ');
    FDEntidade.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDEntidade.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end else if rgFiltrar.ItemIndex = 1 then begin
    FDEntidade.SQL.Add('and cc.cc_datalancamento >= :k_dataIni and cc.cc_datalancamento <= :k_dataFim ');
    FDEntidade.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDEntidade.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end else begin
    FDEntidade.SQL.Add('and l.lote_datafechamento >= :k_dataIni and l.lote_datafechamento <= :k_dataFim ');
    FDEntidade.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDEntidade.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end;
  if Trim(xTransacao) <> '' then
    FDEntidade.SQL.Add('and CC.cc_tipo_transacao in ' + xTransacao + ' ');
  FDEntidade.SQL.Add('order by e.ent_nome                                                        ');
  CdsEntidade.Open;

  CdsLote.Close;

  FDLote.Close;
  FDLote.Params.Clear;
  FDLote.SQL.Clear;
  FDLote.SQL.Add('select ent_id, lote_id, lote_numero, lote_descricao, lote_fechamento, lote_valor,                                                '+
                 '(totalRecebAnterior-LoteAnterior) SaldoAnterior,                                                                                 '+
                 'iif((totalReceb+totalRecebAnterior-LoteAnterior-lote_valor)>0,lote_valor,iif(totalReceb>0,totalReceb+totalRecebAnterior-LoteAnterior,0)) totalReceb, '+
                 'iif((lote_valor-totalReceb-totalRecebAnterior+LoteAnterior)<0,0,(lote_valor-totalReceb-totalRecebAnterior+LoteAnterior)) Saldo,  '+
                 'iif((lote_valor-totalReceb-totalRecebAnterior+LoteAnterior)<0,(totalReceb-lote_valor+totalRecebAnterior-LoteAnterior),0) Haver   '+
                 'from(                                                                                                                            '+
                 'select l.ent_id ent_id, l.lote_id lote_id, l.lote_numero lote_numero, l.lote_descricao lote_descricao,                           '+
                 'l.lote_datafechamento lote_fechamento, l.lote_valor lote_valor,                                                                  '+
                 '(                                                                                                                                '+
                 'select Coalesce(sum(cc2.cc_valor),0)                                                                                             '+
                 'from contacorrente cc2                                                                                                           '+
                 'inner join lote l2 on cc2.ent_id=l2.ent_id and cc2.lote_id=l2.lote_id                                                            '+
                 'inner join entidade e2 on cc2.ent_id=e2.ent_id and cc2.lote_id=l2.lote_id                                                        '+
                 'where e2.ent_id=l.ent_id and l2.lote_datafechamento < l.lote_datafechamento                                                      '+
                 ') totalRecebAnterior,                                                                                                            '+
                 '(select Coalesce(sum(l3.lote_valor),0)                                                                                           '+
                 'from lote l3                                                                                                                     '+
                 'inner join entidade e3 on l3.ent_id=e3.ent_id                                                                                    '+
                 'where e3.ent_id=l.ent_id and l3.lote_datafechamento < l.lote_datafechamento                                                      '+
                 ') LoteAnterior,                                                                                                                  '+
                 '(                                                                                                                                '+
                 'select Coalesce(sum(cc.cc_valor),0)                                                                                              '+
                 'from contacorrente cc                                                                                                            '+
                 'inner join entidade e on cc.ent_id=e.ent_id and cc.lote_id=l.lote_id                                                             '+
                 'where e.ent_id=l.ent_id                                                                                                          '+
                 ') totalReceb                                                                                                                     '+
                 'from lote l                                                                                                                      ');
  if cbPesquisar.Text = 'Descri��o do Lote' then
    FDLote.SQL.Add('where lote_descricao like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Numero do Lote' then
    FDLote.SQL.Add('where lote_numero like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Valor de Fechamento' then begin
    FDLote.SQL.Add('where lote_valor = :k_lote_valor ');
    FDLote.ParamByName('k_lote_valor').AsFloat := edValor.Value;
  end;
  if rgFiltrar.ItemIndex = 2 then begin
    FDLote.SQL.Add('and l.lote_datafechamento >= :k_dataIni and l.lote_datafechamento <= :k_dataFim ');
    FDLote.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDLote.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end;
  FDLote.SQL.Add('order by l.ent_id, l.lote_datafechamento)                                                                    '+
                 'group by ent_id, lote_id, lote_numero, lote_descricao, lote_fechamento, lote_valor, totalRecebAnterior,LoteAnterior, totalReceb, Saldo, Haver ');
  CdsLote.Open;

  CdsCC.Close;

  FDCC.Close;
  FDCC.Params.Clear;
  FDCC.SQL.Clear;
  FDCC.SQL.Add('select cc.*                                                      '+
               'from contacorrente cc                                            '+
               'left outer join lote l on cc.ent_id=l.ent_id and cc.lote_id=l.lote_id ');
  if cbPesquisar.Text = 'Descri��o do Lote' then
    FDCC.SQL.Add('where lote_descricao like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Numero do Lote' then
    FDCC.SQL.Add('where lote_numero like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Valor de Fechamento' then begin
    FDCC.SQL.Add('where cc_valor = :k_cc_valor ');
    FDCC.ParamByName('k_cc_valor').AsFloat := edValor.Value;
  end else if cbPesquisar.Text = 'Valor de Lan�amento' then begin
    FDCC.SQL.Add('where lote_valor = :k_lote_valor ');
    FDCC.ParamByName('k_lote_valor').AsFloat := edValor.Value;
  end;
  if rgFiltrar.ItemIndex = 0 then begin
    FDCC.SQL.Add('and cc.cc_data >= :k_dataIni and cc.cc_data <= :k_dataFim ');
    FDCC.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDCC.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end else if rgFiltrar.ItemIndex = 1 then begin
    FDCC.SQL.Add('and cc.cc_datalancamento >= :k_dataIni and cc.cc_datalancamento <= :k_dataFim ');
    FDCC.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDCC.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end else begin
    FDCC.SQL.Add('and l.lote_datafechamento >= :k_dataIni and l.lote_datafechamento <= :k_dataFim ');
    FDCC.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDCC.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end;
  if Trim(xTransacao) <> '' then
    FDCC.SQL.Add('and CC.cc_tipo_transacao in ' + xTransacao + ' ');
  FDCC.SQL.Add('order by cc.ent_id, cc.lote_id, cc.cc_datalancamento, cc.cc_seq            ');
  CdsCC.Open;

  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select sum(cc.cc_valor) total                                                   '+
             'from contacorrente cc                                                           '+
             'where cc.cc_datalancamento >= :k_dataIni and cc.cc_datalancamento <= :k_dataFim ');
  tq.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
  tq.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  tq.Open;

  edTotRecebidoPeriodo.Value := tq.FieldByName('total').AsFloat;

  tq.Close;
  tq.Free;

  if not CdsCC.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsCC.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConContaCorrente.Imprimirlanamentosporlote1Click(Sender: TObject);
var
  caminho, arquivo   : String;
  xSaldoAnterior     : Double;
  xTotalDevedor      : Double;
  xTotalRecebido     : Double;
  xSaldoAntDevedor   : Double;
  xSaldoAntRecebido  : Double;
  xSaldoAnteriorLote : Double;
begin
  inherited;
  caminho     := ExtractFilePath(Application.ExeName);
  arquivo     := 'logo.png';

  CdsImpCC.Close;
  CdsImpCC.CreateDataSet;
  CdsImpCC.Open;

  DM.frxGenerico.Clear;
  DM.frxGenerico.LoadFromFile(ExtractFilePath(Application.ExeName)+'Fast\ImpLote2.fr3');

  CdsLote.First;
  CdsCC.First;

  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;

  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select Coalesce((sum(l.lote_valor) * -1),0) TotalDevedor '+
             'from lote l                                              '+
             'where l.ent_id = :k_ent_id                               ');
  tq.ParamByName('k_ent_id').AsInteger := CdsLote.FieldByName('ent_id').AsInteger;
  tq.Open;

  xTotalDevedor := tq.FieldByName('TotalDevedor').AsFloat;

  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select Coalesce((sum(cc.cc_valor)),0) TotalRecebido '+
             'from contacorrente cc                               '+
             'where cc.ent_id = :k_ent_id                         ');
  tq.ParamByName('k_ent_id').AsInteger := CdsLote.FieldByName('ent_id').AsInteger;
  tq.Open;

  xTotalRecebido := tq.FieldByName('TotalRecebido').AsFloat;

  DM.frxGenerico.Variables['ent_nome']       := QuotedStr(CdsEntidade.FieldByName('ent_nome').AsString);
  DM.frxGenerico.Variables['total_devedor']  := QuotedStr(Formata_Valor_decimal(xTotalDevedor,2));
  DM.frxGenerico.Variables['total_recebido'] := QuotedStr(Formata_Valor_decimal(xTotalRecebido,2));
  DM.frxGenerico.Variables['saldo_total']    := QuotedStr(Formata_Valor_decimal(xTotalDevedor+xTotalRecebido,2));
  if xTotalDevedor+xTotalRecebido > 0 then
    DM.frxGenerico.Variables['deve_haver']   := QuotedStr('HAVER')
  else if xTotalDevedor+xTotalRecebido < 0 then
    DM.frxGenerico.Variables['deve_haver']   := QuotedStr('DEVE')
  else
    DM.frxGenerico.Variables['deve_haver']   := QuotedStr('QUITADO');

  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select Coalesce((sum(l.lote_valor) * -1),0) TotalDevedor       '+
             'from lote l                                                    '+
             'where l.ent_id = :k_ent_id and l.lote_datafechamento < :k_data ');
  tq.ParamByName('k_ent_id').AsInteger := CdsLote.FieldByName('ent_id').AsInteger;
  tq.ParamByName('k_data').AsDateTime  := CdsLote.FieldByName('lote_fechamento').AsDateTime;
  tq.Open;

  xSaldoAntDevedor := tq.FieldByName('TotalDevedor').AsFloat;

  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select Coalesce((sum(cc.cc_valor)),0) TotalRecebido                '+
             'from contacorrente cc                                              '+
             'inner join lote l on cc.ent_id=l.ent_id and cc.lote_id=l.lote_id   '+
             'where cc.ent_id = :k_ent_id and l.lote_datafechamento < :k_data    ');
//             'where cc.ent_id = :k_ent_id and l.lote_numero < :k_lote_numero   ');
  tq.ParamByName('k_ent_id').AsInteger      := CdsLote.FieldByName('ent_id').AsInteger;
//  tq.ParamByName('k_lote_numero').AsInteger := CdsLote.FieldByName('lote_numero').AsInteger;
  tq.ParamByName('k_data').AsDateTime  := CdsLote.FieldByName('lote_fechamento').AsDateTime;
  tq.Open;

  xSaldoAntRecebido := tq.FieldByName('TotalRecebido').AsFloat;

  xSaldoAnterior := xSaldoAntDevedor+xSaldoAntRecebido;

  while not CdsLote.Eof do begin
    CdsImpCC.Append;
    CdsImpCC.FieldByName('data').AsDateTime            := CdsLote.FieldByName('lote_fechamento').AsDateTime;
    CdsImpCC.FieldByName('saldoanterior').AsFloat      := xSaldoAnterior;
    CdsImpCC.FieldByName('debito').AsFloat             := CdsLote.FieldByName('lote_valor').AsFloat*-1;
    CdsImpCC.FieldByName('credito').AsFloat            := 0;
    CdsImpCC.FieldByName('saldo').AsFloat              := xSaldoAnterior + CdsImpCC.FieldByName('debito').AsFloat;
    CdsImpCC.FieldByName('tipo').AsInteger             := 6;
    CdsImpCC.FieldByName('descricao').AsString         := CdsLote.FieldByName('lote_descricao').AsString;
    CdsImpCC.FieldByName('lote_id').AsInteger          := CdsLote.FieldByName('lote_id').AsInteger;
    CdsImpCC.FieldByName('lote_numero').AsInteger      := CdsLote.FieldByName('lote_numero').AsInteger;
    CdsImpCC.FieldByName('lote_descricao').AsString    := CdsLote.FieldByName('lote_descricao').AsString;
    CdsImpCC.FieldByName('lote_fechamento').AsDateTime := CdsLote.FieldByName('lote_fechamento').AsDateTime;
    CdsImpCC.FieldByName('lote_valor').AsFloat         := CdsLote.FieldByName('lote_valor').AsFloat;
    xSaldoAnterior                                     := CdsImpCC.FieldByName('saldo').AsFloat;
//    xSaldoAnteriorLote                                 := CdsImpCC.FieldByName('saldo').AsFloat;
    CdsImpCC.FieldByName('TotalRecebLote').AsFloat     := CdsLote.FieldByName('totalReceb').AsFloat;
    CdsImpCC.FieldByName('SaldoLote').AsFloat          := CdsLote.FieldByName('Saldo').AsFloat;
    CdsImpCC.FieldByName('HaverLote').AsFloat          := CdsLote.FieldByName('Haver').AsFloat;
    CdsImpCC.Post;

    while not CdsCC.Eof do begin
      CdsImpCC.Append;
      CdsImpCC.FieldByName('data').AsDateTime            := CdsCC.FieldByName('cc_datalancamento').AsDateTime;
      CdsImpCC.FieldByName('saldoanterior').AsFloat      := xSaldoAnterior;
      CdsImpCC.FieldByName('debito').AsFloat             := 0;
      CdsImpCC.FieldByName('credito').AsFloat            := CdsCC.FieldByName('cc_valor').AsFloat;
      CdsImpCC.FieldByName('saldo').AsFloat              := CdsImpCC.FieldByName('saldoanterior').AsFloat + CdsImpCC.FieldByName('credito').AsFloat;
      CdsImpCC.FieldByName('tipo').AsInteger             := CdsCC.FieldByName('cc_tipo_transacao').AsInteger;
      CdsImpCC.FieldByName('descricao').AsString         := CdsCC.FieldByName('cc_descricao').AsString;
      CdsImpCC.FieldByName('lote_id').AsInteger          := CdsLote.FieldByName('lote_id').AsInteger;
      CdsImpCC.FieldByName('lote_numero').AsInteger      := CdsLote.FieldByName('lote_numero').AsInteger;
      CdsImpCC.FieldByName('lote_descricao').AsString    := CdsLote.FieldByName('lote_descricao').AsString;
      CdsImpCC.FieldByName('lote_fechamento').AsDateTime := 0;
      CdsImpCC.FieldByName('lote_valor').AsFloat         := 0;
      xSaldoAnterior                                     := CdsImpCC.FieldByName('saldo').AsFloat;
      CdsImpCC.FieldByName('TotalRecebLote').AsFloat     := CdsLote.FieldByName('totalReceb').AsFloat;
      CdsImpCC.FieldByName('SaldoLote').AsFloat          := CdsLote.FieldByName('Saldo').AsFloat;
      CdsImpCC.FieldByName('HaverLote').AsFloat          := CdsLote.FieldByName('Haver').AsFloat;
      CdsImpCC.Post;
      CdsCC.Next;
    end;

    CdsLote.Next;
  end;

  if ( FileExists(caminho+arquivo)) then begin
    if (DM.frxGenerico.FindObject('Picture1')<> nil )  then
      TfrxPictureView(DM.frxGenerico.FindObject('Picture1')).Picture.LoadFromFile(caminho+arquivo);
  end;

  tq.Close;
  tq.Free;

  DM.frxGenerico.PrepareReport;
  DM.frxGenerico.ShowReport;
end;

procedure TF_ConContaCorrente.ImprimirlanamentostipoCC1Click(Sender: TObject);
var
  caminho, arquivo : String;
  xMes             : String;
begin
  inherited;
  AbreForm(TF_AuxOrdenacao,Self);
  if xAux_AuxOrdenacao_Ordem = -1 then
    Exit;

  AbreForm(TF_AuxPeriodo,Self);

  if AuxPeriodo_Ent_ID = 0 then
    Exit;

  caminho     := ExtractFilePath(Application.ExeName);
  arquivo     := 'logo.png';

  DM.frxGenerico.Clear;
  DM.frxGenerico.LoadFromFile(ExtractFilePath(Application.ExeName)+'Fast\ImpCC.fr3');

  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select sum(lote_valor) SaldoAnterior from(                         '+
             'select Sum(cc.cc_valor) lote_valor                                 '+
             'from contacorrente cc                                              '+
             'inner join entidade e on cc.ent_id=e.ent_id                        '+
             'where cc.ent_id = :k_ent_id and cc.cc_datalancamento < :k_dataIni  '+
             '                                                                   '+
             'union                                                              '+
             '                                                                   '+
             'select Sum(l.lote_valor*-1) lote_valor                             '+
             'from lote l                                                        '+
             'inner join entidade e2 on l.ent_id=e2.ent_id                       '+
             'where l.ent_id = :k_ent_id and l.lote_datafechamento < :k_dataIni) ');
  tq.ParamByName('k_ent_id').AsInteger   := AuxPeriodo_Ent_ID;
  tq.ParamByName('k_dataIni').AsDateTime := AuxPeriodo_DataIni;
  tq.Open;

  if MonthOf(AuxPeriodo_DataIni) = MonthOf(AuxPeriodo_DataFim) then
    xMes := MesExtenso(MonthOf(AuxPeriodo_DataIni))
  else
    xMes := '';

  DM.frxGenerico.Variables['SaldoAnterior'] := QuotedStr(FloatToStr(tq.FieldByName('SaldoAnterior').AsFloat));
  DM.frxGenerico.Variables['dataIni']       := QuotedStr(DateToStr(AuxPeriodo_DataIni));
  DM.frxGenerico.Variables['dataFim']       := QuotedStr(DateToStr(AuxPeriodo_DataFim));
  DM.frxGenerico.Variables['mes']           := QuotedStr(xMes);

  CdsExtrato.Close;
  FDExtrato.Close;
  FDExtrato.Params.Clear;
  FDExtrato.SQL.Clear;
  FDExtrato.SQL.Add('select ''C'' tipo, cc.cc_datalancamento data, cc.cc_descricao descricao, cc.cc_valor lote_valor, cc.cc_conferido conferido,    '+
                    'cc.cc_ordem ordem                                                                                                              '+
                    'from contacorrente cc                                                                                                          '+
                    'inner join entidade e on cc.ent_id=e.ent_id                                                                                    '+
                    'where cc.ent_id = :k_ent_id and cc.cc_datalancamento >= :k_dataIni and cc.cc_datalancamento <= :k_dataFim                      '+
                    '                                                                                                                               '+
                    'union                                                                                                                          '+
                    '                                                                                                                               '+
                    'select ''D'' tipo, l.lote_datafechamento data, l.lote_descricao descricao, (l.lote_valor*-1) lote_valor, 1 conferido, 0 ordem  '+
                    'from lote l                                                                                                                    '+
                    'inner join entidade e2 on l.ent_id=e2.ent_id                                                                                   '+
                    'where l.ent_id = :k_ent_id and l.lote_datafechamento >= :k_dataIni and l.lote_datafechamento <= :k_dataFim                     ');
  if xAux_AuxOrdenacao_Ordem = 0 then
    FDExtrato.SQL.Add('order by 6,2                                                                                                                 ')
  else
    FDExtrato.SQL.Add('order by 2                                                                                                                   ');
  FDExtrato.ParamByName('k_ent_id').AsInteger   := AuxPeriodo_Ent_ID;
  FDExtrato.ParamByName('k_dataIni').AsDateTime := AuxPeriodo_DataIni;
  FDExtrato.ParamByName('k_dataFim').AsDateTime := AuxPeriodo_DataFim;
  CdsExtrato.Open;

  if xAux_AuxOrdenacao_Ordem = 0 then begin
    tq.Close;
    tq.Params.Clear;
    tq.SQL.Clear;
    tq.SQL.Add('select sum(Coalesce(l.lote_valor,0)) valor  '+
               'from lote l                                                                                                                    '+
               'inner join entidade e2 on l.ent_id=e2.ent_id                                                                                   '+
               'where l.ent_id = :k_ent_id and l.lote_datafechamento >= :k_dataIni and l.lote_datafechamento <= :k_dataFim                     ');
    tq.ParamByName('k_ent_id').AsInteger   := AuxPeriodo_Ent_ID;
    tq.ParamByName('k_dataIni').AsDateTime := AuxPeriodo_DataIni;
    tq.ParamByName('k_dataFim').AsDateTime := AuxPeriodo_DataFim;
    tq.Open;
    DM.frxGenerico.Variables['valorDevido']:= QuotedStr(FloatToStr(tq.FieldByName('valor').AsFloat));
  end;

  DM.frxGenerico.Variables['ordenacao']         := QuotedStr(IntToStr(xAux_AuxOrdenacao_Ordem));

  if ( FileExists(caminho+arquivo)) then begin
    if (DM.frxGenerico.FindObject('Picture1')<> nil )  then
      TfrxPictureView(DM.frxGenerico.FindObject('Picture1')).Picture.LoadFromFile(caminho+arquivo);
  end;

  DM.frxGenerico.PrepareReport;
  DM.frxGenerico.ShowReport;

  tq.Close;
  tq.Free;
end;

procedure TF_ConContaCorrente.JvDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
//  if Column.FieldName = 'SALDO' then begin
//    if CdsLote.FieldByName('SALDO').AsFloat > 0 then begin
//      JvDBGrid2.Canvas.Brush.Color := clGreen;
//      JvDBGrid2.Canvas.Font.Color  := clwhite;
//    end else if CdsLote.FieldByName('SALDO').AsFloat < 0 then begin
//      JvDBGrid2.Canvas.Brush.Color := clRed;
//      JvDBGrid2.Canvas.Font.Color  := clwhite;
//    end else begin
//      JvDBGrid2.Canvas.Brush.Color := clWhite;
//      JvDBGrid2.Canvas.Font.Color  := clBlack;
//    end;
//  end;
//  JvDBGrid2.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TF_ConContaCorrente.JvDBGrid3DblClick(Sender: TObject);
begin
  inherited;
  if CdsCC.RecordCount > 0 then begin
    xAux_ConCC_ent_ID  := CdsCC.FieldByName('ent_id').AsInteger;
    xAux_ConCC_lote_ID := CdsCC.FieldByName('lote_id').AsInteger;
    xAux_ConCC_cc_seq  := CdsCC.FieldByName('cc_seq').AsInteger;
    AbreForm(TF_CadContaCorrente, Self);
    icoPesquisarClick(Sender);
    CdsEntidade.Locate('ent_id',xAux_ConCC_ent_ID,[]);
    CdsLote.Locate('lote_id',xAux_ConCC_lote_ID,[]);
    CdsCC.Locate('cc_seq',xAux_ConCC_cc_seq,[]);
    xAux_ConCC_ent_ID := 0;
    xAux_ConCC_lote_ID := 0;
    xAux_ConCC_cc_seq := 0;
  end;
end;

procedure TF_ConContaCorrente.JvDBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if Column.FieldName = 'CC_TIPO' then begin
    if CdsCC.FieldByName('CC_TIPO').AsInteger = 0 then begin
      JvDBGrid3.Canvas.Brush.Color := clRed;
      JvDBGrid3.Canvas.Font.Color  := clwhite;
    end else begin
      JvDBGrid3.Canvas.Brush.Color := clGreen;
      JvDBGrid3.Canvas.Font.Color  := clwhite;
    end;
  end;
  if Column.FieldName = 'CC_CONFERIDO' then begin
    if CdsCC.FieldByName('CC_CONFERIDO').AsInteger = 1 then begin
      JvDBGrid3.Canvas.Brush.Color := clSkyBlue;
      JvDBGrid3.Canvas.Font.Color  := clBlack;
    end else begin
      JvDBGrid3.Canvas.Brush.Color := clWhite;
      JvDBGrid3.Canvas.Font.Color  := clBlack;
    end;
  end;

  JvDBGrid3.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TF_ConContaCorrente.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  xAux_ConEntidade_Ent_ID := 0;
  AbreForm(TF_CadEntidade,Self);
  icoPesquisarClick(Sender);
  if xAux_ConEntidade_Ent_ID <> 0 then
    CdsEntidade.Locate('ent_id',xAux_ConEntidade_Ent_ID,[]);
  xAux_ConEntidade_Ent_ID := 0;
end;

procedure TF_ConContaCorrente.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  xAux_ConLote_Ent_ID  := CdsEntidade.FieldByName('ent_id').AsInteger;
  xAux_ConLote_lote_ID := 0;
  AbreForm(TF_CadLote,Self);
  icoPesquisarClick(Sender);
  if xAux_ConLote_lote_ID <> 0 then
    CdsLote.Locate('ent_id;lote_id',VarArrayOf([xAux_ConLote_Ent_ID,xAux_ConLote_lote_ID]),[]);
  xAux_ConLote_Ent_ID  := 0;
  xAux_ConLote_lote_ID := 0;
end;

end.
