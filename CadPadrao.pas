unit CadPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, JvBaseEdits, Vcl.Buttons, JvRichEdit;

type
  TF_CadPadrao = class(TForm)
    Panel2: TPanel;
    Timer1: TTimer;
    Panel3: TPanel;
    Panel4: TPanel;
    pnCancelar: TPanel;
    pnAlterar: TPanel;
    pnExcluir: TPanel;
    pnSalvar: TPanel;
    pnFechar: TPanel;
    Panel5: TPanel;
    icoFechar: TImage;
    icoAlterar: TImage;
    icoExcluir: TImage;
    icoSalvar: TImage;
    icoCancelar: TImage;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure icoFecharClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ControleBotoes(Botao:Integer); //1-Cancelar 2-Alterar
  end;

var
  F_CadPadrao: TF_CadPadrao;

implementation

{$R *.dfm}

uses Principal, X;

procedure TF_CadPadrao.ControleBotoes(Botao: Integer);
begin
  //Estado dos bot�es
  if Botao = 1 then begin
    icoSalvar.Hide;
    icoAlterar.Show;
    icoExcluir.Show;
    icoFechar.Show;
    icoCancelar.Hide;

    //Estado das legendas dos bot�es
    pnSalvar.Hide;
    pnAlterar.Show;
    pnExcluir.Show;
    pnFechar.Show;
    pnCancelar.Hide;

    //Ordem dos bot�es quando est�o vis�veis
    icoFechar.Left  := 629;
    icoExcluir.Left := 539;
    icoAlterar.Left := 449;

    //Ordem das legendas bot�es quando est�o vis�veis
    pnFechar.Left := 629;
    pnExcluir.Left := 539;
    pnAlterar.Left := 449;
  end else begin
    if Botao = 2 then begin
      icoSalvar.Show;
      icoAlterar.Hide;
      icoExcluir.Hide;
      icoFechar.Hide;
      icoCancelar.Show;

      //Estado das legendas dos bot�es
      pnSalvar.Show;
      pnAlterar.Hide;
      pnExcluir.Hide;
      pnFechar.Hide;
      pnCancelar.Show;

      //Ordem dos bot�es quando est�o vis�veis
      icoCancelar.Left := 540;
      icoSalvar.Left := 450;

      //Ordem das legendas bot�es quando est�o vis�veis
      pnCancelar.Left := 540;
      pnSalvar.Left := 450;
    end;
  end;
end;

procedure TF_CadPadrao.FormCreate(Sender: TObject);
begin
  if IsDebuggerPresent then begin
    Caption := Caption + ' - BASE TESTE';
  end;
end;

procedure TF_CadPadrao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F3) and (icoSalvar.Visible = True) then
    icoSalvar.OnClick(Self)
  else if (Key = VK_F5) and (icoCancelar.Visible = True) then
    icoCancelar.OnClick(Self)
  else if (Key = VK_F2) and (icoAlterar.Visible = True) then
    icoAlterar.OnClick(Self)
  else if (Key = VK_F4) and (icoExcluir.Visible = True) then
    icoExcluir.OnClick(Self)
  else if (Key = VK_ESCAPE) and (icoCancelar.Visible = False) then
    icoFechar.OnClick(Self);
end;

procedure TF_CadPadrao.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    if (not (ActiveControl is TMemo)) and (not (ActiveControl is TJvRichEdit)) then
      Perform(Wm_NextDlgCtl,0,0);
end;

procedure TF_CadPadrao.icoFecharClick(Sender: TObject);
begin
  Close;
end;

end.
