unit clsCaixa;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TCaixa = class
  public
    cx_id          : Integer;
    ent_id         : Integer;
    cx_dataentrada : TDateTime;
    cx_valor       : Double;
    cx_obs         : String;
    cx_debcred     : String; //C-Credito - D-D�bito
    cx_forma_pgto  : Integer; //0-Cheque
                              //1-Dep�sito
                              //2-Dinheiro
                              //3-DOC
                              //4-PIX
                              //5-TED
                              //6-FECHAMENTO
    ent_id_cc      : Integer;
    lote_id        : Integer;
    cc_seq         : Integer;

    function  Recuperar(Pcx_id:Integer):Boolean;
    procedure Gravar(Pcx_id:Integer);
    procedure Eliminar(Pcx_id:Integer);
    function  Ultima:Integer;
    function  RecuperarObs(Pent_id:Integer;Pcx_debcred:String):Boolean;
    function  RecuperarLancCC(Pent_id_cc,Plote_id,Pcc_seq:Integer):Boolean;
  end;

implementation

{ TCaixa }

uses DMC;

procedure Tcaixa.Eliminar(Pcx_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from caixa where cx_id = :K_cx_id');
  tq.ParamByName('K_cx_id').AsInteger      := Pcx_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure Tcaixa.Gravar(Pcx_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from caixa where cx_id = :k_cx_id');
  tq.ParamByName('k_cx_id').AsInteger := Pcx_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into caixa values('+
    ':K_cx_id,'+
    ':K_ent_id,'+
    ':K_cx_dataentrada,'+
    ':K_cx_valor,'+
    ':K_cx_obs,'+
    ':K_cx_debcred,'+
    ':k_cx_forma_pgto,'+
    ':k_ent_id_cc,'+
    ':k_lote_id,'+
    ':k_cc_seq)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update caixa set '+
    'ent_id         = :K_ent_id,'+
    'cx_dataentrada = :K_cx_dataentrada,'+
    'cx_valor       = :K_cx_valor,'+
    'cx_obs         = :K_cx_obs,'+
    'cx_debcred     = :K_cx_debcred,'+
    'cx_forma_pgto  = :k_cx_forma_pgto,'+
    'ent_id_cc      = :k_ent_id_cc,'+
    'lote_id        = :k_lote_id,'+
    'cc_seq         = :k_cc_seq ');
    tq.SQL.Add('where cx_id = :K_cx_id');
  end;

  tq.ParamByName('K_cx_id').AsInteger            := Pcx_id;
  if ent_id > 0 then
    tq.ParamByName('K_ent_id').AsInteger         := ent_id
  else
    tq.ParamByName('K_ent_id').IsNull;
  tq.ParamByName('K_cx_dataentrada').AsDateTime  := cx_dataentrada;
  tq.ParamByName('K_cx_valor').AsFloat           := cx_valor;
  tq.ParamByName('K_cx_obs').AsString            := cx_obs;
  tq.ParamByName('K_cx_debcred').AsString        := cx_debcred;
  if cx_forma_pgto > 0 then
    tq.ParamByName('k_cx_forma_pgto').AsInteger  := cx_forma_pgto
  else
    tq.ParamByName('k_cx_forma_pgto').IsNull;
  if ent_id_cc > 0 then begin
    tq.ParamByName('k_ent_id_cc').AsInteger      := ent_id_cc;
    tq.ParamByName('k_lote_id').AsInteger        := lote_id;
    tq.ParamByName('k_cc_seq').AsInteger         := cc_seq;
  end else begin
    tq.ParamByName('k_ent_id_cc').IsNull;
    tq.ParamByName('k_lote_id').IsNull;
    tq.ParamByName('k_cc_seq').IsNull;
  end;


  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TCaixa.Recuperar(Pcx_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from caixa where cx_id = :K_cx_id');
  tq.ParamByName('K_cx_id').AsInteger      := Pcx_id;
  tq.Open;
  if tq.Eof then
    Result := False
  else
    Result := True;

  cx_id          := tq.FieldByName('cx_id').AsInteger;
  ent_id         := tq.FieldByName('ent_id').AsInteger;
  cx_dataentrada := tq.FieldByName('cx_dataentrada').AsDateTime;
  cx_valor       := tq.FieldByName('cx_valor').AsFloat;
  cx_obs         := tq.FieldByName('cx_obs').AsString;
  cx_debcred     := tq.FieldByName('cx_debcred').AsString;
  cx_forma_pgto  := tq.FieldByName('cx_forma_pgto').AsInteger;
  ent_id_cc      := tq.FieldByName('ent_id_cc').AsInteger;
  lote_id        := tq.FieldByName('lote_id').AsInteger;
  cc_seq         := tq.FieldByName('cc_seq').AsInteger;

  tq.Close;
  tq.Free;
end;

function TCaixa.RecuperarLancCC(Pent_id_cc, Plote_id,
  Pcc_seq: Integer): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('select first 1 cx.*                                                               '+
             'from caixa cx                                                                     '+
             'where cx.ent_id_cc=:k_ent_id_cc and cx.lote_id=:k_lote_id and cx.cc_seq=:k_cc_seq '+
             'order by cx.cx_id desc                                                            ');
  tq.ParamByName('k_ent_id_cc').AsInteger := Pent_id_cc;
  tq.ParamByName('k_lote_id').AsInteger   := Plote_id;
  tq.ParamByName('k_cc_seq').AsInteger    := Pcc_seq;
  tq.Open;
  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('cx_id').AsInteger);

  tq.Close;
  tq.Free;
end;

function TCaixa.RecuperarObs(Pent_id: Integer; Pcx_debcred: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('select first 1 cx.*                                       '+
             'from caixa cx                                             '+
             'where cx.ent_id=:k_ent_id and cx.cx_debcred=:k_cx_debcred '+
             'order by cx.cx_id desc                                    ');
  tq.ParamByName('k_ent_id').AsInteger    := Pent_id;
  tq.ParamByName('k_cx_debcred').AsString := Pcx_debcred;
  tq.Open;
  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('cx_id').AsInteger);

  tq.Close;
  tq.Free;
end;

function Tcaixa.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(cx_id) maior from caixa');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
