inherited F_CadUsuario: TF_CadUsuario
  Caption = ' CADASTRO DE USU'#193'RIO'
  ClientHeight = 344
  ClientWidth = 686
  OnActivate = FormActivate
  ExplicitWidth = 702
  ExplicitHeight = 383
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 267
    Width = 686
    ExplicitTop = 277
    ExplicitWidth = 686
    inherited icoFechar: TImage
      Left = 596
      ExplicitLeft = 596
    end
    inherited icoAlterar: TImage
      Left = 416
      OnClick = icoAlterarClick
      ExplicitLeft = 416
    end
    inherited icoExcluir: TImage
      Left = 506
      OnClick = icoExcluirClick
      ExplicitLeft = 506
    end
    inherited icoSalvar: TImage
      Left = 236
      OnClick = icoSalvarClick
      ExplicitLeft = 236
    end
    inherited icoCancelar: TImage
      Left = 326
      OnClick = icoCancelarClick
      ExplicitLeft = 326
    end
  end
  inherited Panel3: TPanel
    Width = 686
    Height = 226
    ExplicitWidth = 686
    ExplicitHeight = 236
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 684
      Height = 224
      ActivePage = TabSheet1
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = False
      object TabSheet1: TTabSheet
        Caption = 'Dados do Usu'#225'rio'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 694
        ExplicitHeight = 202
        object Label2: TLabel
          Left = 30
          Top = 9
          Width = 36
          Height = 17
          Caption = 'Nome'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 34
          Top = 40
          Width = 32
          Height = 17
          Caption = 'Login'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 176
          Top = 40
          Width = 35
          Height = 17
          Caption = 'Senha'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 502
          Top = 9
          Width = 57
          Height = 17
          Caption = 'Admiss'#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 502
          Top = 40
          Width = 57
          Height = 17
          Caption = 'Demiss'#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 16
          Top = 71
          Width = 49
          Height = 17
          Caption = 'Telefone'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 42
          Top = 99
          Width = 24
          Height = 17
          Caption = 'Obs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object edUsua_nome: TEdit
          Left = 72
          Top = 6
          Width = 422
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edUsua_login: TEdit
          Left = 72
          Top = 37
          Width = 96
          Height = 25
          CharCase = ecUpperCase
          TabOrder = 2
        end
        object edUsua_senha: TEdit
          Left = 214
          Top = 37
          Width = 96
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          PasswordChar = '*'
          TabOrder = 3
        end
        object edUsua_Admissao: TJvDateEdit
          Left = 565
          Top = 6
          Width = 105
          Height = 25
          ShowNullDate = False
          TabOrder = 1
        end
        object edUsua_Desligamento: TJvDateEdit
          Left = 565
          Top = 37
          Width = 105
          Height = 25
          ShowNullDate = False
          TabOrder = 4
        end
        object rgUsua_Ativo: TRadioGroup
          Left = 559
          Top = 63
          Width = 111
          Height = 41
          Caption = ' Ativo '
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ItemIndex = 0
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          TabOrder = 6
        end
        object ckUsua_alterarSenha: TCheckBox
          Left = 523
          Top = 110
          Width = 147
          Height = 17
          Caption = 'Alterar senha ao logar'
          TabOrder = 8
        end
        object edUsua_Fone: TMaskEdit
          Left = 72
          Top = 68
          Width = 113
          Height = 25
          EditMask = '!\(99\)90000-0000;1;_'
          MaxLength = 14
          TabOrder = 5
          Text = '(  )     -    '
        end
        object edUsua_Obs: TMemo
          Left = 72
          Top = 99
          Width = 422
          Height = 89
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Dados do Email'
        ImageIndex = 1
        object Label10: TLabel
          Left = 18
          Top = 16
          Width = 62
          Height = 17
          Alignment = taRightJustify
          Caption = 'Remetente'
        end
        object Label11: TLabel
          Left = 49
          Top = 47
          Width = 31
          Height = 17
          Caption = 'Email'
        end
        object Label12: TLabel
          Left = 35
          Top = 78
          Width = 45
          Height = 17
          Alignment = taRightJustify
          Caption = 'Usu'#225'rio'
        end
        object Label13: TLabel
          Left = 45
          Top = 109
          Width = 35
          Height = 17
          Alignment = taRightJustify
          Caption = 'Senha'
        end
        object Label14: TLabel
          Left = 7
          Top = 140
          Width = 73
          Height = 17
          Alignment = taRightJustify
          Caption = 'Config Email'
        end
        object sbConfigEmail: TSpeedButton
          Left = 129
          Top = 137
          Width = 25
          Height = 26
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000585751747069
            E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
            F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
            8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
            FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
            FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
            FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
            B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
            A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
            3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
            CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
            74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
            F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
            B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
            C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
            999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
        end
        object edUsua_email_nome: TEdit
          Left = 86
          Top = 13
          Width = 295
          Height = 25
          TabOrder = 0
        end
        object edUsua_email: TEdit
          Left = 86
          Top = 44
          Width = 295
          Height = 25
          TabOrder = 1
        end
        object edUsua_email_usuario: TEdit
          Left = 86
          Top = 75
          Width = 295
          Height = 25
          TabOrder = 2
        end
        object edUsua_email_senha: TEdit
          Left = 86
          Top = 106
          Width = 295
          Height = 25
          PasswordChar = '*'
          TabOrder = 3
        end
        object edConfe_id: TJvCalcEdit
          Left = 86
          Top = 137
          Width = 37
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ShowButton = False
          TabOrder = 4
          DecimalPlacesAlwaysShown = False
        end
        object edConfe_nome: TEdit
          Left = 160
          Top = 137
          Width = 221
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Configura'#231#245'es'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 694
        ExplicitHeight = 202
        object Label16: TLabel
          Left = 23
          Top = 15
          Width = 92
          Height = 17
          Alignment = taRightJustify
          Caption = 'Impressora PDF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 3
          Top = 46
          Width = 112
          Height = 17
          Alignment = taRightJustify
          Caption = 'Impressora Padr'#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object edusua_caminho_imp_pdf: TEdit
          Left = 121
          Top = 12
          Width = 295
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edusua_caminho_imp_padrao: TEdit
          Left = 121
          Top = 43
          Width = 295
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
  end
  inherited Panel4: TPanel
    Top = 312
    Width = 686
    ExplicitTop = 322
    ExplicitWidth = 686
    inherited pnCancelar: TPanel
      Left = 326
      ExplicitLeft = 326
    end
    inherited pnAlterar: TPanel
      Left = 416
      ExplicitLeft = 416
    end
    inherited pnExcluir: TPanel
      Left = 506
      ExplicitLeft = 506
    end
    inherited pnSalvar: TPanel
      Left = 236
      ExplicitLeft = 236
    end
    inherited pnFechar: TPanel
      Left = 596
      ExplicitLeft = 596
    end
  end
  inherited Panel5: TPanel
    Width = 686
    ExplicitWidth = 686
    object Label1: TLabel
      Left = 23
      Top = 10
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 72
      Top = 7
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 576
  end
end
