--000.001 - 13-01-2022
CREATE TABLE USUARIO (
    USUA_ID                     INTEGER NOT NULL,
    USUA_NOME                   VARCHAR(250) NOT NULL,
    USUA_LOGIN                  VARCHAR(50) NOT NULL,
    USUA_SENHA                  VARCHAR(50) NOT NULL,
    USUA_DATAADMISSAO           DATE,
    USUA_DATADESLIGAMENTO       DATE,
    USUA_FONE                   VARCHAR(15),
    USUA_OBS                    blob sub_type 1 segment size 80,
    USUA_ATIVO                  INTEGER,
    USUA_EMAIL                  VARCHAR(255),
    USUA_EMAIL_NOME             VARCHAR(255),
    USUA_EMAIL_USUARIO          VARCHAR(255),
    USUA_EMAIL_SENHA            VARCHAR(255),
    usua_caminho_imp_pdf        VARCHAR(255),
    usua_caminho_imp_padrao     VARCHAR(255),
    usua_alterarSenha           Integer
);

ALTER TABLE USUARIO ADD PRIMARY KEY (USUA_ID);

CREATE TABLE CONFIGEMAIL (
    CONFE_ID            INTEGER NOT NULL,
    CONFE_NOME          VARCHAR(150),
    CONFE_AUTENTICACAO  VARCHAR(15),
    CONFE_PORTA         INTEGER,
    CONFE_SMTP          VARCHAR(255),
    CONFE_ENDERECO      VARCHAR(100)
);

ALTER TABLE CONFIGEMAIL ADD PRIMARY KEY (CONFE_ID);

CREATE TABLE ENTIDADE (
    ENT_ID                       INTEGER NOT NULL,
    ENT_NOME                     VARCHAR(250),
    ENT_DATACADASTRO             DATE,
    ENT_CELULAR                  VARCHAR(20),
    ENT_TELEFONE                 VARCHAR(20),
    ENT_EMAIL                    VARCHAR(255),
    ENT_ATIVO                    INTEGER,
    ENT_OBS                      BLOB SUB_TYPE 1 SEGMENT SIZE 80
);

ALTER TABLE ENTIDADE ADD PRIMARY KEY (ENT_ID);

SET TERM ^ ;

CREATE PROCEDURE Proc_Digitos(Texto VARCHAR(100)) 
RETURNS(Retorno VARCHAR(100)) AS 
DECLARE Ch CHAR(1); 
BEGIN 
  Retorno = ''; 
  WHILE (Texto IS NOT NULL AND Texto <> '') DO 
  BEGIN 
    Ch = SUBSTRING(Texto FROM 1 FOR 1); 
    IF (Ch >= '0' AND Ch <= '9') THEN 
      Retorno = Retorno || Ch; 
    Texto = SUBSTRING(Texto FROM 2 FOR 100); 
  END 
  SUSPEND; 
END^ 

SET TERM ; ^

create table Lote (
    Ent_id              integer not null,
    Lote_id             integer not null,
    Lote_descricao      varchar(255),
    Lote_datacadastro   date,
    Lote_datafechamento date,
    Lote_valor          numeric(15,2),
    Lote_obs            BLOB SUB_TYPE 1 SEGMENT SIZE 80
);

alter table Lote add primary key(ent_id,lote_id);
alter table Lote add foreign key(ent_id) references Entidade(ent_id);

--000.002 - 16-01-2022
Create table ContaCorrente (
    ent_id              integer not null,
    lote_id             integer not null,
    cc_seq              integer not null,
    cc_tipo             integer,
    cc_tipo_transacao   integer,
    cc_descricao        varchar(255),
    cc_data             date,
    cc_valor            numeric(15,2)
);

alter table ContaCorrente add primary key(ent_id,lote_id,cc_seq);
alter table ContaCorrente add foreign key(ent_id,lote_id) references Lote(ent_id,lote_id);

alter table ContaCorrente add cc_datalancamento date;

alter table lote add lote_numero integer;

--000.003 - 17-01-2022
Create table Libera (
    lib_id              integer not null,
    lib_mac             varchar(20) not null
);

alter table Libera add primary key(lib_id);

--000.004 - 27-03-2022
alter table contacorrente add cc_conferido integer default 0 not null;

--000.005 - 09-10-2022
CREATE TABLE CAIXA (
    CX_ID           INTEGER NOT NULL,
    ENT_ID          INTEGER,
    CX_DATAENTRADA  DATE,
    CX_VALOR        NUMERIC(15,2),
    CX_OBS          VARCHAR(200),
    CX_DEBCRED      CHAR(1),
    CX_FORMA_PGTO   INTEGER,
    ent_id_cc       Integer,
    lote_id         Integer,
    cc_seq          Integer
);


ALTER TABLE CAIXA ADD PRIMARY KEY (CX_ID);
ALTER TABLE CAIXA ADD FOREIGN KEY (ENT_ID) REFERENCES ENTIDADE (ENT_ID);
ALTER TABLE CAIXA ADD FOREIGN KEY (ENT_ID_CC,LOTE_ID,CC_SEQ) REFERENCES CONTACORRENTE (ENT_ID,LOTE_ID,CC_SEQ);

--000.006 - 27-08-2023
alter table contacorrente add cc_ordem integer;
