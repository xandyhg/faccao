unit CadCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Buttons, JvToolEdit, Vcl.Mask,
  JvExMask, JvBaseEdits, FireDAC.Comp.Client;

type
  TF_CadCaixa = class(TF_CadPadrao)
    ed_cx_id: TJvCalcEdit;
    Label1: TLabel;
    dtpData: TJvDateEdit;
    Label3: TLabel;
    rg_debcred: TRadioGroup;
    ed_cx_valor: TJvCalcEdit;
    Label5: TLabel;
    ed_clifor_nome: TEdit;
    sb_clifor: TSpeedButton;
    ed_clifor_id: TJvCalcEdit;
    Label7: TLabel;
    ed_cx_obs: TEdit;
    Label6: TLabel;
    cbFormaPgto: TComboBox;
    Label2: TLabel;
    procedure ed_clifor_idExit(Sender: TObject);
    procedure ed_clifor_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure rg_debcredClick(Sender: TObject);
    procedure sb_cliforClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CarregaObs;
  end;

var
  F_CadCaixa: TF_CadCaixa;

implementation

{$R *.dfm}

uses clsCaixa, DMC, X, clsEntidade, Principal, ConEntidade, ConCaixa;

var
  Caixa     : TCaixa;
  tq        : TFDQuery;
  Entidade  : TEntidade;

{ TF_CadCaixa }

procedure TF_CadCaixa.CarregaObs;
begin
  Caixa := TCaixa.Create;
  if rg_debcred.ItemIndex = 0 then begin
    if Caixa.RecuperarObs(ed_clifor_id.AsInteger,'C') then begin
      ed_cx_obs.Text := Caixa.cx_obs;
    end;
  end;
  if rg_debcred.ItemIndex = 1 then begin
    if Caixa.RecuperarObs(ed_clifor_id.AsInteger,'D') then begin
      ed_cx_obs.Text := Caixa.cx_obs;
    end;
  end;
  FreeAndNil(Caixa);
end;

procedure TF_CadCaixa.ed_clifor_idExit(Sender: TObject);
begin
  inherited;
  if ed_clifor_id.AsInteger > 0 then begin
    Entidade := TEntidade.Create;
    if Entidade.Recuperar(ed_clifor_id.AsInteger) then begin
      ed_clifor_nome.Text := Entidade.ent_nome;
      CarregaObs;
    end else begin
      if rg_debcred.ItemIndex = 0 then
        msgPerson('Devedor n�o encontrado!',[mbok],'Lan�amento de Caixa',mtError)
      else
        msgPerson('Favorecido n�o encontrado!',[mbok],'Lan�amento de Caixa',mtError);
      ed_clifor_id.SetFocus;
      ed_clifor_id.SelectAll;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadCaixa.ed_clifor_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 113 then
    sb_cliforClick(Sender);
end;

procedure TF_CadCaixa.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConCaixa_cx_id > 0 then begin
    Caixa    := TCaixa.Create;
    Entidade := TEntidade.Create;
    Caixa.Recuperar(xAux_ConCaixa_cx_id);
    ed_cx_id.AsInteger          := Caixa.cx_id;
    dtpData.Date                := Caixa.cx_dataentrada;
    if Caixa.cx_debcred = 'C' then
      rg_debcred.ItemIndex      := 0
    else
      rg_debcred.ItemIndex      := 1;
    ed_cx_valor.Value           := Caixa.cx_valor;
    ed_clifor_id.AsInteger      := Caixa.ent_id;
    ed_cx_obs.Text              := Caixa.cx_obs;
    Entidade.Recuperar(ed_clifor_id.AsInteger);
    ed_clifor_nome.Text         := Entidade.ent_nome;
    cbFormaPgto.ItemIndex       := Caixa.cx_forma_pgto;

    FreeAndNil(Caixa);
    FreeAndNil(Entidade);

    Panel3.Enabled := False;
    ControleBotoes(1);
  end else begin
    dtpData.Date                := Date;
    rg_debcred.ItemIndex        := 1;
    ed_cx_id.AsInteger          := 0;
    ed_clifor_id.Clear;
    ed_clifor_nome.Clear;
    ed_cx_valor.Clear;
    ed_cx_obs.Clear;
    cbFormaPgto.ItemIndex       := 4;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadCaixa.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadCaixa.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConCaixa_cx_id > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadCaixa.icoExcluirClick(Sender: TObject);
begin
  inherited;
  try
    DM.FDCon.StartTransaction;
    Caixa := TCaixa.Create;
    if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Caixa',mtWarning) = IDNO then begin
      DM.FDCon.Rollback;
      FreeAndNil(Caixa);
      Exit;
    end;

    Caixa.Eliminar(ed_cx_id.AsInteger);

    msgPerson('Registro eliminado com sucesso!',[mbok],'Caixa',mtConfirmation);

    FreeAndNil(Caixa);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadCaixa.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Caixa := TCaixa.Create;

    Caixa.cx_dataentrada := dtpData.Date;
    if rg_debcred.ItemIndex = 0 then
      Caixa.cx_debcred   := 'C'
    else
      Caixa.cx_debcred   := 'D';
    Caixa.cx_valor       := ed_cx_valor.Value;
    Caixa.ent_id         := ed_clifor_id.AsInteger;
    Caixa.cx_obs         := ed_cx_obs.Text;
    Caixa.cx_forma_pgto  := cbFormaPgto.ItemIndex;

    if ed_cx_id.AsInteger = 0 then begin
      xAux_ConCaixa_cx_id := Caixa.Ultima;
      Caixa.Gravar(xAux_ConCaixa_cx_id);
    end else begin
      Caixa.Gravar(xAux_ConCaixa_cx_id);
    end;

    if rg_debcred.ItemIndex = 0 then
      msgPerson('Cr�dito lan�ado com sucesso!',[mbok],'Lan�amento de Caixa',mtConfirmation)
    else
      msgPerson('D�bito lan�ado com sucesso!',[mbok],'Lan�amento de Caixa',mtConfirmation);

    FreeAndNil(Caixa);

    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

procedure TF_CadCaixa.rg_debcredClick(Sender: TObject);
begin
  inherited;
  if rg_debcred.ItemIndex = 0 then
    Label7.Caption := 'Devedor'
  else if rg_debcred.ItemIndex = 1 then
    Label7.Caption := 'Favorecido';
end;

procedure TF_CadCaixa.sb_cliforClick(Sender: TObject);
begin
  inherited;
  AbreForm(TF_ConEntidade,Self);
  ed_clifor_id.AsInteger      := xAux_ConEntidade_Ent_ID;
end;

end.
