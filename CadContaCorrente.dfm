inherited F_CadContaCorrente: TF_CadContaCorrente
  Caption = ' Lan'#231'amentos de Conta Corrente'
  ClientHeight = 224
  ClientWidth = 631
  OnActivate = FormActivate
  ExplicitWidth = 647
  ExplicitHeight = 263
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 147
    Width = 631
    ExplicitTop = 147
    ExplicitWidth = 631
    inherited icoFechar: TImage
      Left = 541
      ExplicitLeft = 541
    end
    inherited icoAlterar: TImage
      Left = 361
      OnClick = icoAlterarClick
      ExplicitLeft = 361
    end
    inherited icoExcluir: TImage
      Left = 451
      OnClick = icoExcluirClick
      ExplicitLeft = 451
    end
    inherited icoSalvar: TImage
      Left = 181
      OnClick = icoSalvarClick
      ExplicitLeft = 181
    end
    inherited icoCancelar: TImage
      Left = 271
      OnClick = icoCancelarClick
      ExplicitLeft = 271
    end
  end
  inherited Panel3: TPanel
    Width = 631
    Height = 106
    ExplicitWidth = 631
    ExplicitHeight = 106
    object Label4: TLabel
      Left = 12
      Top = 11
      Width = 70
      Height = 17
      Alignment = taRightJustify
      Caption = 'Lan'#231'amento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 329
      Top = 11
      Width = 59
      Height = 17
      Alignment = taRightJustify
      Caption = 'Transa'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 487
      Top = 11
      Width = 30
      Height = 17
      Alignment = taRightJustify
      Caption = 'Valor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 25
      Top = 43
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 194
      Top = 76
      Width = 41
      Height = 17
      Caption = 'Ordem'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCC_datalancamento: TJvDateEdit
      Left = 88
      Top = 8
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 0
    end
    object rgCC_tipo: TRadioGroup
      Left = 194
      Top = 0
      Width = 131
      Height = 34
      Caption = 'Tipo'
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemIndex = 1
      Items.Strings = (
        'D'#233'bito'
        'Cr'#233'dito')
      ParentFont = False
      TabOrder = 1
    end
    object cbCC_tipo_transacao: TComboBox
      Left = 394
      Top = 8
      Width = 87
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemIndex = 1
      ParentFont = False
      TabOrder = 2
      Text = 'Dep'#243'sito'
      Items.Strings = (
        'Cheque'
        'Dep'#243'sito'
        'Dinheiro'
        'DOC'
        'PIX'
        'TED')
    end
    object edCC_valor: TJvCalcEdit
      Left = 523
      Top = 8
      Width = 100
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
    end
    object edCC_descricao: TEdit
      Left = 88
      Top = 40
      Width = 535
      Height = 25
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object rgCC_Conferido: TRadioGroup
      Left = 88
      Top = 66
      Width = 100
      Height = 34
      Caption = 'Conferido'
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim')
      ParentFont = False
      TabOrder = 5
    end
    object edcc_ordem: TJvCalcEdit
      Left = 241
      Top = 73
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 6
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Panel4: TPanel
    Top = 192
    Width = 631
    ExplicitTop = 192
    ExplicitWidth = 631
    inherited pnCancelar: TPanel
      Left = 271
      ExplicitLeft = 271
    end
    inherited pnAlterar: TPanel
      Left = 361
      ExplicitLeft = 361
    end
    inherited pnExcluir: TPanel
      Left = 451
      ExplicitLeft = 451
    end
    inherited pnSalvar: TPanel
      Left = 181
      ExplicitLeft = 181
    end
    inherited pnFechar: TPanel
      Left = 541
      ExplicitLeft = 541
    end
  end
  inherited Panel5: TPanel
    Width = 631
    ExplicitWidth = 631
    object Label1: TLabel
      Left = 12
      Top = 12
      Width = 70
      Height = 17
      Caption = 'C'#243'd. Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 150
      Top = 12
      Width = 56
      Height = 17
      Caption = 'C'#243'd. Lote'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 464
      Top = 12
      Width = 53
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cadastro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 278
      Top = 13
      Width = 47
      Height = 17
      Caption = 'C'#243'd. CC'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 88
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edCodigo2: TJvCalcEdit
      Left = 212
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 1
      DecimalPlacesAlwaysShown = False
    end
    object edCC_data: TJvDateEdit
      Left = 523
      Top = 9
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 2
    end
    object edCodigo3: TJvCalcEdit
      Left = 331
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 584
    Top = 112
  end
end
